#include "logdownload.h"
#include "constants.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QProgressDialog>
#include <QDesktopServices>
#include <QInputDialog>

LogDownload::LogDownload() {
}

LogDownload::LogDownload(USBInterface &interface, DatabaseHelper &helper) {
  usbInterface = &interface;
  dbHelper = &helper;

  for (int i = 0; i < Constants::TOTAL_SENSORS; i++) {
    availableSensors[i] = false;
    sensorLogValues[i] = 0;
  }
  for (int i = 0; i < Constants::TOTAL_ADC; i++) {
    availableADC[i] = false;
    adcLogValues[i] = 0;
  }
  for (int i = 0; i < Constants::TOTAL_DEBUG; i++) {
    availableDebug[i] = false;
    debugLogValues[i] = 0;
  }

  sensorNames[0] = "Temperature";
  sensorNames[1] = "Humidity";
  sensorNames[2] = "Pressure";
  sensorNames[3] = "Light";
  sensorNames[4] = "CO";
  sensorNames[5] = "H2S";
  sensorNames[6] = "IR Temperature";
  debugNames[0] = "UV";
  debugNames[1] = "IR";
  debugNames[2] = "Battery";
  debugNames[3] = "Tasks";

  packetSize = 0;

  csvFilePath = "";
}

void LogDownload::setAvailableSensors(bool *sensors) {
  for (int i = 0; i < Constants::TOTAL_SENSORS; i++) {
    availableSensors[i] = sensors[i];
  }
}

void LogDownload::setAvailableADC(bool *adc) {
  for (int i = 0; i < Constants::TOTAL_ADC; i++) {
    availableADC[i] = adc[i];
  }
}

void LogDownload::setAvailableDebug(bool *debug) {
  for (int i = 0; i < Constants::TOTAL_DEBUG; i++) {
    availableDebug[i] = debug[i];
  }
}

void LogDownload::setLogPacketSize(int pSize) {
  packetSize = pSize;
}

/**
 * @brief Downloads data from ClimateLogger and places in database
 *        Uses deviceName to create a new capture record, then
 *        uses the capture id and device id in the log records
 * @param parent
 * @param deviceName
 * @return capture id
 */
int LogDownload::downloadFromDevice(QWidget *parent, QString deviceName,
        bool coVersion) {

  bool isWorking = true;
  bool ret = false;
  int totalLogs = 0;
  QString msgText;
  int captureID = 0;
  int deviceID = dbHelper->getDeviceIDFromName(deviceName);
  if (deviceID == 0) {
    // Didn't find an existing device with this name
    // can't go any further, give user feedback and quit    
    QMessageBox msgBox;
    msgBox.setWindowTitle("Notice");
    msgBox.setIcon(QMessageBox::Information);
    msgText = "Could not find device name in database.  Try changing the device name";
    msgBox.setText(msgText);
    msgBox.exec();
    isWorking = false;
    msgText = "";
  } else {
    // if we get to here we know that there is a valid device record
    //  already created, so we're good to add a capture record
    //  and enter the logs for that capture
    if (validateConnection()) {

      // Get log config information
      readConfig();

      // readConfig populates global variables
      int captureTS = unixTimestampValue;
      totalLogs = totalLogValue;
      
      // ask the user if they want to add a note to this capture record
      QString cTitle = "Log name";
      QString cLabel = "You can enter a name for this log";      
      bool ok = true;
      QString captureNote;
      QMessageBox msgBox;
      msgBox.setWindowTitle("Notice");
      msgBox.setIcon(QMessageBox::Information);
      captureNote = QInputDialog::getText(parent, cTitle,cLabel, QLineEdit::Normal,"",&ok);
      if (captureNote.length() > 24) {
        msgText = "Name must be less that 24 characters";
        msgBox.setText(msgText);
        msgBox.exec();
        captureNote.clear();
        // exit if name was too long      
        return 0;
      }else if(!ok){
        // user canceled, so just quit
        return 0;
      }
      
      // Create a new capture record, then use the capture_id returned
      //  for the log entries
      captureID = dbHelper->insertCapture(deviceID, captureTS, captureNote);

      if (captureID == 0) {
        // we weren't able to enter in the capture record, so can't proceed
        QMessageBox msgBox;
        msgBox.setWindowTitle("Notice");
        msgText = "Could not enter log records";
        msgBox.setText(msgText);
        msgBox.exec();
        isWorking = false;
        msgText = "";
      }

      if (isWorking) {
        int recordCount = 0;
        // Can start processing records now
        // Create progress bar
        QProgressDialog progress(parent);
        progress.setWindowTitle("Download");
        progress.setWindowModality(Qt::WindowModal);
        progress.setLabelText("Downloading logs...");
        progress.setCancelButton(0);
        progress.setRange(0, totalLogs);
        progress.setMinimumDuration(0);
        progress.show();        

        dbHelper->startTransaction();

        int fail = 0;
        for (int i = 0; i < totalLogs; i++) {

          progress.setValue(i);

          if (progress.wasCanceled()) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Notice");
            msgText = "Download was canceled";
            msgBox.setText(msgText);
            msgBox.exec();
            msgText = "";

            // we have to delete the new capture record and
            // any logs that have been entered so far
            bool deleteCapture = dbHelper->removeCapture(parent,captureID, false);
            if (!deleteCapture) {
              // THIS HAS NOT BEEN TESTED!
              // remove for production
              QMessageBox msgBox;
              msgBox.setWindowTitle("Notice");
              msgText = "Delete of partial log failed";
              msgBox.setText(msgText);
              msgBox.exec();
              msgText = "";
            }
            // need to commit the transaction or capture record
            //  does not get deleted
            isWorking = true;
            // change to captureID to 0 so that front end
            //  doesn't think that records were captured
            captureID = 0;
            break;
          }

          // Read flash at current address
          //  Reads 1 log packet per loop
          //  Also increments unixTimestampValue
          fail = readFlash(i);

          if (fail == 0) {
            // Insert into database
            ret = dbHelper->insertLog(captureID, unixTimestampValue, sensorLogValues[0], 
                    sensorLogValues[1], sensorLogValues[2], sensorLogValues[3], sensorLogValues[4], 
                    adcLogValues[4], adcLogValues[5], adcLogValues[9], debugLogValues[0], 
                    debugLogValues[1], debugLogValues[2], availableSensors, availableADC,
                    availableDebug, coVersion);
            if (!ret) {
              QMessageBox msgBox;
              msgBox.setWindowTitle("Notice");
              msgBox.setIcon(QMessageBox::Information);
              msgText = "A record could not be entered into database\n";
              msgText += "Will attempt to enter the rest of the log records";
              msgBox.setText(msgText);
              msgBox.exec();
              msgText = "";
              // don't want to break here, as we should try the next record
              // may be a problem if they all start failing, as user will not
              // break out of it.

              // Should change message box to one with a cancel option?
              // If wheels fall off user may be clicking OK for thousands of records
              // Check this before production
            }
            isWorking = true;

            //break;
          } else if (fail == 1) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Notice");
            msgBox.setIcon(QMessageBox::Information);
            msgText = "The download has terminated unexpectedly.\n";
            msgText += QString::number(recordCount) + " of ";
            msgText += QString::number(totalLogs) + " records were imported";
            msgBox.setText(msgText);
            msgBox.exec();
            msgText = "";
            isWorking = true;
            break;
          } else if (fail == 2) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Notice");
            msgBox.setIcon(QMessageBox::Information);
            msgText = "The device was reset or interrupted during logging.\n";
            msgText += QString::number(recordCount) + " of ";
            msgText += QString::number(totalLogs) + " records were imported";
            msgBox.setText(msgText);
            msgBox.exec();
            msgText = "";
            isWorking = true;
            break;
          } else if (fail == 3) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Notice");
            msgBox.setIcon(QMessageBox::Information);
            msgBox.setText("The device has no logs to download.");
            msgBox.exec();
            isWorking = true;
            msgText = "";
            break;
          }
          recordCount++;
        }
        progress.setValue(totalLogs);

        // Only finish writing to database if successfully downloaded
        if (isWorking == true) {          
          dbHelper->endTransaction();
        }        
        // update the capture record with the actual record count
        dbHelper->updateCaptureCount(recordCount, captureID);
      }
      // could not validate connection
    }
  }


  return captureID;

}


void LogDownload::generateCSV(QWidget *parent, int captureID) {
  
  // get the log list
  QStringList list = dbHelper->getLogs(parent, settings.getTemperatureUnit(),
                                        settings.getPressureUnit(),captureID);
  
  if (list.size() <= 0) {
    QMessageBox msgBox;
    msgBox.setWindowTitle("Notice");
    msgBox.setText("Operation canceled / no logs to download.");
    msgBox.exec();
  } else {
    // get the log name
    QString logName = dbHelper->getLogName(captureID);
    int numRecords = list.size();

    // Get file path
    QString path = getFolderPath(parent, logName);

    // If path is .csv, it means that the cancel button was hit
    if (path != "") {

      QFile file(path);

      // Save file path for future use
      QStringList directoryList = path.split("/");
      QString directory;
      for (int i = 0; i < directoryList.size() - 1; i++) {
        directory.append(directoryList.at(i));
        directory.append("/");
      }

      settings.saveFileDirectory(directory);

      file.open(QIODevice::WriteOnly | QIODevice::Text);
      QTextStream out(&file);      

      // Set up progress bar
      QProgressDialog progress(parent);
      progress.setWindowModality(Qt::WindowModal);
      progress.setWindowTitle("Notice");
      progress.setLabelText("Generating File...");
      progress.setCancelButton(0);
      progress.setRange(0, numRecords);
      progress.setMinimumDuration(0);
      progress.show();

      QString line;
      QStringList lineList;


      for (int i = 0; i < numRecords; i++) {
        progress.setValue(i);
        line = list.at(i);
        //qDebug() << line;
        line.append("\n");
        out << line;      
      }

      progress.setValue(numRecords);
      file.close();
      // Automatically try to open CSV after downloading
      openCSV();
    }
  }
}
 

bool LogDownload::checkForNewLogs(QString deviceName) {

  // assume it's a new log unless we find otherwise
  bool newLogs = true;
  int captureTS = 0;
  int deviceID;

  if (validateConnection()) {

    // get the device_id from the name argument
    deviceID = dbHelper->getDeviceIDFromName(deviceName);
    if (deviceID > 0) {
      // found the device name in the database

      // Get log config information
      readConfig();

      // readConfig places log timestamp into global variable
      captureTS = unixTimestampValue;
      

      // have name and ts now, so see if this record already exists
      if (dbHelper->alreadyCaptured(deviceID, captureTS)) {
        // found a matching record, so these logs have
        //  already been entered into the database
        newLogs = false;
      }
    } else {
      // It's quite possible that this unit has not been plugged into this
      //  computer before, and likely that we don't have the device name
      //  So if this device name isn't in the database
      //  we need to create a new device record here
      QString serial("00001"); // dummy data for now
      QString location(""); // dummy data for now
      bool isWorking = dbHelper->insertDevice(deviceName, serial, location);

      if (!isWorking) {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Change Name Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("Device name could not be entered");
        msgBox.exec();
      }

      // in any case treating this as a new set of logs, so can
      //  assume they'll be downloaded to the database from here
    }
  }
  return newLogs;
}


/**
 * NEED DETAIL ON:
 *  availableSensor(0-4)
 *  availableADC((4,5,9)
 *  availableDebug(0,1,2)
 * @param segment
 * @return 
 */
int LogDownload::readFlash(int segment) {

  int fail = 0;

  QByteArray data = usbInterface->readLog(segment, packetSize, fail);

  if (fail == 0) {

    unsigned char sensorBytes[4];
    int sensorVal = 0;
    int index = 0;

    if (availableSensors[0] == true) {
      sensorBytes[0] = data[index++];
      sensorBytes[1] = data[index++];

      sensorVal = (((int) sensorBytes[1] << 8) & 0x0000FF00) + ((int) sensorBytes[0] & 0x000000FF);

      // Check for negative number
      if (sensorVal >= 32768) {
        sensorVal = sensorVal - 65536;
      }

      // Check for overflow
      if (sensorVal > -9998) {
        sensorLogValues[0] = (float) sensorVal / 10;
      } else {        
        sensorLogValues[0] = (float) sensorVal;
      }
    }
    if (availableSensors[1] == true) {
      sensorBytes[0] = data[index++];
      sensorBytes[1] = data[index++];

      sensorVal = (((int) sensorBytes[1] << 8) & 0x0000FF00) + ((int) sensorBytes[0] & 0x000000FF);

      // Check for negative number
      if (sensorVal >= 32768) {
        sensorVal = sensorVal - 65536;
      }

      // Check for overflow
      if (sensorVal > -9998) {
        sensorLogValues[1] = (float) sensorVal / 10;

        // No negatives for humidity
        if (sensorLogValues[1] < 0) {
          sensorLogValues[1] = 0;
        } else if (sensorLogValues[1] > 100) {
          sensorLogValues[1] = 100;
        }
      } else {
        sensorLogValues[1] = (float) sensorVal;
      }
    }
    if (availableSensors[2] == true) {
      sensorBytes[0] = data[index++];
      sensorBytes[1] = data[index++];
      sensorBytes[2] = data[index++];

      sensorVal = (((int) sensorBytes[2] << 16) & 0x00FF0000) + (((int) sensorBytes[1] << 8) & 0x0000FF00) + ((int) sensorBytes[0] & 0x000000FF);

      // Check for overload
      if (sensorVal < 0xFFFFFE) {
        sensorLogValues[2] = (float) sensorVal / 10;
      } else {
        sensorLogValues[2] = (float) sensorVal;
      }
    }
    if (availableSensors[3] == true) {
      sensorBytes[0] = data[index++];
      sensorBytes[1] = data[index++];
      sensorBytes[2] = data[index++];
      sensorBytes[3] = data[index++];

      sensorVal = (((int) sensorBytes[3] << 24) & 0xFF000000) + (((int) sensorBytes[2] << 16) & 0x00FF0000) + (((int) sensorBytes[1] << 8) & 0x0000FF00) + ((int) sensorBytes[0] & 0x000000FF);


      // Check for overload
      if (sensorVal != 0xFFFFFFFF) {
        sensorLogValues[3] = (float) sensorVal / 10;
      } else {
        sensorLogValues[3] = (float) sensorVal;
      }
    }
    if (availableSensors[4] == true) {
      sensorBytes[0] = data[index++];
      sensorBytes[1] = data[index++];

      sensorVal = (((int) sensorBytes[1] << 8) & 0x0000FF00) + ((int) sensorBytes[0] & 0x000000FF);

      // Check for negative number
      if (sensorVal >= 32768) {
        sensorVal = sensorVal - 65536;
      }

      // Check for overflow
      if (sensorVal > -9998) {
        sensorLogValues[4] = sensorVal / 10;
      } else {
        sensorLogValues[4] = sensorVal;
      }
    }


    if (availableADC[4] == true) {
      sensorBytes[0] = data[index++];
      sensorBytes[1] = data[index++];

      sensorVal = (((int) sensorBytes[1] << 8) & 0x0000FF00) + ((int) sensorBytes[0] & 0x000000FF);
      adcLogValues[4] = sensorVal;
    }
    if (availableADC[5] == true) {
      sensorBytes[0] = data[index++];
      sensorBytes[1] = data[index++];

      sensorVal = (((int) sensorBytes[1] << 8) & 0x0000FF00) + ((int) sensorBytes[0] & 0x000000FF);
      adcLogValues[5] = sensorVal;
    }
    if (availableADC[9] == true) {
      sensorBytes[0] = data[index++];
      sensorBytes[1] = data[index++];

      sensorVal = (((int) sensorBytes[1] << 8) & 0x0000FF00) + ((int) sensorBytes[0] & 0x000000FF);
      adcLogValues[9] = sensorVal;
    }

    if (availableDebug[0] == true) {
      sensorBytes[0] = data[index++];
      sensorBytes[1] = data[index++];

      sensorVal = (((int) sensorBytes[1] << 8) & 0x0000FF00) + ((int) sensorBytes[0] & 0x000000FF);
      debugLogValues[0] = (float) sensorVal;
    }
    if (availableDebug[1] == true) {
      sensorBytes[0] = data[index++];
      sensorBytes[1] = data[index++];
      sensorBytes[2] = data[index++];
      sensorBytes[3] = data[index++];

      sensorVal = (((int) sensorBytes[3] << 24) & 0xFF000000) + (((int) sensorBytes[2] << 16) & 0x00FF0000) + (((int) sensorBytes[1] << 8) & 0x0000FF00) + ((int) sensorBytes[0] & 0x000000FF);
      debugLogValues[1] = (float) sensorVal;
    }
    if (availableDebug[2] == true) {
      sensorBytes[0] = data[index++];
      sensorBytes[1] = data[index++];

      sensorVal = (((int) sensorBytes[1] << 8) & 0x0000FF00) + ((int) sensorBytes[0] & 0x000000FF);
      debugLogValues[2] = (float) sensorVal;
    }

    // Format timestamp
    logTimestamp.setTime_t(unixTimestampValue);
    timestampString = logTimestamp.toString("M/d/yyyy h:mm:ss AP");

    // Increment timestamp
    unixTimestampValue += intervalValue;
  }

  return fail;
}

void LogDownload::readConfig() {

  unixTimestampValue = 0;
  offsetValue = 0;
  intervalValue = 0;
  totalLogValue = 0;

  QByteArray data = usbInterface->readPacket(Constants::SEND_CONFIG_PACKET_ID);

  unixTimestampValue = ((int) data[0] & 0x000000FF) + (((int) data[1] << 8) & 0x0000FF00) + (((int) data[2] << 16) & 0x00FF0000) + (((int) data[3] << 24) & 0xFF000000);
  offsetValue = ((int) data[4] & 0x000000FF) + (((int) data[5] << 8) & 0x0000FF00) + (((int) data[6] << 16) & 0x00FF0000) + (((int) data[7] << 24) & 0xFF000000);
  intervalValue = ((int) data[8] & 0x000000FF) + (((int) data[9] << 8) & 0x0000FF00) + (((int) data[10] << 16) & 0x00FF0000) + (((int) data[11] << 24) & 0xFF000000);
  totalLogValue = ((int) data[12] & 0x000000FF) + (((int) data[13] << 8) & 0x0000FF00);

  //qDebug() << "READ CONFIG STAMP VAL: " << unixTimestampValue;
  //qDebug() << "READ CONFIG OFFSET VAL: " << offsetValue;
  //qDebug() << "READ CONFIG INTERVAL VAL: " << intervalValue;
  //qDebug() << "READ CONFIG LOG VAL: " << totalLogValue;

  // Add one to account for first log at startup
  //totalLogValue += 1;
}

// getters for the readConfig values
int LogDownload::getUnixTimestampValue(){
  return unixTimestampValue;
}

int LogDownload::getOffsetValue(){
  return offsetValue;
}

int LogDownload::getTotalLogValue(){
  return totalLogValue;
}

int LogDownload::getIntervalValue(){
  return intervalValue;
}


/*
 * Called by dl_capture.js.checkCaptureStatus
 *  Places the log start time on the button
 *  in the user interface
 */
QString LogDownload::getLogTimestamp() {
  readConfig();
  logTimestamp.setTime_t(unixTimestampValue);
  timestampString = logTimestamp.toString("M/d/yyyy h:mm:ss AP");

  return timestampString;
}

bool LogDownload::validateConnection() {

  if (usbInterface->isConnected() == false) {

    QMessageBox msgBox;
    msgBox.setWindowTitle("Connection Error");
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.setText("The device is not currently connected over USB.");
    msgBox.exec();
  }

  return usbInterface->isConnected();
}

QString LogDownload::getFolderPath(QWidget *parent, QString logName) {

  QString fileDirectory;

  if (settings.getFileDirectory() == "") {
    fileDirectory = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0);
  } else {
    fileDirectory = settings.getFileDirectory();
  }
  
  if(logName != ""){
    fileDirectory += "\\" + logName;
  }
  QString filter = parent->tr("CSV (*.csv)");
  QString fileName = QFileDialog::getSaveFileName(parent,
          parent->tr("Choose File"),
          fileDirectory,
          parent->tr("CSV (*.csv)"),
          &filter);

  qDebug() << "FILENAME: " << fileName;
  csvFilePath = fileName;

  return fileName;
}

void LogDownload::openCSV() {

  if (csvFilePath == "") {
    QMessageBox msgBox;
    msgBox.setWindowTitle("Notice");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setText("You have not exported the CSV.");
    msgBox.exec();
  } else {
    QString fileUrl = "file:///";
    fileUrl.append(csvFilePath);

    qDebug() << "ATTEMPTING TO OPEN FILE: " << fileUrl;

    QDesktopServices::openUrl(QUrl(fileUrl));
  }
}
