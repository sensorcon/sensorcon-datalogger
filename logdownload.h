#ifndef LOGDOWNLOAD_H
#define LOGDOWNLOAD_H

#include "usbinterface.h"
#include "databasehelper.h"
#include "usersettings.h"

class LogDownload
{
public:
    LogDownload();
    LogDownload(USBInterface &interface, DatabaseHelper &helper);

    void setAvailableSensors(bool *sensors);
    void setAvailableADC(bool *adc);
    void setAvailableDebug(bool *debug);
    void setLogPacketSize(int pSize);
    int downloadFromDevice(QWidget *parent, QString deviceName, bool coVersion);
    void generateCSV(QWidget *parent, int captureID);
    bool checkForNewLogs(QString deviceName);
    QString getLogTimestamp();
    void openCSV();
    int getUnixTimestampValue();
    int getOffsetValue();
    int getTotalLogValue();
    int getIntervalValue();
    void readConfig();
private:
    USBInterface *usbInterface;
    DatabaseHelper *dbHelper;
    UserSettings settings;

    bool availableSensors[8];
    bool availableADC[10];
    bool availableDebug[8];
    float sensorLogValues[8];
    float adcLogValues[10];
    float debugLogValues[8];
    QString sensorNames[8];
    QString debugNames[8];
    int packetSize;
    QDateTime logTimestamp;
    QString timestampString;
    QString csvFilePath;

    int unixTimestampValue;
    int offsetValue;
    int totalLogValue;
    int intervalValue;
    
    int readFlash(int segment);
    bool validateConnection();
    QString getFolderPath(QWidget *parent, QString logName = "");
};

#endif // LOGDOWNLOAD_H
