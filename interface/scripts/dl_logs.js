/* 
 * File:    dl_logs.js
 *
 * Created: January, 2017
 * Purpose: Supports logs.html
 * 
 */

/*******************************************************************************
 * Globals
 */

// Determines which capture record is being graphed
// Is set to top table record when capture page is refreshed, or to
//  active record when a new capture is pulled from the Climate Logger
var globalCaptureID;

// Interupts the readSensor function called by periodicTask
//  when the climate logger is downloading records in order
//  to speed up the download
var downloadingFromDevice = false

var logToggleTemp = true;
var logToggleHumidity = true;
var logTogglePressure = true;
var logToggleLight = true;
var logToggleCo = true;


/** getCaptureRecords
 *  Pulls capture records from local database and displays them on
 *    the log page in a formated table.  The sql.exec function in
 *    databasehelper.getCaptureJSON() sorts the field names in alpha order,
 *    so we needed to rename them as follows:
 *      c.capture_ts AS capture_0
 *      d.device_name AS capture_1
 *      c.capture_note AS capture_2
 *      c.capture_count AS capture_3
 *      c.capture_id AS capture_4
 *    This allowed us to get them in the correct order for the table
 * @params  captureID is OPTIONAL.  captureID is the latest capture
 *          written to the database.  If included that capture will be
 *          highlighted in the table.  If not the table will be 
 *          rebuilt without any records highlighted
 * @returns {undefined}
 */
function getCaptureRecords(captureID) {
  var captureRecords;
  var parsedRecords;
  var headerString;
  var outputString = "&nbsp;";
  var firstRecord = true;
  var recordCount = 0;
  // reset the globalCaptureID
  globalCaptureID = 0;
  captureRecords = ClimateLogger.getCaptureRecordsJSON();
  parsedRecords = JSON.parse(captureRecords);
  if (parsedRecords.length > 0) {
    // returned at least one record, so create table
    // create the header
    headerString = "<table id='log-header-table'>\n";
    headerString += "<thead>\n";
    headerString += "<tr>\n";
    headerString += "<th class='log-table-date'>Start date/time</th>\n";
    headerString += "<th class='log-table-device'>Device</th>\n";
    headerString += "<th class='log-table-name'>Name</th>\n";
    headerString += "<th class='log-table-count'>Count</th>\n";
    headerString += "<th class='log-table-icons'>Chart</th>\n";
    headerString += "<th class='log-table-icons'>Export</th>\n";
    headerString += "<th class='log-table-icons'>Delete</th>\n";
    headerString += "</tr>\n";
    headerString += "</thead>\n";
    headerString += "</table>\n";
    
    // create the body
    outputString = "<table id='log-table-table'>\n";
    outputString += "<tbody>\n";

    for (var i = 0; i < parsedRecords.length; i++) {
      // new row, check for a capture ID, apply formating if found
      for (var key in parsedRecords[i]) {
        if (key === "capture_4") {
          if (captureID) {
            globalCaptureID = captureID;
            if (parsedRecords[i]["capture_4"] === captureID) {
              // flag the row as the newest capture for highlighting
              outputString += "<tr id='capture-" + parsedRecords[i][key];
              outputString += "' class='new-capture'>\n";
            } else {
              // enter the id as id='capture-xx' in the row
              outputString += "<tr id='capture-" + parsedRecords[i][key] + "'>\n";
            }
          } else {
            // not viewing as part of a new capture event, so place
            //  the id as id='capture-xx' in the row
            outputString += "<tr id='capture-" + parsedRecords[i][key] + "'>\n"
          }
        }
      }

      for (var key in parsedRecords[i]) {
        if (key === "capture_0") {
          // this is the date, get it in the correct format
          // change the data type to date first
          var cDate = new Date();
          var sDate;
          cDate = moment.unix(parsedRecords[i][key]);
          // now get it into the format we want
          sDate = dateFormat(cDate, "mm-dd-yyyy h:MM tt");
          outputString += "<td class='log-table-date'>" + sDate + "</td>\n";
        } else if (key === "capture_1") {
          // the device name
          outputString += "<td class='log-table-device'>" + parsedRecords[i][key] + "</td>\n";
        } else if (key === "capture_2") {
          // the note
          outputString += "<td class='log-table-name'>" + parsedRecords[i][key] + "</td>\n";
        } else if (key === "capture_3") {
          // number of log records
          outputString += "<td class='log-table-count'>" + parsedRecords[i][key] + "</td>\n";
        } else if (key === "capture_4") {
          if (firstRecord && globalCaptureID == 0) {
            // Sets the globalCaptureID to the top record, but only
            //  if not already set above because of a new capture
            // Required for the graphing function
            globalCaptureID = parsedRecords[i][key];
            firstRecord = false;
          }
          // chart icon
          outputString += "<td class='log-table-icons'><a href='javascript:captureChart(";
          outputString += parsedRecords[i][key] + ")' ><img src='img/graph_sm.png' ";
          outputString += "/></a></td>\n";
    
          // export icon
          outputString += "<td class='log-table-icons'><a href='javascript:captureTransfer(";
          outputString += parsedRecords[i][key] + ")'><img src='img/export_sm.png' ";
          outputString += "/></a></td>\n";
          
          // delete icon
          outputString += "<td class='log-table-icons'><a href='javascript:captureDelete(";
          outputString += parsedRecords[i][key] + ")'><img src='img/delete_sm.png' ";
          outputString += "/></a></td>\n";
        } else {
          // should never get here, but if something gets added on the back
          //  end we'll wonder why we're not seeing it
          outputString += "<td>" + parsedRecords[i][key] + "</td>\n";
        }
      }
      // end row
      outputString += "</tr>\n";
      recordCount++;
    }
    // end table
    outputString += "</tbody>";
    outputString += "</table>";
    //alert(headerString +"\n" + outputString);

  } else {
    // we'll get here if the database is empty
    headerString = "There are no logs in the database yet";
  }
  
  // If there's more than 5 records the log-table will have a vertical
  //  scroll bar, so we'll need to decrease the width of the log-header
  //  table  so the columns line up
  if(recordCount > 5){
    $("#log-header").width("597px");
  } else {
    $("#log-header").width("614px");
  }
  
  // dump the header html into the header id
  $("#log-header").html(headerString);
  
  // dump the table html into the table id
  $("#log-table").html(outputString);

}
;

/**
 * @brief downloads data from device, then
 *  updates and sets focus on the capture page
 * @returns {undefined}
 */
function downloadLogs() {
  // interupt the sensor stream to make the download faster
  $("#dashboard-chart").html("Chart disabled while downloading logs...");
  downloadingFromDevice = true;
  var captureID = ClimateLogger.downloadFromDevice();
  // reset global variable
  downloadingFromDevice = false;
  // destroy the chart so that the values reset
  destroyChart("dashboard","dashboard-chart");
  //alert("CaptureID: " + captureID);
  if (captureID > 0) {
    // reset new logs button   
    document.getElementById("new-logs-button").style.display = "none";
    // update the capture log page
    getCaptureRecords(captureID);

    // Manually set focus on the log page
    //  as calling selectContent will reset the
    //  new record highlight    
    hideContent(activeContent);
    // set the global variable for the chart
    globalCaptureID = captureID;
    // highlight the logs link in the sidebar
    document.getElementById("dl-logs").style.display = "block";
    // make the log class active
    $("#sidebar-logs").addClass("start active").siblings().removeClass("start active");

    captureChart(captureID);

    // reset the check log flag so that UI will indicate
    //  that there are new logs to download
    checkForNewLogsFlag = 0;
  }
}
;

/**
 * @brief   Deletes a single capture record, 
 *          and ALL associated log records from the database
 * @param {type} captureID
 * @returns {undefined}
 */
function captureDelete(captureID) {
  var deleteWorked = ClimateLogger.deleteCapture(captureID);
  if (deleteWorked) {
    // If no problem or user did not cancel refresh the table
    getCaptureRecords();
    // reset the check log flag so that UI will indicate
    //  that there are new logs to download
    checkForNewLogsFlag = 0;
  }
}
;

/**
 * @brief Transfers database records to CSV
 *        If captureID is not included will
 *          transfer the entire database 
 * @param {type} captureID - OPTIONAL!
 * @returns {undefined}
 */
function captureTransfer(captureID) {
  ClimateLogger.generateCSV(captureID);
}


/**
 * @brief Creates the chart on log.html the shows
 *          the output of the capture series that the
 *          user selected.
 * @param {type} captureID
 * @param {type} keepToggleVals
 * @returns {undefined}
 */
function captureChart(captureID, keepToggleVals) {

  // zero all of the arrays, update the div
  destroyChart("log", "log-chart");

  // reset the page
  $("#log-chart").html("Loading chart, please wait...");

  // keepToggleVals is an optional parameter, only
  //  included if this function is called by logToggle.
  //  If argument is not supplied we need to reset the
  //  global vals and the checkboxes
  if(!keepToggleVals){
    logToggleTemp = true;
    logToggleHumidity = true;
    logTogglePressure = true;
    logToggleLight = true;
    logToggleCo = true; 
    $("#log-chart-check-temperature").prop("checked",true);
    $("#log-chart-check-humidity").prop("checked",true);
    $("#log-chart-check-pressure").prop("checked",true);
    $("#log-chart-check-light").prop("checked",true);
    $("#log-chart-check-co").prop("checked",true);
    // hide all of the checkboxes
    $("#log-chart-toggle-temperature").hide();
    $("#log-chart-toggle-humidity").hide();
    $("#log-chart-toggle-light").hide();
    $("#log-chart-toggle-pressure").hide();
    $("#log-chart-toggle-co").hide();
  }

  if (captureID) {
    // if has argurment set new global value
    globalCaptureID = captureID;
    // highlight the row with this ID, reset all other back to black
    $("#capture-" + captureID).css("color", "blue").siblings().css("color", "black");
  }
  var chartRecords;
  var parsedRecords;

  // get my UTZ offset
  var myTZOffset = new Date();
  myTZOffset = myTZOffset.getTimezoneOffset() * 60 * 1000;

  //var counter = 1;
  // need to give the elements a chance to refresh
  //  so place all of the backend and charting functions
  //  in a setTimeout, so that they will run last
  setTimeout(function () {
    $("#log-chart").html("&nbsp;");
    chartRecords = ClimateLogger.getChartDataJSON(globalCaptureID);
    parsedRecords = JSON.parse(chartRecords);

    if (parsedRecords.length > 0) {
      // returned at least one record, so get the sensors
      //  to show the checkboxes we want.  We only need
      //  to look at the first record, as they're all the same
      for (var key in parsedRecords[0]) {
        if (key === "Temp") {
          $("#log-chart-toggle-temperature").show();
        } else if (key === "Humidity") {
          $("#log-chart-toggle-humidity").show();
        } else if (key === "Pressure") {
          $("#log-chart-toggle-pressure").show();
        } else if (key === "Light") {
          $("#log-chart-toggle-light").show();
        } else if (key === "CO") {
          $("#log-chart-toggle-co").show();
        }
      }

      // now go through each record and load up the arrays
      for (var i = 0; i < parsedRecords.length; i++) {
        // have to get the timestamp first
        for (var key in parsedRecords[i]) {
          if (key === "Date") {
            // database records are stored in Unix seconds, but the
            //  chart needs to have it in milliseconds            
            var curDate = (parsedRecords[i][key] * 1000) - myTZOffset;
          }
        }
        for (var key in parsedRecords[i]) {
          if (key === "Temp" && logToggleTemp) {
            logChartTemp.push([curDate, parsedRecords[i][key]]);
          } else if (key === "Humidity" && logToggleHumidity) {
            logChartHumidity.push([curDate, parsedRecords[i][key]]);
          } else if (key === "Pressure" && logTogglePressure) {
            logChartPressure.push([curDate, parsedRecords[i][key]]);
          } else if (key === "Light" && logToggleLight) {
            logChartLight.push([curDate, parsedRecords[i][key]]);
          } else if (key === "CO" && logToggleCo) {
            logChartCO.push([curDate, parsedRecords[i][key]]);

          }
        }
        //counter++;
      }
      // show the chart
      //var chartDataSet = [logChartTemp, logChartHumidity, logChartPressure, logChartLight, logChartCO];
      showChart("log", "log-chart");
    } else {
      // no records returned, message given in getChartDataJSON,
      //  so no need to give another here
      // remove for production
      ClimateLogger.displayMessage("No data in this series");
    }
  }, 100);
}

function logToggle(whichID) {
  if (whichID === "temperature") {
    logToggleTemp = !logToggleTemp;
  } else if (whichID === "humidity") {
    logToggleHumidity = !logToggleHumidity;
  } else if (whichID === "pressure") {
    logTogglePressure = !logTogglePressure;
  } else if (whichID === "light") {
    logToggleLight = !logToggleLight;
  } else if (whichID === "co") {
    logToggleCo = !logToggleCo;
  } else {
    // should never get here, but if it does
    //  we'll reset everything to visible    
    ClimateLogger.displayMessage("Toggle ID: " + whichID);
    logToggleTemp = true;
    logToggleHumidity = true;
    logTogglePressure = true;
    logToggleLight = true;
    logToggleCo = true;
  }
  var keepToggleVals = true;
  captureChart(globalCaptureID, keepToggleVals);
}