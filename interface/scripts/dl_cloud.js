/* 
 * File:    dl_header.js
 *
 * Created: March, 2017
 * Purpose: Supports cloud.html
 * 
 */


function postLocalDBToCloud() {
  //alert("Not ready yet...");
  ClimateLogger.postLocalDB();
  
  //alert("Upload complete");
}

function updateLogUploadCount(){
  $("#log-upload-count").html("&nbsp;");
  $("#log-upload-pbar").html("&nbsp;");
  var logCount = 0;
  var logTotalCount = 0;
  var logPcnt = 0;
  logCount = ClimateLogger.getLogUpdateCount();
  logTotalCount = ClimateLogger.getLogTotalCount();
  if(logTotalCount > 0){
    logPcnt = Math.floor((logCount/logTotalCount)*100);
  }
  var logHtml = "Log upload count: " + logCount + " of " + logTotalCount;

  $("#log-upload-count").html(logHtml);
  var logPbar = '<div class="progress">';
      logPbar += '<div class="progress-bar" role="progressbar" ';
      logPbar += 'aria-valuenow="' + logPcnt + '" aria-valuemin="0" ';
      logPbar += 'aria-valuemax="100"';
      logPbar += ' style="width:'+ logPcnt + '%">' + logPcnt;
      logPbar += '%</div></div>';
  //alert(logPbar);
  $("#log-upload-pbar").html(logPbar);
} 