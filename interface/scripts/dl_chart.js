/*
 dl_chart.js
 Purpose       Functions to display chart on datalogger index page

 Created on:    11/18/16
 Modified on:
 */

/*******************************************************************************
 * Globals
 * The arrays that hold the data are kept in grouped sets, as the dashboard
 *  page and the log page may both be active at the same time, and we don't
 *  want to over-write one with the other
 */

// dashboard chart variables, these are arrays that hold the data points
//  for the dynamic dashboard chart.  Fed by the timer function
var dashboardChartTemp = [];
var dashboardChartHumidity = [];
var dashboardChartLight = [];
var dashboardChartPressure = [];
var dashboardChartCO = [];

 // counts timer clicks, controls chart scrolling
var dashboardChartCounter = 1;

// some sensors start at weird values, give chance to stabilize
var okToChart = 0;

// wait this many seconds to load the chart
//  Set to 3 in onConnect function in dl_timer.js, so that
//  the first loading of the chart is delayed long enough
//  for the light sensor to stabilize.
//  Is then set to 0 in destroyChart function so that there is not
//  as much of a delay when the units or chart data
//  options are changed
var dashboardChartDelay = 3;


// log chart variables,these are arrays that hold the data points
//  for the fixed log charts.  Updated in dl_logs.js.capture_chart
var logChartTemp = [];
var logChartHumidity = [];
var logChartLight = [];
var logChartPressure = [];
var logChartCO = [];
var logChartADC4 = [];

// global reference to the plots
var dashboardChart;
var logChart;

/******************************************************************************/

/**
 * 
 * @param {type} whichPage - page calling the chart, must
 *                            be 'log' or 'dashboard'
 * @param {type} divID  -    id of div where the chart is to be placed
 *                            (without the '#')
 * @returns {undefined}
 */
function showChart(whichPage, divID) {

  // chartData is array of data arrays
  var chartData = [];       
  // y-axis array
  var yAxis = [];
  // x-axis array
  var xAxis = [];
  // y-axis start on left, then move to right after first one
  var yAxisLocation = 'left';
  // set up the format for the x-axis
  var timeFormat;
  // counts the y-axis, so knows when to start placing on right
  var axisCounter = 1;      
  // These are objects that hold the arrays to be graphed
  //  This was done so that we could use the same graphing code
  //  for both pages that call the graphing function
  var chartTemp;            
  var chartHumidity;
  var chartLight;
  var chartPressure;
  var chartCO;
  
  // set up the correct arrays to chart
  if(whichPage === "log"){
    chartTemp = logChartTemp;
    chartHumidity = logChartHumidity;
    chartLight = logChartLight;
    chartPressure = logChartPressure;
    chartCO = logChartCO;
    timeFormat = "%b.%e.%Y<br />%I:%M%P";

  }else{
    chartTemp = dashboardChartTemp;
    chartHumidity = dashboardChartHumidity;
    chartLight = dashboardChartLight;
    chartPressure = dashboardChartPressure;
    chartCO = dashboardChartCO;
    timeFormat = "%I:%M%p<br />:%S Secs";

  }
  
  // test to see if array have values, if so add to data array
  if(chartTemp.length > 0){
    var tempLabel = "Temperature (" + tempUnit + ")";
    //alert(tempLabel);
    chartData.push({
      data:chartTemp,
      label:tempLabel,
      yaxis:axisCounter,
      color:'#DC143C' //crimson
    });
    if (axisCounter > 1){
      yAxisLocation = 'right';
    }
    yAxis.push({
      axisLabel:tempLabel,
      tickDecimals: 0,
      position:yAxisLocation
    });
    axisCounter++;
  }
  if(chartHumidity.length > 0){
    chartData.push({
      data:chartHumidity,
      label:'Humidity (%)',
      yaxis:axisCounter,
      color: '#00008B' // darkblue
    });
    if (axisCounter > 1){
      yAxisLocation = 'right';
    }
    yAxis.push({
      axisLabel: 'Humidity (%)',
      tickDecimals: 0,
      position:yAxisLocation
    });
    axisCounter++;
  }
  if(chartLight.length > 0){
    chartData.push({
      data:chartLight,
      label:'Light (lux)',
      yaxis:axisCounter,
      color: '#FF8C00' // darkorange
    });
    if (axisCounter > 1){
      yAxisLocation = 'right';
    }
    yAxis.push({
      axisLabel: 'Light (lux)',
      tickDecimals: 0,
      position:yAxisLocation
    });
    axisCounter++;
  }
  if(chartPressure.length > 0){
    var pressureTicks = 0;
    if(pressureUnit==="inHg"){
      // if it's in inHg set the tick decimals
      //  to 2 so that shows more detail
      pressureTicks = 2;
    }else if(pressureUnit==="mBar"){
      pressureTicks = 1;
    }
    var pressureLabel = "Pressure (" + pressureUnit + ")";
    chartData.push({
      data:chartPressure,
      label:pressureLabel,
      yaxis:axisCounter,
      color:'#006400' // darkgreen
    });
    if (axisCounter > 1){
      yAxisLocation = 'right';
    }
    yAxis.push({
      axisLabel: pressureLabel,
      tickDecimals: pressureTicks,
      position:yAxisLocation
    });
    axisCounter++;
  }
    if(chartCO.length > 0){
    chartData.push({
      data:chartCO,
      label:'CO (ppm)',
      yaxis:axisCounter,
      color:'#00BFFF' // deepskyblue
    });
    if (axisCounter > 1){
      yAxisLocation = 'right';
    }
    yAxis.push({
      axisLabel: 'CO (ppm)',
      tickDecimals: 0,
      position:yAxisLocation
    });
    axisCounter++;
  }
  
  // set up x axis
  xAxis.push({mode:'time',timeformat: timeFormat, ticks: 3});
  
  // load the options
  var options = {
            legend: {labelBoxBorderColor: "none", position: "se"},
            yaxes: yAxis,
            xaxes: xAxis
          };

  // finally show the chart
  if(whichPage == "dashboard"){
    dashboardChart = $.plot($("#" + divID), chartData , options);
  }else{
    logChart = $.plot($("#" + divID), chartData , options);
  }
};

/**
 * Clear the charts, clear memory, reset the chart and the div
 * @param {type} whichPage
 * @param {type} divID
 * @returns {undefined}
 */
function destroyChart(whichPage, divID){
  
  var data = "";
  var options = "";
  if(whichPage === "dashboard"){
    dashboardChartDelay = 0;
    resetDashboardChart();
    dashboardChart = $.plot($("#" + divID), data, options); 
    dashboardChart.destroy();
    // reset the counter, but don't need to wait as long
    //  as we do on the initial load
    //dashboardChartCounter = 2;
    okToChart = 0;
  }else{
    resetLogChart();
    logChart = $.plot($("#" + divID), data , options);
    logChart.destroy();
  }    
}

/**
 * Resets the dashboard chart
 * @returns {undefined}
 */
function resetDashboardChart() {
  // reset all of the chart arrays, reset counter to 1
  //  * should find way to cycle through all chart arrays
  //  * instead of hardcoding this.  Object of arrays?
  dashboardChartTemp.length = 0;
  dashboardChartHumidity.length = 0;
  dashboardChartLight.length = 0;
  dashboardChartPressure.length = 0;
  dashboardChartCO.length = 0;
  dashboardChartCounter = 1;  
};

/**
 * Resets the log chart
 * @returns {undefined}
 */
function resetLogChart() {
  // reset all of the chart arrays, reset counter to 1
  //  * should find way to cycle through all chart arrays
  //  * instead of hardcoding this.  Object of arrays?  
  //alert("works to here");
  logChartTemp.length = 0;
  logChartHumidity.length = 0;
  logChartLight.length = 0;
  logChartPressure.length = 0;
  logChartCO.length = 0;
};
