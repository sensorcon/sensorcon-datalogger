/* 
 * File:    dl_calibrate.js
 *
 * Created: January, 2017
 * Purpose: Supports calibrate.html
 * 
 */
  function userZero() {
    userZeroFlag = 1;
    document.getElementById("zero-bar-div").style.display = "block";
    document.getElementById("zero-success-div").style.display = "none";
    document.getElementById("zero-fail-div").style.display = "none";
    //document.getElementById("nav-menu").style.visibility = "hidden";
    ClimateLogger.coZero();
  }


  function getBaseline() {
    baseline = ClimateLogger.getCoBaseline();
    baselineInst = ClimateLogger.getCoBaselineInst();
  }

  function getSensitivity() {
    sensitivity = ClimateLogger.getCoSensitivity();
    sensitivityInst = ClimateLogger.getCoSensitivityInst();
  }

  function showValues() {
    document.getElementById("zero-input1").value = baseline;
    document.getElementById("sens-input1").value = sensitivity;

    if (baselineInst == -1823) {
      document.getElementById("zero-input3").value = "--";
    } else {
      document.getElementById("zero-input3").value = baselineInst;
    }

    if (sensitivityInst == 65535) {
      document.getElementById("sens-input3").value = "--";
    } else {
      document.getElementById("sens-input3").value = sensitivityInst;
    }
  }

  function readCO() {
    
    var coVal = ClimateLogger.readCO();
    var coValText = "Current CO reading: ";
    if (coVal == "Error") {
      coValText += " Error";
    } else if (coVal == "-OL") {
      coValText += "-OL";
    } else if (coVal == "OL") {
      coValText += "OL";
    } else {      
      coValText += coVal + " PPM";
    }
    $("#co-val").html(coValText);
  }

      function checkZeroStat() {
        zeroStat = ClimateLogger.checkCoZeroStatus();
        if (zeroStat == 1) {

          // Set this to 1 to alert software that it needs to update sensitivity when complete
          if (zeroTrigger == 0) {
            zeroTrigger = 1;
          }
        } else {
          // If baseline just completed, show the temp value
          if (zeroTrigger == 1) {
            zeroTrigger = 0;
            showBaselineTemp = true;

            var zeroErr = ClimateLogger.checkCoZeroError();

            if (coVersion == true) {
              userZeroFlag = 0;
              progressVal = 0;
              document.getElementById("zero-bar-div").style.display = "none";

              if (zeroErr == 1) {
                document.getElementById("zero-fail-div").style.display = "block";
                ClimateLogger.clearCoZeroError();
              } else {
                document.getElementById("zero-success-div").style.display = "block";
              }
            }
          }
        }
      }


  function checkCalStat() {
    calStat = ClimateLogger.checkCoCalStatus();
    if (calStat == 1) {

      // Set this to 1 to alert software that it needs to update sensitivity when complete
      if (calTrigger == 0) {
        calTrigger = 1;
      }
    } else {

      // If baseline just completed, show the temp value
      if (calTrigger == 1) {
        calTrigger = 0;
        ClimateLogger.coSendCalTimestamp();
      }
    }
  }

  function getInstBaseline() {
    ClimateLogger.getInstBaseline();
  }
  function getInstSensitivity() {
    ClimateLogger.getInstSensitivity();
  }
  function writeBaseline() {
    ClimateLogger.writeCOBaseline();
  }
  function writeSensitivity() {
    ClimateLogger.writeCOSensitivity();
    ClimateLogger.coSendCalTimestamp();
  }
  function autoZero() {
    ClimateLogger.coZero();
  }

