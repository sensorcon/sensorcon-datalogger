/* 
 * File:    dl_dashboard.js
 * 
 * Created: January 25, 2017
 * Purpose: Supports dashboard.html
 * 
 */

// Set global overload constants here
// If these get changed the code in
//  DatabaseHelper::getChartDataJSON also needs
//  to get changed, as overload values were set
//  there to convert for the log charts
var olTempC = 125;
var olTempF = 257;
var olNegTempC = -40;
var olNegTempF = -40;
var olHumidity = 100;
var olPressurePa = 110000;
var olPressureMbar = 1100;
var olPressureInHg = 32;
var olNegPressurePa = 20000;
var olNegPressureMbar = 200;
var olNegPressureInHg = 6;
var olLight = 20000;
var olCo = 500;
var olNegCo = -30;

// This is set up the default start up
//  data sets that we want to see on the
//  dashboard page.
// IMPORTANT!
//  if you change these here you also need
//  to change the checked values to match
//  in the dashboard.html page
var dbToggleTemp = true;
var dbToggleHumidity = true;
var dbTogglePressure = false;
var dbToggleLight = false;
var dbToggleCo = false;

var showPressure = false;
var tempDelay = 1;
var pressureDelay = 1;
/**
 * @brief   readSensors runs every second
 *          Called by index.html under periodicTask
 *          This function updates the sensor outputs
 *            and fills the arrays for the chart on the
 *            dashboard page.  The arrays are only filled
 *            if the checkbox for that series is checked
 * @returns {undefined}
 */
function readSensors() {
  // If we're downloading from the device just quit
  //    right away.  This is set in dl_logs.js -> downloadLogs()
  if(downloadingFromDevice){
    return false;
  }
  
  // get the current date / time in milliseconds
  curDate = moment(new Date()).valueOf();
  // get my UTC offset
  var myTZOffset = new Date();
  // getTimezoneOffet returns difference between UTC]
  //  and local time in minutes (convert to milliseconds)
  myTZOffset = myTZOffset.getTimezoneOffset() * 60 * 1000;
  curDate = curDate - myTZOffset;

  // variables to store sensor values and labels 
  var sensorVal;
  var sensorLabel;
  if (temperatureSensor === true) {
    sensorVal = 0;
    sensorLabel = "";
    // increment the tempDelay
    tempDelay++;
    
    var tempVal = ClimateLogger.readTemperature();
    // Check overload
    if (tempVal === "-OL") {
      // if negative overload use the global const values
      if (tempUnit === "F") {
        sensorVal = olNegTempF;
      } else {
        sensorVal = olNegTempC;
      }
      sensorLabel = "-OL";
    } else if (tempVal === "OL") {
      // if overload use the global const values
      if (tempUnit === "F") {
        sensorVal = olTempF;
      } else {
        sensorVal = olTempC;
      }
      sensorLabel = "OL";
    } else {
      sensorVal = tempVal;
      sensorLabel = tempVal + " &#176;" + tempUnit;
    }
    // have a problem with the temperature sensor label updating
    //  to the new radio button selection, while it takes a
    //  second for the value to update, causing formating problems
    // This forces a 1 second delay before showing the new value, is
    //  reset in dl_unit.js when the option is changed
    if(tempDelay < 2){
      sensorLabel = "&nbsp;" + tempUnit;
    }else{
      tempDelay = 1;
    }
    // add the hyperlink to the message about high temps
    sensorLabel = "<a href='javascript:showTempMsg()'>"
                + sensorLabel + "</a>";
    // show value in sensor div
    $("#temp").html(sensorLabel);
    
    // push into the temp array for the chart
    if (okToChart && dbToggleTemp === true) {
      dashboardChartTemp.push([curDate, sensorVal]);
    }
  }
  if (humiditySensor === true) {
    sensorVal = 0;
    sensorLabel = "";
    var humVal = ClimateLogger.readHumidity();
    // Check overload
    if (humVal === "OL") {
      // if overload use the global const values
      sensorVal = olHumidity;
      sensorLabel = "OL";
    } else {
      sensorVal = humVal;
      sensorLabel = humVal + " %";
    }
    // show value in sensor div
    $("#humidity").html(sensorLabel);
    
    // push into the humidity array for the chart
    if (okToChart && dbToggleHumidity === true) {
      dashboardChartHumidity.push([curDate, sensorVal]);
    }
  }
  if (pressureSensor === true) {
    // increment the sensorDelay
    pressureDelay++;
    
    var pressureVal = 0;
    pressureVal = ClimateLogger.readPressure();
    sensorVal = 0;
    sensorLabel = ""
    // Check overload
    if (pressureVal === "-OL") {
      // if negative overload use the global const values
      if (pressureUnit === "Pa") {
        sensorVal = olNegPressurePa;
      } else if (pressureUnit === "inHg")
        sensorVal = olNegPressureInHg;
      else if (pressureUnit === "mBar") {
        sensorVal = olNegPressureMbar;
      }
      sensorLabel = "-OL";
    } else if (pressureVal === "OL") {
      // if overload use the global const values
      if (pressureUnit === "Pa") {
        sensorVal = olPressurePa;
      } else if (pressureUnit === "inHg")
        sensorVal = olPressureInHg;
      else if (pressureUnit === "mBar") {
        sensorVal = olPressureMbar;
      }
      sensorLabel = "OL";
    } else {
      sensorVal = pressureVal;
      sensorLabel = sensorVal + " " + pressureUnit;
    }
      
    // Have a problem with the pressure sensor label updating
    //  to the new radio button selection, while it takes a
    //  second for the value to update, causing formating problems
    // This forces a 1 second delay before showing the new value, is
    //  reset in dl_unit.js when the option is changed
    if(pressureDelay < 2){
      sensorLabel = "&nbsp;" + pressureUnit;
    }else{
      pressureDelay = 1;
    }     
    // show value in sensor div
    $("#pressure").html(sensorLabel);
    
    // push into the pressure array
    if (okToChart && dbTogglePressure === true) {
      dashboardChartPressure.push([curDate, sensorVal]);
    }
  }
  if (lightSensor === true) {
    sensorVal = 0;
    sensorLabel = "";
    var lightVal = ClimateLogger.readLight();
    // Check overload.  Needed to add the comparison of the 
    //  lightVal, as sensor will sometimes jump the olLight limit
    //  and give some very high readings.
    if (lightVal === "OL" || lightVal > olLight) {
      // if overload use the global const values
      sensorVal = olLight;
      sensorLabel = "OL";
    } else {
      sensorVal = lightVal;
      sensorLabel = lightVal + " lux";
    }
    // show value in sensor div
    $("#light").html(sensorLabel);
    
    // push into the light array
    if (okToChart && dbToggleLight === true) {
      dashboardChartLight.push([curDate, sensorVal]);
    }
  }

  if (coSensor === true) {
    sensorVal = 0;
    sensorLabel = "";
    var coVal = ClimateLogger.readCO();
    if (coVal === "Error") {
      // if error put out the neg limit value
      sensorVal = olNegCo;
      sensorLabel = "Error";
    } else if (coVal === "-OL") {
      // if negative overload use the global const values
      sensorVal = olNegCo
      sensorLabel = "-OL";
    } else if (coVal === "OL") {
      // if overload use the global const values
      sensorVal = olCo;
      sensorLabel = "OL";
    } else {
      sensorVal = coVal;
      sensorLabel = coVal + " PPM";
    }
    // show value in sensor div
    $("#co").html(sensorLabel);    
    
    // push into the co array
    if (okToChart && dbToggleCo === true) {
      dashboardChartCO.push([curDate, sensorVal]);
    }
  }
  // adc4 is for debugging only, shouldn't be included in the chart
  if (adc4 === true) {
    sensorVal = 0;
    sensorLabel = "";
    var adc4Val = ClimateLogger.readADC4();
    // debugging only
    //var adc4Val = 253;
    sensorVal = adc4Val;
    sensorLabel = adc4Val;
    $("#adc4").html(adc4Val);
    // not pushed to chart array
  }


  // increment the counter
  dashboardChartCounter++;
  

  // If the counter is more than 40 cut the last value from all arrays
  //  this will keep the chart moving with a total of 40 values shown
  //  If the array being shifted is empty the shift function returns
  //  undefined, which is OK here.  Shift on empty array will not break
  //  the code, so no need to check if series is active
  if (dashboardChartCounter > 40) {
    dashboardChartTemp.shift();
    dashboardChartHumidity.shift();
    dashboardChartPressure.shift();
    dashboardChartLight.shift();
    dashboardChartCO.shift();
  }

  // wait dashboardChartDelay counter clicks for
  //  sensors to stabilize
  if (okToChart) {    
    $("#dashboard-chart").text("");
    showChart("dashboard", "dashboard-chart");
  } else {
    if (dashboardChartCounter <= dashboardChartDelay) {
      // still waiting, give user feedback in chart div
      $("#dashboard-chart").html("Loading chart, please wait...");
    } else {
      // change flag, will take one more cycle to load chart
      okToChart = !okToChart;
    }
  }
}

/**
 * @brief Toggles the global variables used
 *        to determine which data series should
 *        be included in the dashboard chart
 * @param {type} whichID - series to toggle
 * @returns {undefined}
 */
function dashboardToggle(whichID) {
  if (whichID === "temperature") {
    dbToggleTemp = !dbToggleTemp;
  } else if (whichID === "humidity") {
    dbToggleHumidity = !dbToggleHumidity;
  } else if (whichID === "pressure") {
    dbTogglePressure = !dbTogglePressure;
  } else if (whichID === "light") {
    dbToggleLight = !dbToggleLight;
  } else if (whichID === "co") {
    dbToggleCo = !dbToggleCo;
  } else {
    // should never get here, but if it does
    //  we'll reset everything to visible
    ClimateLogger.displayMessage("Data series: " + whichID);
    dbToggleTemp = true;
    dbToggleHumidity = true;
    dbTogglePressure = true;
    dbToggleLight = true;
    dbToggleCo = true;
  }
  // reset the chart so that it's showing the right series
  destroyChart("dashboard", "dashboard-chart");
}

function showTempMsg(){
  var strTitle = "Temperature"
  var strMsg = "";
  strMsg = "Temperature values will be slightly higher than normal,\n";
  strMsg += "and humidity values will be slightly lower than normal\n";
  strMsg += "while charging / connected over USB.";
  ClimateLogger.displayMessage(strMsg, strTitle);
}
