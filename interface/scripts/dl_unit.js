/*
 dl_unit.js
 Purpose       Functions for the unit radio buttons on the left navbar
 
 /******************************************************************************/

function unitSetUpRadioButtons() {
  // hide temp and pressure sections
  document.getElementById("temperature-unit-div").style.visibility = "hidden";
  document.getElementById("pressure-unit-div").style.visibility = "hidden";

  // set the correct temperature radio buttons as checked based on the
  //   unit selection that the user had selected from the previous use
  if (ClimateLogger.getTemperatureUnit() == 1) {
    // user had fahrenheit selected from last use
    $("#f-rb").attr("checked", true);
    $("#c-rb").attr("checked", false);
  } else {
    // user had celsius selected from last use
    $("#f-rb").attr("checked", false);
    $("#c-rb").attr("checked", true);
  }

  // set the correct pressure radio buttons as checked based on the
  //   unit selection that the user had selected from the previous use
  var pressureUnit = ClimateLogger.getPressureUnit();
  if (pressureUnit == 1) {
    // user had inHg stored from last use
    $("#pressure-rb1").attr("checked", true);
    $("#pressure-rb2").attr("checked", false);
    $("#pressure-rb0").attr("checked", false);
  } else if (pressureUnit == 2) {
    // user had Pa stored from last use
    $("#pressure-rb1").attr("checked", false);
    $("#pressure-rb2").attr("checked", true);
    $("#pressure-rb0").attr("checked", false);
  } else {
    // user had mBar selected from last use
    //  also here as a default value, in case
    //  QT spits out something other than a 0
    //  on the first use
    $("#pressure-rb1").attr("checked", false);
    $("#pressure-rb2").attr("checked", false);
    $("#pressure-rb0").attr("checked", true);
  }
}

function unitOnConnect() {
  if (temperatureSensor == true) {
    document.getElementById("temperature-unit-div").style.visibility = "visible";
  }
  if (pressureSensor == true) {
    document.getElementById("pressure-unit-div").style.visibility = "visible";
  }
}

function changeTempUnits(tUnit) {
  var storedTValue;
  // reset the delay, see readSensors
  //  in dl_dashboard.js for details
  tempDelay = 0;

  if (tUnit === "F") {
    storedTValue = 1;
  } else if (tUnit === "C") {
    storedTValue = 0;
  } else {
    // safety valve
    storedTValue = -1;
  }
  // Store the selection in QT's storage
  ClimateLogger.storeTemperatureUnit(storedTValue);

  // Set the global variable
  tempUnit = tUnit;

  // reset the dashboard chart when this changes
  destroyChart("dashboard", "dashboard-chart");

  // reset the capture chart when this changes
  captureChart(globalCaptureID, true);
}


function changePressureUnits(pUnit) {
  var storedPValue;
  // reset the delay, see readSensors
  //  in dl_dashboard.js for details
  pressureDelay = 0;

  if (pUnit === "inHg") {
    storedPValue = 1;
  } else if (pUnit === "Pa") {
    // has Pa selected
    storedPValue = 2;
  } else {
    // this is mBar, but on read and write from the back end
    //  anything that is not 1 or 2 is treated as 0
    storedPValue = 0;
  }
  // Store the selection in QT's storage
  ClimateLogger.storePressureUnit(storedPValue);

  // Set the global variable
  pressureUnit = pUnit;

  // reset the dashboard chart when this changes
  destroyChart("dashboard", "dashboard-chart");

  // reset the capture chart when this changes
  captureChart(globalCaptureID, true);
}