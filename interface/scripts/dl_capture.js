/* 
 * File:    dl_capture.js
 *
 * Created: January 16, 2017
 * Purpose: Supports capture.html
 * 
 * Notes:   Date formatting handled using libraries
 *          date-format.js (http://blog.stevenlevithan.com/archives/date-time-format)
 *          moment.js (http://momentjs.com/)
 *          datetimepicker (http://www.malot.fr/bootstrap-datetimepicker/)
 *            ** DO NOT USE INSTRUCTION FROM BELOW, WON'T WORK!
 *              **  https://eonasdan.github.io/bootstrap-datetimepicker/
 */

/*******************************************************************************
 * Globals
 */
var MAX_CALENDAR_DATE = "2038-01-19 03:14:17";
var sliderStartGlobal = 0;
var sliderEndGlobal = 0;

/******************************************************************************/


/*
 * Load current date into start date field
 * if start now checkbox isn't selected
 */
function loadStartDate() {
  var curDate = new Date();
  var curDateFormatted = dateFormat(curDate, "yyyy-mm-dd HH:MM:ss");
  var startDateText = $("#start-date-text");
  // if nothing in start date default to right now
  if (startDateText.val() === "") {
    startDateText.val(curDateFormatted);
  }
}
;

function loadEndDate() {
  // get the start date
  var startTimeText = $("#start-date-text").val();
  // get the interval in seconds
  var intervalSeconds = getIntervalInSeconds();
  // get the max date based on start time and the interval
  var endDate = getMaxDate(startTimeText, intervalSeconds);
  // check to see if the get max checkbox is checked
  var endMaxIsChecked = ($('#end-max').prop('checked'));
  // if end-max is checked update end-date no matter what
  if(endMaxIsChecked){
    // update the end date
    $('#end-date-text').val(endDate);
    // also enter it into the summary end div, only if not currently logging
    if(loggingStatus ===0){
      $('#capture-summary-end').text(endDate);
    }
  }else{
    // Only update the end date if it's greater than the max date.
    //  This makes sure we don't over extend the units max logs
    if(startTimeText > endDate){
      // update the end date
      $('#end-date-text').val(endDate);
      // also enter it into the summary end div, only if not currently logging
      if(loggingStatus ===0){
        $('#capture-summary-end').text(endDate);
      }
    }
  }
}
;

/*
 * Get the maximum date, based on the start
 *     date and interval arguments.  Adds the
 *     interval to the start date, up to the
 *     global constant MAX_CALENDAR_DATE
 * @param   {type} beginDateText - date to start
 * @param   {type} intervalInSeconds - interval
 * @returns {String|endTimeText}
 *          string representation of the 
 */
function getMaxDate(beginDateText, intervalInSeconds) {
  // good place to reset an errors or warnings
  $('#end-date-validate').html('&nbsp;');
  
    // reset log error message if there
  //$("#log-button-validate").html("&nbsp;");
  
  // declare start time
  var unixStartTime;
  
  // should never see this, but start with a default value
  var endTimeText = "0000-00-00 00:00:00";

  // First look to see ClimateLogger.getMaxEndTime will
  //  return a time stamp that exceeds the C++ long int max.
  //  This will cause the calendar to fail.

  // Get the max unix date of long integer value, -1
  var unixMaxDate = 2147483646;

  // Get the current unix timestamp
  //var curUnixDate = moment().unix();
  
  // Get the begin date unix timestamp
  var beginUnixDate = moment(beginDateText).unix();

  // Add (interval * maxLogs) to the current timestamp to get the max end date
  var unixMaxTest = beginUnixDate + (intervalInSeconds * ClimateLogger.getMaxLogs());
  //alert("unixMaxTest: " + unixMaxTest + "\nunixMaxDate: " + unixMaxDate);
  // If it exceeds the max date just default to the fail date
  if (unixMaxTest >= unixMaxDate) {
    unixStartTime = "";
  } else {
    // convert begin date text to a date
    var startDate = new Date(moment(beginDateText));

    // convert that to a unix timestamp
    unixStartTime = moment(startDate).unix();
  }  
  
  // Should handle any failures and unix 32 bit epoch problem
   if (unixStartTime !== "") {
    // get the maximum end date from ClimateLogger
    var secondsToEnd = ClimateLogger.getMaxEndTime(unixStartTime, intervalInSeconds);

    // convert all of those seconds to a date
    var endDate = new Date(moment.unix(secondsToEnd));

    // format it correctly for the screen
    // if user has selected date will be HH:MM
    if ($('#start-now').prop('checked')) {
      endTimeText = dateFormat(endDate, "yyyy-mm-dd HH:MM:ss");
    } else {
      endTimeText = dateFormat(endDate, "yyyy-mm-dd HH:MM");
    }
  } else {
    // default max date/time
    endTimeText = MAX_CALENDAR_DATE;
    
    // give user feedback
    $("#end-date-validate").html("Max end date reached");
    
  }

  // look at value currently in end-date-text, if is a later date
  //  than this end date it should be updated to the new max date,
  //  otherwise is confusing
  var curEndTimeText = $('#end-date-text');
  if (endTimeText < curEndTimeText.val()) {
    curEndTimeText.val(endTimeText);
  }

  // return max date text  
  return endTimeText;
}
;

/*
 * Returns the interval selected in seconds
 * @returns {Element.value|Number} interval in seconds
 */
function getIntervalInSeconds() {
  var intValue = 0;
  var interval = $('#interval').val();
  var timeFactor = $("input[type='radio'][name='interval-radios']:checked").val();
  
  if (timeFactor === "seconds") {
    intValue = interval;
  } else if (timeFactor === "minutes") {    
    intValue = interval * 60;
  } else if (timeFactor === "hours") {
    intValue = interval * 3600;
  } else if (timeFactor === "days") {
    // the days option has been removed on the html page,
    //  this should not get hit anymore but leaving it
    //  in case it is somehow referenced somewhere else
    intValue = interval * 86400;
  } else {
    // just return the seconds
  }
  // can occasionally get to float type, so force it
  //  to be an integer value
  return Math.floor(intValue);
}
;

/*
 * Takes the intervalSeconds and returns
 * object with days,hours,minutes, and seconds
 * @param   {type} interval in seconds
 * @returns {object}
 *            .intervalYears
 *            .intervalDays
 *            .intervalHours
 *            .intervalMinutes
 *            .intervalSeconds
 */
function formatIntervalSeconds(intervalSeconds) {
  var intervalYears = 0;
  var intervalDays = 0;
  var intervalHours = 0;
  var intervalMinutes = 0;
  var intervalObj = new Object();

  // get the number of years
  if (intervalSeconds >= 31536000) {
    intervalYears = Math.floor(intervalSeconds / 31536000);
    intervalSeconds -= intervalYears * 31536000;
  }

  // get the number of days
  // technically this is wrong, because I'm not counting
  // leap years, but it should be close enough for us
  if (intervalSeconds >= 86400) {
    intervalDays = Math.floor(intervalSeconds / 86400);
    intervalSeconds -= intervalDays * 86400;
  }
  // get the number of hours
  if (intervalSeconds >= 3600) {
    intervalHours = Math.floor(intervalSeconds / 3600) % 24;
    intervalSeconds -= intervalHours * 3600;
  }
  // get the number of minutes
  if (intervalSeconds >= 60) {
    intervalMinutes = Math.floor(intervalSeconds / 60) % 60;
    intervalSeconds -= intervalMinutes * 60;
  }
  
  // seconds will be what's leftover


  intervalObj.intervalYears = intervalYears;
  intervalObj.intervalDays = intervalDays;
  intervalObj.intervalHours = intervalHours;
  intervalObj.intervalMinutes = intervalMinutes;
  intervalObj.intervalSeconds = intervalSeconds;

  return intervalObj;
}
;

/* The start date has been updated,
 * or the interval radio or text box has changed,
 * so need to check some things
 * @returns {undefined}
 */
function startDateOrIntervalUpdate() {

  // check the interval first, as will crash
  // other functions if the interval is not valid
  if(isValidInterval()){
    // update the end calendar dropdown
    updateEndCalendar();

    // update the split field in the summary field
    updateIntervalSplit();

    // update the max capacity
    var intervalInSeconds = getIntervalInSeconds();
    // get the max capacity of the unit in seconds
    updateCapacity(intervalInSeconds, "max");


    /******************************************
     * Removed per review meeting 1/8/17 *

    // update the capture rate;

    updateCaptureRate();

    *****************************************/


    // update the actual run time
    // this is handled by the periodic tasks in index    
      updateActualRunTime();      
    
    // make sure it's a valid date
    if (isValidDate("start")) {
      // update the start date in the summary field
      var startDateText = $('#start-date-text').val();
      // only update the summary if not currently logging
      if(loggingStatus ===0){
        $('#capture-summary-start').text(startDateText);
      }

      // if going to synchronize dates (synchDates) would do it here
    }

    // update the slider
    //updateDateSlider();
  }
}
;

function isValidInterval(){
  var isValid = true;
  var errorMsg = "&nbsp;";
  var intervalError = $("#interval-validate");
  var intervalText = $("#interval").val();
  if (!intervalText || isNaN(intervalText) || intervalText <= 0){
    isValid = false;
    errorMsg = "Not a valid interval";
  }
  intervalError.html(errorMsg);
  return isValid;
}


/*
 * Updates the max run time in the summary when the
 * page initially loads
 * @returns {undefined}
 */
function updateMaxRunTime() {
  // get the interval
  var intervalInSeconds = getIntervalInSeconds();
  // get the max capacity of the unit in seconds
  updateCapacity(intervalInSeconds, "max");
};

/*
 * Keep anything updated after the end date
 *  text field has been updated
 * @returns {undefined}
 */
function endDateUpdate() {

  // make sure the summary matches the end date
  // after it's been updated
  if (isValidDate("end")) {
    // get the start date
    var startTimeText = $("#start-date-text").val();
    // get the interval in seconds
    var intervalSeconds = getIntervalInSeconds();
    // get the max date and place it in the end date element
    var maxEndDate = getMaxDate(startTimeText, intervalSeconds);
    
    var endDate = $('#end-date-text').val();
    if(endDate < maxEndDate){
      // update summary only if not currently logging
      if(loggingStatus ===0){
        $('#capture-summary-end').text(endDate);
      }      
      updateActualRunTime();      
    }
  }
  
}
;


/**
 * @brief Updates the battery life base on intervalInSeconds
 *        Called by updateActualRunTime
 * @param actualSeconds - number of seconds to compare battery life to 
 * @returns {undefined}
 */
function updateBatteryLife(actualSeconds){
  // Get the interval and the maximum capacity
  var intervalInSeconds = getIntervalInSeconds();
  var maxCap = intervalInSeconds * ClimateLogger.getMaxLogs();
  // Reset the string
  var batteryLifeStr = "";
  var compareSeconds;
  // clear any old messages first
  // create variable for the DOM div
  var batteryDiv = $("#capture-summary-battery");
  // reset any old messages
  batteryDiv.html("&nbsp;");
  // reset css to black
  batteryDiv.css("color", "black");
  
  // get the expected battery life, should
  //  always be in years, but get days just in case
  var batteryLifeInSeconds = ClimateLogger.getEstimatedBattery(intervalInSeconds);
  if(batteryLifeInSeconds > 0){
      // split those seconds into formatted days,hrs,min,secs
    var batteryLifeObj = formatIntervalSeconds(batteryLifeInSeconds);
    if (batteryLifeObj.intervalYears > 0) {
      batteryLifeStr = batteryLifeObj.intervalYears + " years";
    } else if (batteryLifeObj.intervalDays > 0) {
      batteryLifeStr += batteryLifeObj.intervalDays + " days";
    } else{
    // should never get here, but just in case
    batteryLifeStr = "&nbsp;";
    }
    //alert("updateBatterLife called");
    if(actualSeconds > 0){
      compareSeconds = actualSeconds;
    }else{
      compareSeconds = maxCap;
    }
    if(batteryLifeInSeconds < compareSeconds){
      batteryLifeStr += " (recharge required)";
      batteryDiv.css("color", "red");
    }
  }else{
    // should never get here, but just in case
    batteryLifeStr = "&nbsp;";
  }
  
  batteryDiv.html(batteryLifeStr);
}


/*
 * update the end date calendar dropdown
 * @returns {undefined}
 */
function updateEndCalendar() {

  // validate the start date

  if (isValidDate("start")) {

    // get the starting date field
    var startingDate = $('#start-date-text').val();
    var endingDate;

    // get the interval from radio buttons and interval entered
    var interval = getIntervalInSeconds();
    // get the max date from the ClimateLogger function  
    endingDate = getMaxDate(startingDate, interval);

    // end-date beginning date will always default to start-date value,
    //  as user should never be able to get a date that's before
    //  the start-date  
    $('#end-date').datetimepicker('setStartDate', startingDate);

    // end-date ending date will always be any date up to
    //  the max date returned by ClimateLogger
    $('#end-date').datetimepicker('setEndDate', endingDate);
  } // if it's not valid don't do anything, should show up
    //  in the text warning
}

/*
 * Update the interval summary text boxes
 * @returns {undefined}
 */
function updateIntervalSplit() {
  var intervalDays = $('#interval-days');
  var intervalHours = $('#interval-hours');
  var intervalMinutes = $('#interval-minutes');
  var intervalSeconds = $('#interval-seconds');
  // get the number of days, hours, minutes and seconds
  //  in the selected interval
  var interval = getIntervalInSeconds();
  // formatIntervalSeconds returns an object with 
  //  the interval broken down into days,hours, minutes and seconds
  var intervalSummaryObj = formatIntervalSeconds(interval);
  intervalDays.val(intervalSummaryObj.intervalDays);
  intervalHours.val(intervalSummaryObj.intervalHours);
  intervalMinutes.val(intervalSummaryObj.intervalMinutes);
  intervalSeconds.val(intervalSummaryObj.intervalSeconds);
}
;

/* Gets the capture rate in yrs, hrs, mins, secs
 * and updates the summary
 * 
 * @returns {undefined}
 */
/***********************************************
 * No longer used per 1/8/17 review
 *********************************************
function updateCaptureRate() {

  var intervalStr = "";
  // get the interval in seconds
  var intervalInSeconds = getIntervalInSeconds();
  // display it in days, hrs, min, secs
  intervalObj = formatIntervalSeconds(intervalInSeconds);
  if (intervalObj.intervalYears > 0) {
    intervalStr = intervalObj.intervalYears + " y";
  }
  if (intervalObj.intervalDays > 0) {
    if (intervalStr) {
      intervalStr += ", ";
    }
    intervalStr += intervalObj.intervalDays + " d";
  }
  if (intervalObj.intervalHours > 0) {
    if (intervalStr) {
      intervalStr += ", ";
    }
    intervalStr += intervalObj.intervalHours + " h";
  }
  if (intervalObj.intervalMinutes > 0) {
    if (intervalStr) {
      intervalStr += ", ";
    }
    intervalStr += intervalObj.intervalMinutes + " m";
  }
  if (intervalObj.intervalSeconds > 0) {
    if (intervalStr) {
      intervalStr += ", ";
    }
    intervalStr += intervalObj.intervalSeconds + " s";
  }
  $('#capture-summary-interval').text(intervalStr);
}
*/

/*
 * Gets the maximum capacity of the climate logger unit
 *  and breaks that time down into yrs, days, hrs, min, secs
 *  and places that value into the div in the summary
 * WhichSummary needs to be 'max' or 'actual'
 *  max - gets the maximum amount
 *  actual - gets amount from start to finish
 * @returns {undefined}
 * @params  intervalSecs is the interval in seconds
 *          whichSummary is 'max' or 'actual'
 */
/******************************************************
 * max summary was removed per review meeting 1/8/17
 * so technically this could get hard-coded to 'actual'
 * now, as that's the only spot it is used
 ******************************************************/
function updateCapacity(intervalSecs, whichSummary) {

  // set up string variable
  var maxCapStr = "";
  var maxCap;
  // if it's for max, need to multiply by the climate logger maxlogs
  // get the max capacity of the unit in seconds
  if (whichSummary === "max") {
    maxCap = intervalSecs * ClimateLogger.getMaxLogs();
  } else {
    maxCap = intervalSecs;
  }
  // split those seconds into formatted days,hrs,min,secs
  var maxCapObj = formatIntervalSeconds(maxCap);
  if (maxCapObj.intervalYears > 0) {
    maxCapStr = maxCapObj.intervalYears + " y";
  }
  if (maxCapObj.intervalDays > 0) {
    if (maxCapStr) {
      maxCapStr += ", ";
    }
    maxCapStr += maxCapObj.intervalDays + " d";
  }
  if (maxCapObj.intervalHours > 0) {
    if (maxCapStr) {
      maxCapStr += ", ";
    }
    maxCapStr += maxCapObj.intervalHours + " h";
  }
  if (maxCapObj.intervalMinutes > 0) {
    if (maxCapStr) {
      maxCapStr += ", ";
    }
    maxCapStr += maxCapObj.intervalMinutes + " m";
  }
  if (maxCapObj.intervalSeconds > 0) {
    if (maxCapStr) {
      maxCapStr += ", ";
    }
    maxCapStr += maxCapObj.intervalSeconds + " s";
  }
  if (maxCapStr === "") {
    // this will happen if we get a negative number
    $('#capture-summary-' + whichSummary).html("&nbsp;");
  }
  $('#capture-summary-' + whichSummary).text(maxCapStr);
}

/*
 * defaultStartDate and defaultEndDate called by
 *  periodicTask,so updated every second ONLY IF the 
 *  associated check boxes are selected
 *  if statement in index page
 *  @returns {undefined}
 **************************************************
 */
function defaultStartDate() {
  // enters current date into start-date-text field
  //  called by periodicTask on main page
  var curDate = new Date();
  var curDateFormatted = dateFormat(curDate, "yyyy-mm-dd HH:MM:ss");
  $('#start-date-text').val(curDateFormatted);
  // also enter it into the summary start div, only if not currently logging
  if(loggingStatus ===0){
    $('#capture-summary-start').text(curDateFormatted);
  }
}
;

function defaultEndDate() {
  // enters max end date into end-date-text field
  //  called by periodicTask on main page
  loadEndDate();

}
;

/*
 * Updates the actual run time text
 * in the capture summary
 * Called by:
 *          periodicTask on main page, 
 *          configLog
 *          endDateUpdate
 *          startDateOrIntervalUpdate
 * @returns 0 if fails, actual run time in seconds if succeeds
 */
function updateActualRunTime() {  
  var result;
  // create variable for the DOM div
  var captureSumDiv = $("#capture-summary-actual");
  // reset any old messages
  captureSumDiv.html("&nbsp;");
  // reset css to black
  captureSumDiv.css("color", "black");
  // make sure it's a valid end date
  if (isValidDate("end")) {
    var endDateText = $('#end-date-text').val();
  } else {
    // give feedback to user
    captureSumDiv.css("color", "#d84a38");
    captureSumDiv.text("Bad end date");
    return 0;
  }
  if (isValidDate("start")) {
    var startDateText = $('#start-date-text').val();
  } else {
    // give feedback to user
    captureSumDiv.css("color", "#d84a38");
    captureSumDiv.text("Bad start date");
    return 0;
  }
  // set start and end in moment format
  var startDate = moment(startDateText);
  var endDate = moment(endDateText);
  // get the difference in seconds
  var intervalInSeconds = endDate.diff(startDate, 'seconds');

  if (intervalInSeconds > 0) {
    // hand off to updateCapacity, will take care of UI update
    updateCapacity(intervalInSeconds, "actual");
    
    // return used in configLog to send to C++ function
    result =  intervalInSeconds;
  } else {
    // start date after end date
    // give feedback to user
    captureSumDiv.css("color", "#d84a38");
    captureSumDiv.text("Start date after end date");
    result =  0;
  }
  
  // This is good place to update the battery life,
  //  because whenever we're updating the running time
  //  we need to look at the battery life
  updateBatteryLife(result);  
    
  // update the slider, but only allow periodicTask
  //  to do it if both start now and get max are checked
  //  Otherwise this needs be done manually.  If this is
  //  allowed to run  while periodicTask is running
  //  the slider is jumpy and does not react like expected
  //if($('#start-now').prop('checked') && $('#end-max').prop('checked')){
  //  updateDateSlider();
  //}
  
  return result;
}

/*
 * Validate the date field
 *  whichDate must be 'start' or 'end' exactly
 *  Also catches errors and makes some corrections
 *  @returns{bool} true if date is ok, false otherwise
 */
function isValidDate(whichDate) {
  var whichDateText = "";
  var whichDateDate = new Date();
  var msgText = "";
  var msgDetail = "";
  var validDate = false;

  
  // assign the variable
  whichDateText = $("#" + whichDate + "-date-text").val();
  // empty the validate div of any existing warning message
  $("#" + whichDate + "-date-validate").html('&nbsp;');
  // should default to red text
  $("#" + whichDate + "-date-validate").css("color", "#d84a38");

  // startDateText now holds string representation of start or end date
  if (whichDateText !== "") {
    // something in there, make sure it's valid
    if (!moment(whichDateText).isValid()) {
      // give user feedback
      msgText = "Not a valid date format";
    } else if (whichDateText === "1899-12-31 00:00") {
      // catch the weird bug if user enters text that gets
      //  somehow converted to this default date
      // reset the value, as this 1899 date is confusing
      //  ** NOTE
      //    This creates some confusion on the front end, as
      //    can over-write the date while the user is trying to
      //    manually type something in.  Don't see a better
      //    solution right now, as the alternative is to show
      //    the 1899 date, which is even more confusing.
      //  **

      // if it's the start date just reset to right now
      if (whichDate === "start") {
        // empty it first, set it to a handy empty variable
        $("#start-date-text").val(msgText);
        loadStartDate();
      } else {
        // if its the end date set it to the max date
        loadEndDate();
      }

    } else {
      // Is not empty, is valid date and not weird date, so convert text to date
      // set the dates to be the same format for comparison
      whichDateDate = moment(whichDateText).format();
      var maxDateDate = moment(MAX_CALENDAR_DATE).format();
      // If the start date is prior to now() just fix it, as
      //  periodicTasks probably just ran over the selected date,
      //  and even if not the user can't pick a date in the past
      // No need to do this if start now box is checked, as is
      //  being updated by periodic tasks
      if (whichDate === "start" && !($('#start-now').is(':checked'))) {
        var curDate = moment().format();

        if (whichDateDate <= curDate) {
          // set start-date-text to current time
          // empty it first, set it to a handy empty variable
          $("#start-date-text").val(msgText);
          // need to set it to the current minute, not second,
          //  so can't call loadStartDate
          var curDateFormatted = dateFormat(curDate, "yyyy-mm-dd HH:MM");
          $("#start-date-text").val(curDateFormatted);
          // also update the summary text, if not currently logging
          if(loggingStatus === 0){
            $('#capture-summary-start').text(curDateFormatted);
          }
          // we're done here, life is good
          validDate = true;
          return validDate;
        }
      } else {
        // valid date, and not start date with start now unchecked
        // give curDate some slack, as clicking and activating can cause
        // a 1-5 second delay when page is trying to use current date
        curDate = moment().subtract(10, 'seconds').format();
        // check date limits
        
        if (whichDateDate < curDate) {
          //alert("startDateDate: " + startDateDate + "\ncurDate: " + curDate);
          if (whichDate === "start") {
            msgDetail = "current date";
          } else {
            msgDetail = "start date";
          }
          msgText = "Can't be prior to " + msgDetail;
        } else if (whichDateDate == maxDateDate) { 
          //alert("1: whichDateDate: " + whichDateDate + "\nmaxDateDate: " + maxDateDate);
          msgText = "Maximum date reached, ";
          // but mark it as valid so doesn't throw error in summary
          validDate = true;
          // and make the text orange for warning, instead of red
          $("#" + whichDate + "-date-validate").css("color", "darkorange");
        }else if (whichDateDate > maxDateDate){
          // date beyond Unix epoch
          msgText = "Date too far in the future";
        } else {
          //alert("2: whichDateDate: " + whichDateDate + "\nmaxDateDate: " + maxDateDate);
          // should be ok if it gets to here
          validDate = true;
          msgText = "";
        }
      }
    }
  } else {
    // field is empty
    msgText = "Date is required";
  }

  if (msgText === "") {
    // no errors
    validDate = true;
    return validDate;
  } else {
    // something bad happened, put message in 
    // appropriate spot
    $("#" + whichDate + "-date-validate").html(msgText);
    // and return flag
    return validDate;
  }
}
;

/****************************************************/
/*  Capture code
 * 
 * Sets up the log in the ClimateLogger
 * @returns {undefined}
 */
function configLog() {
  
  // reset log error message if there
  $("#log-button-validate").html("&nbsp;");

  // Get duration seconds determined from difference between
  // start date and end date.  updateActualRunTime will update
  // the dates and check for errors.  If there are any errors
  // will return 0, otherwise will return total duration in seconds 
  var duration = updateActualRunTime();  
  // get interval seconds determined by interval selection
  var interval = getIntervalInSeconds();

  // If either returned 0 there is a problem and we can't
  //  start the log
  if (interval === 0 || duration === 0) {
    // place error message in div
    var msg = "Problem with dates or interval, log not started";
    $("#log-button-validate").text(msg);
  } else {
    // should be good to go
    if ($('#start-now').is(':checked')) {
      // log will start now
      ClimateLogger.setUpLog(duration, interval);
    } else {
      // Log will start on start date
      //  add a minute to start date,
      //  just in case user managed to get the start
      //  date within a couple of seconds of Now(),
      //  we don't want to send a past date to function
      // We know start date is valid, as just checked it when
      //  getting duration
      var startDateText = $("#start-date-text").val();
      
      // add a minute to be safe
      // removed per meeting 3/28/17, will let back end handle
      // any issues with time not being correct
      //var startDateDate = moment(startDateText).add(1, 'm');
      var startDateDate = moment(startDateText);
      // This can be in the past now, so trap, give warning
      var curDate = moment();
      if (startDateDate < curDate){
        var strMsg = "Can not schedule log with a start date in the past";
        $('#log-button-validate').show();
        $('#log-button-validate').html(strMsg);        
      }else{
        // now convert it to unix timestamp
        var unixDate = moment(startDateDate).unix();
      
        // send it to climatelogger
        ClimateLogger.setUpLog(unixDate, duration, interval);
      }
    }
  }
  checkForNewLogsFlag = 0;
};

function setSetupPage(status){

  // status gives us the logging status:
  //  0 - not logging, ready for new log setup
  //  1 - currently logging
  //  2 - log scheduled future date / time
  if(status === 0){
    // Set page ready to get new logs,
    //  so first enable everything that should be enabled
    $('#start-now').prop('disabled',false);
    if(!$('#start-now').prop('checked')){
      $('#start-date-text').prop('disabled',false);
      $('#start-date-glyph').show();
    }
    $('#end-max').prop('disabled',false);
    if(!$('#end-max').prop('checked')){      
      $('#end-date-text').prop('disabled',false);
      $('#end-date-glyph').show();
    }
    // enable the radio buttons
    $('#capture-form input:radio').prop('disabled',false);
    // enable the interval text box
    $('#interval').prop('disabled',false);
    
    // reset the legend text
    $('#summary-legend').html("Setup Summary:");
    $('#summary-legend').css('color','black');
    
    // reset the colors to black
    $('#capture-summary-actual').css('color','black');
    $('#capture-summary-start').css('color','black');
    $('#capture-summary-end').css('color','black');
    
    // reset the battery summary text
    $('#capture-battery').html('Expected battery life on full charge');
    $('#capture-summary-battery').css('color', 'black');
    
  }else{
    // Currently logging, or scheduled to get logs,
    //  so disable everything
    $('#start-now').prop('disabled',true);
    $('#start-date-text').prop('disabled',true);
    $('#end-max').prop('disabled',true);
    $('#end-date-text').prop('disabled',true);
    // hide the calendar icons no matter what, as
    //  can't disable them
    $('#start-date-glyph').hide();
    $('#end-date-glyph').hide();
    // disable the radio buttons
    $('#capture-form input:radio').prop('disabled',true);
    // disable the interval text box
    $('#interval').prop('disabled',true);
    // Now read the ClimateLogger configuration andd
    //  show it in the summary
    // First call readConfig, as this updates
    //  the private variables
    ClimateLogger.readConfig();
    // now set up the local variables
    var unixTimestampValue = ClimateLogger.getUnixTimestampValue();
    //var offsetValue = ClimateLogger.getOffsetValue();
    var totalLogValue = ClimateLogger.getTotalLogValue();
    var intervalValue = ClimateLogger.getIntervalValue();    
    
    // start time is the unix time stamp + offset
    var startTime = unixTimestampValue;
    var startDate = moment.unix(startTime).format("YYYY-MM-DD HH:mm:ss");
    
    // get the total running time in seconds
    var runningTime = totalLogValue * intervalValue;
    // end time is start time + (interval * total logs)
    var endTime = unixTimestampValue + runningTime;
    var endDate = moment.unix(endTime).format("YYYY-MM-DD HH:mm:ss");

    // update the capacity, this will put the actual
    // log run time into the div capture-summary-actual    
    updateCapacity(runningTime, "actual"); 
    
    // place the dates in the right slots
    $('#capture-summary-start').html(startDate);
    $('#capture-summary-end').html(endDate);
    
    // set text in the battery slot
    $('#capture-battery').html('Interval');
    // update the interval value, this will put the
    //  formatted interval value into the
    //  div capture_summary_battery
    updateCapacity(intervalValue, "battery");

    // set colors according to status
    if (status === 1){
      // currently logging
      // set color to red
      $('#capture-summary-actual').css('color','#d84a38');
      $('#capture-summary-start').css('color','#d84a38');
      $('#capture-summary-end').css('color','#d84a38');
      $('#capture-summary-battery').css('color', '#d84a38');
      // set the legend text
      $('#summary-legend').html("Current Log Summary:");
      $('#summary-legend').css('color','#d84a38');
    }else{
      // log scheduled
      $('#capture-summary-actual').css('color','darkorange');
      $('#capture-summary-start').css('color','darkorange');
      $('#capture-summary-end').css('color','darkorange');
      $('#capture-summary-battery').css('color', 'darkorange');
      // set the legend text
      $('#summary-legend').html("Scheduled Log Summary:");
      $('#summary-legend').css('color','darkorange');
    }
    
    // debugging
    /*
    var strReturnValues = "startDate: " + startDate + "<br />";
    strReturnValues += "offsetValue: " + offsetValue + "<br />";
    strReturnValues += "totalLogValue: " + totalLogValue + "<br />";
    strReturnValues += "intervalValue: " + intervalValue + "<br />";
    strReturnValues += "runningTime: " + runningTime + "<br />";
    strReturnValues += "endDate: " + endDate;
    $('#log-button-validate').show();
    $('#log-button-validate').html(strReturnValues);
    */
  }
}




/**
 * Resets a unit that is currently capturing a log
 * @returns {undefined}
 */
function resetLog() {  
  $('#log-button-validate').html("&nbsp;");  
  ClimateLogger.resetLog();
  // this will force a new cycle for the check for new logs
  minuteCheck = moment().subtract(11,'s');
  };

/* SLIDER FUNCTIONS DISABLED PER REVIEW MEETING 2/14/17
/**
 * @brief Creates the data slider on the capture page
 *  Called from capture.html
 * @returns {undefined}]
 */
/*
function loadDateSlider(){
  // load the date slider with default values
  //  from start and end date
  var startDateText = $("#start-date-text").val();
  var endDateText = $("#end-date-text").val();
  var startDate = moment(startDateText);
  var endDate = moment(endDateText);
  // We're setting the default data values in hours
  var sliderStart = startDate.diff(startDate,"hours");
  var sliderEnd = endDate.diff(startDate,"hours");
  // We're also setting up the grid and disabling by defualt,
  //  because the start now and end max checkboxes are
  //  selected by default
 $("#date-slider").ionRangeSlider({
    type: "double",
    min: sliderStart,
    max: sliderEnd,
    from: sliderStart,
    to: sliderEnd,
    disable: true,
    onChange: function(data){
      sliderUpdated(data);
      //alert("onChange.from: " + data.from);
    },
    onFinish: function(data){
      sliderStartGlobal = data.from;
      //alert("onFinish.from: " + data.from);
    }
  });    
};

function updateDateSlider() {
  // Figure out what the time function will be:
  //  If the selected interval is 'seconds' 
  //    the time function will be 'hours'
  //  If the selected interval is 'minutes'
  //    the time function will be 'days'
  //  If the selected invterval is 'hours'
  //    the time function will be 'weeks'
  var timeFunction;
  // get the time factor first
  var timeFactor = $("input[type='radio'][name='interval-radios']:checked").val();
  if(timeFactor === "seconds"){
    timeFunction = "hours";
  }else if (timeFactor === "minutes"){
    timeFunction = "days";
  }else {
    timeFunction = "weeks";
  }
  var legendText = "Or select total log length (" + timeFunction + ")";
  // update the legend
  $("#slider-legend").html(legendText);

  // set up the time functions
  var startDateText = $("#start-date-text").val();
  var endDateText = $("#end-date-text").val();
  var startDate = moment(startDateText);
  var endDate = moment(endDateText);
  var sliderStart = startDate.diff(startDate, timeFunction);
  var sliderEnd = endDate.diff(startDate, timeFunction);
  
  // set up the slider variable
  var slider = $("#date-slider").data("ionRangeSlider");

  // no matter what we want to update the min and max
  slider.update({
    min: sliderStart,
    max: sliderEnd
  });

  // If the start now  and end max checkboxes are 
  // both checked, the from and end are the same 
  // as the min and max, and the slider is locked
  if($('#start-now').is(':checked') && $('#end-max').is(':checked')){
    slider.update({
      from: sliderStart,
      to: sliderEnd,
      disable: true
    });
    // nothing more to do
    return;
  }
  
  // If just the start now checkbox is checked,
  //  we need to lock in the end, but allow
  //  the start to move.
  if(!$('#start-now').is(':checked') && $('#end-max').is(':checked')){
    slider.update({
      from: sliderStart,
      to: sliderEnd,
      from_fixed: false,
      to_fixed: true,
      disable: false
    });
    return;
  }
  
  // If just the end max checkbox is checked,
  //  we need to lock in the start, but allow
  //  the end to move.
  
  if($('#start-now').is(':checked') && !$('#end-max').is(':checked')){
    slider.update({
      from: sliderStart,
      to: sliderEnd,
      from_fixed: true,
      to_fixed: false,
      disable: false
    });
    return;
  }

  // If neither checkbox is checked,
  //  we need to allow both start and end to move.  
  if(!$('#start-now').is(':checked') && !$('#end-max').is(':checked')){
    slider.update({
      from: sliderStart,
      to: sliderEnd,
      from_fixed: false,
      to_fixed: false,
      disable: false
    });
  }
  
}


function sliderUpdated(data){
  // if there are global values get them first
  var sliderStartLocal = data.from;
  if(sliderStartLocal < sliderStartGlobal){
    //alert("subtract\nglobal: " + sliderStartGlobal + "\nlocal: " + sliderStartLocal);
  }else{
    //alert("add\nglobal: " + sliderStartGlobal + "\nlocal: " + sliderStartLocal);
  }

  
  //alert("local: " + sliderStartLocal + "\nglobal: " + sliderStartGlobal);
  return;
  // get the time factor first
  var timeFactor = $("input[type='radio'][name='interval-radios']:checked").val();
  var timeFunction;
  if(timeFactor === "seconds"){
    timeFunction = "hours";
  }else if (timeFactor === "minutes"){
    timeFunction = "days";
  }else {
    timeFunction = "weeks";
  }

  if(data.from){
    var startDateText = $("#start-date-text");
    var startDateDate = moment(startDateText.val());
    var addTime = data.from;
    //alert(addTime + ", " + timeFunction);
    var newDate = startDateDate.add(addTime,timeFunction);
    //alert(startDateDate + ", " + newDate);
    startDateText.val(dateFormat(newDate, "yyyy-mm-dd HH:MM:ss"));
    //alert("slider from: " + data.from);
  }
}

//      END SLIDER FUNCTIONS
*****************************************************/


