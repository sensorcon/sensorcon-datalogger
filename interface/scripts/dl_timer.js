/* 
 * File:    dl_timer.js
 *
 * Created: January, 2017
 * Purpose: Supports logs.html
 * 
 */

/*******************************************************************************
 * Globals
 */
var temperatureSensor = false;
var humiditySensor = false;
var pressureSensor = false;
var lightSensor = false;
var coSensor = false;
var adc4 = false;

// We need to know if is production version, even if not connected,
//  so this is moved here instead of being in the onConnect function
var developmentVersion = ClimateLogger.getDevelopmentVersion();
var coVersion = ClimateLogger.getCOVersion();
var cloudVersion = ClimateLogger.getCloudVersion();

/**
 * Updates the connected html pages
 * @returns {undefined}
 */
function updateConnectedScreens() {
  // these are loaded no matter what
  connectDashboardScreen();
  connectCaptureScreen();
  connectLogsScreen();
  // these only run if not production version
  if(coVersion){
    connectCalibrateScreen();
  }
  
  if(cloudVersion){
    connectCloudScreen();
  }
}

/**
 * Everything that needs to be done when the
 *  climate logger is unplugged
 * @returns {undefined}
 */
function onDisconnect() {
  connectionState = 0;
  document.getElementById("status").innerHTML =
          '<button id="connect-button" type="button" \n\
            class="btn connect-button-class">Click To Connect</button>';
  document.getElementById("connect-button").onclick = connect;
  document.getElementById("connect-button").disabled = false;
  document.getElementById("battery").style.visibility = "hidden";
  $("#logger-status-header").hide();
  document.getElementById("sidebar-calibrate").style.visibility = "hidden";
  document.getElementById("temperature-unit-div").style.visibility = "hidden";
  document.getElementById("pressure-unit-div").style.visibility = "hidden";
  document.getElementById("version-div").style.display = "none";
  updateConnectedScreens();
  clearInterval(timer);
  hideDeviceName();
  checkForNewLogsFlag = 0;
  downloadingFromDevice = false;
  // reset the chart so that new logger can have
  //  chance to stabilize before charting
  okToChart = 0; 
  // set focus on the dashboard screen, just to clear
  //  any old screens that are hidden now
  selectContent("dashboard");
}

/**
 * Everything that needs to be done when the
 *  climate logger is connected
 * @returns {undefined}
 */
function onConnect() {
  connectionState = 1;
  document.getElementById("status").innerHTML =
          '<button id="connected-button" type="button" \n\
            class="btn green connect-button-class">Connected</button>';
  document.getElementById("connected-button").disabled = true;
  document.getElementById("battery").style.visibility = "visible";
  temperatureSensor = ClimateLogger.temperatureSensorAvailable();
  humiditySensor = ClimateLogger.humiditySensorAvailable();
  pressureSensor = ClimateLogger.pressureSensorAvailable();
  lightSensor = ClimateLogger.lightSensorAvailable();
  coSensor = ClimateLogger.coSensorAvailable();
  adc4 = ClimateLogger.adc4Available();
  // debugging only
  //adc4 = true;
  /**********************
   * located in dl_header.js
   */
  showDeviceName();        
  showFirmwareVersion();
  /**********************/
  unitOnConnect();
  var isLogging = ClimateLogger.checkLoggingStatus();
  if (coSensor && coVersion && isLogging < 1 ) {
    // use not visible instead of not displayed, keeps spacing correct
    document.getElementById("sidebar-calibrate").style.visibility = "visible";
  }else{
    document.getElementById("sidebar-calibrate").style.visibility = "hidden";
  }
  if(cloudVersion){
    // show it in the development mode
    document.getElementById("sidebar-cloud").style.visibility = "visible";    
   
  }else{
    // hide the cloud tab
    document.getElementById("sidebar-cloud").style.visibility = "hidden"; 
  }
  
  updateConnectedScreens();  
  timer = setInterval(refresh, 1000);
}

/**
 * Attemps to connect via back end,
 *  updates front end based on results
 * @returns {undefined}
 */
function connect() {
  var connected = ClimateLogger.connect();

  if (connected == true) {
    if (connectionState != 1) {
      onConnect();
    }
  }
}

/**
 * Controls connect / disconnect / periodicTask
 * @returns {undefined}
 */
function refresh() {
  if (connectionState == -1) {
    var isConnected = ClimateLogger.isConnected();
    if (isConnected == true) {
      onConnect();
    } else {
      onDisconnect();
    }
  } else if (connectionState == 1) {
    var isConnected = ClimateLogger.isConnected();
    if (isConnected == false) {
      onDisconnect();
    } else {
      periodicTask();
    }
  }
}


/**
 * Shows advanced tab if appropriate
 * @returns {Boolean}
 */
function advanced() {
  var result = false;
  // this should be redundant, as the back end
  //  should catch this, but just in case
  if (coVersion) {
    var response = ClimateLogger.advancedDialog();
    if (response === true) {
      result = true;
    }
  } else {
    result = true;
  }
  return result;
}
