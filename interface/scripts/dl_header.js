/* 
 * File:    dl_header.js
 *
 * Created: January, 2017
 * Purpose: Supports header and sidebar
 * 
 */

var minuteCheck = moment();

function changeName() {
  ClimateLogger.changeName();
  showDeviceName();
}

function showFirmwareVersion() {
// Show firmware version
  document.getElementById("pver").innerHTML = ClimateLogger.getProductVersion();
  document.getElementById("prev").innerHTML = ClimateLogger.getProductRevision();
  document.getElementById("pbul").innerHTML = ClimateLogger.getProductBuild();
  document.getElementById("version-div").style.display = "inline";
}

function showDeviceName() {
  document.getElementById("device-name-link").innerHTML = ClimateLogger.readName();
  document.getElementById("device-name-link").style.visibility = "visible";
}

function hideDeviceName() {
  document.getElementById("device-name-link").innerHTML = "&nbsp;"
  document.getElementById("device-name-link").style.visibility = "hidden";
}

function checkLoggingStatus() {
  
  // Don't want to do any of this if we're
  //  currently downloading from the device
  if(downloadingFromDevice){
    return;
  }
  
  var htmlText = "";
  // reset the status text
  var divHTML = "";
  var loggerStatusHeader = $("#logger-status-header");
  var loggerStatusBody = $("#logger-status-body");

  // no matter what we end up with here we want to show the header button
  loggerStatusHeader.show();
  

  if (checkForNewLogsFlag === 0) {
    //alert("check here");
    // reset the flag
    checkForNewLogsFlag = 1;
    //alert("check for new logs");
    var stat = ClimateLogger.checkLoggingStatus();
    // update UI on setup
    setSetupPage(stat);

    if (stat === 1) {
      //currently logging
      divHTML = "<button id='logger-status-button-logging' type='button'";
      divHTML += "class='logger-status-button btn' onclick = 'selectContent(\"capture\")'>";
      divHTML += "Currently logging</button>";
      loggerStatusHeader.html(divHTML);
      loggerStatusBody.html("The climate logger is currently logging data");
      loggerStatusBody.show();
      document.getElementById("new-logs-button").style.display = "none";
      divHTML = "";
      loggingStatus = 1;      

      $("#log-status-button").show();
      $("#log-queued-button").hide();
      $("#log-start-button").hide();
      // Don't show the cailibrate page even if it's enabled
      document.getElementById("sidebar-calibrate").style.visibility = "hidden";
    } else if (stat === 0) {
      // not logging, ok to start new capture, but check for new logs first    
      var newLogs = ClimateLogger.checkForNewLogs();

      if (newLogs == true) {
        // Logger has new logs available to download
        divHTML = "<button id='logger-status-button-newlogs' type='button'";
        divHTML += "class='logger-status-button btn' onclick = 'downloadLogs()'>";
        divHTML += "Download Logs</button>";
        loggerStatusHeader.html(divHTML);
        document.getElementById("new-logs-button").style.display = "block";
        // we're showing the download button in the body, so hide the text
        loggerStatusBody.hide();
        divHTML = "";
      } else {
        // update UI on setup
        setSetupPage(stat);
        // No new logs, unit is ready to set up for new log
        divHTML = "<button id='logger-status-button-ready' type='button'";
        divHTML += "class='logger-status-button btn' onclick = 'selectContent(\"capture\")'>";
        divHTML += "Ready for new log</button>";
        loggerStatusHeader.html(divHTML);
        var htmlText = "The climate logger is <a href='javascript:selectContent(\"capture\")'>";
        htmlText += "ready to start a new log</>";
        loggerStatusBody.html(htmlText);
        loggerStatusBody.show();
        document.getElementById("new-logs-button").style.display = "none";
        htmlText = "";
        divHTML = "";
      }
      loggingStatus = 0;
      $("#log-status-button").hide();
      $("#log-queued-button").hide();
      $("#log-start-button").show();

      if (coSensor && coVersion) {
        // only show the calibrate page if has CO sensor and is not production
        document.getElementById("sidebar-calibrate").style.visibility = "visible";
      }
    } else {
      // stat == 2, 
      // Log set up to start in future
      divHTML = "<button id='logger-status-button-scheduled' type='button'";
      divHTML += "class='logger-status-button btn' onclick = 'selectContent(\"capture\")'>";
      divHTML += "Log scheduled</button>";
      loggerStatusHeader.html(divHTML);
      document.getElementById("new-logs-button").style.display = "none";
      var stamp = ClimateLogger.getLogTimestamp();
      loggerStatusBody.html("The climate logger is scheduled to start a new log on " + stamp);
      loggerStatusBody.show();
      loggingStatus = 2;
      document.getElementById("log-queued-button-text").innerHTML = "Log set to start on " + stamp + " - click to reset";
      $("#log-status-button").hide();
      $("#log-queued-button").show();
      $("#log-start-button").hide();
      if (coSensor && coVersion) {
        // only show the calibrate page if has CO sensor and is not production
        document.getElementById("sidebar-calibrate").style.visibility = "visible";
      }else{
        document.getElementById("sidebar-calibrate").style.visibility = "hidden";
      }
    }
  }
  
 
  // Need to check status every so often
  // Start by getting the current time
  var curLogTime = moment();
  // Now subtract a some time from it
  curLogTime = moment().subtract(10, 's');
  // Now see if it's greater than the global
  if (curLogTime > minuteCheck) {
    // reset the flag
    checkForNewLogsFlag = 0;
    // reset the global
    minuteCheck = moment();
    //alert("flag reset");
  }

}

/*
 * Checks battery voltage and sets battery level image
 */
function checkBattery() {
  var battVoltage = ClimateLogger.readBatteryVoltage();

  if (battVoltage === -1) {
    document.getElementById("battery0").style.display = "none";
    document.getElementById("battery1").style.display = "none";
    document.getElementById("battery2").style.display = "none";
    document.getElementById("battery3").style.display = "none";
    document.getElementById("battery4").style.display = "inline";
  } else if (battVoltage >= 3.8) {
    document.getElementById("battery0").style.display = "none";
    document.getElementById("battery1").style.display = "none";
    document.getElementById("battery2").style.display = "none";
    document.getElementById("battery3").style.display = "inline";
    document.getElementById("battery4").style.display = "none";
  } else if ((battVoltage < 3.8) && (battVoltage >= 3.5)) {
    document.getElementById("battery0").style.display = "none";
    document.getElementById("battery1").style.display = "none";
    document.getElementById("battery2").style.display = "inline";
    document.getElementById("battery3").style.display = "none";
    document.getElementById("battery4").style.display = "none";
  } else if ((battVoltage < 3.5) && (battVoltage >= 3.1)) {
    document.getElementById("battery0").style.display = "none";
    document.getElementById("battery1").style.display = "inline";
    document.getElementById("battery2").style.display = "none";
    document.getElementById("battery3").style.display = "none";
    document.getElementById("battery4").style.display = "none";
  } else {
    document.getElementById("battery0").style.display = "inline";
    document.getElementById("battery1").style.display = "none";
    document.getElementById("battery2").style.display = "none";
    document.getElementById("battery3").style.display = "none";
    document.getElementById("battery4").style.display = "none";
  }
}


/**
 * Show Owner manual in default web browser
 * @returns {undefined}
 */
function showSite() {
  ClimateLogger.showSensorconSite();
}

/**
 * Disable right click
 * @returns {undefined}
 */
function disableclick() {
  if (event.button === 2) {
    var strMsg = "Right click disabled";
    ClimateLogger.displayMessage(strMsg);
    $(this).on("contextmenu", function () {
      return false;
    });
  }
}