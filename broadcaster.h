/* 
 * File:    broadcaster.h
 * Author:  dcanfield
 *
 * Created: January 3, 2017, 11:16 AM
 */

#ifndef BROADCASTER_H
#define BROADCASTER_H

#include <QtCore/QDebug>
#include <QNetworkAccessManager>
#include <QThread>
#include <QUrl>
//#include <QUrlQuery>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QApplication>
//#include <QtNetwork>
#include "databasehelper.h"

class BroadCaster : public QObject {
  Q_OBJECT

private:
  QNetworkAccessManager *bDeviceManager;
  QNetworkRequest bDeviceRequest;
  QNetworkAccessManager *bLogManager;
  QNetworkRequest bLogRequest;
  DatabaseHelper *dbHelper;
  int logUpdateCount;
  int deviceUpdateCount;

public:
  BroadCaster(DatabaseHelper &helper);
  void postDevice(QWidget *parent);
  void postLog(QWidget *parent);  
  int getLogUpdateCount();

public slots:
  void onDevicePostCompleted(QNetworkReply*);
  void onLogPostCompleted(QNetworkReply*);
};

#endif

