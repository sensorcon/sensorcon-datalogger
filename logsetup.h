#ifndef LOGSETUP_H
#define LOGSETUP_H

#include "usbinterface.h"
#include "usersettings.h"
#include "databasehelper.h"

class LogSetup
{
public:
    LogSetup();
    LogSetup(USBInterface &interface);

    void setMaxLogs(int mLogs);
    void setLogPacketSize(int pSize);
    bool sendConfiguration(int unixStamp, int offset, int interval, int numLogs);

    int getMaxEndTime(int startTimestamp, int interval);
    int getMaxLogs();
    void setUpLog(QWidget *parent, int duration, int interval);
    void setUpLog(QWidget *parent, int startTimestamp, int duration, int interval);

    void setUpLogLater(QWidget *parent, QString startDate, QString startTime, QString endDate, QString endTime, int startAmPm, int endAmPm, QString interval);
    void setUpLogLater(QWidget *parent, QString startDate, QString startTime, int startAmPm, QString days, QString hours, QString minutes, QString seconds, QString interval);
    void setUpLogNow(QWidget *parent, QString endDate, QString endTime, int endAmPm, QString interval);
    void setUpLogNow(QWidget *parent, QString days, QString hours, QString minutes, QString seconds, QString interval);
    void showLogSetup(QDateTime startDateTime, QDateTime endDateTime, QString interval);
private:
    USBInterface *usbInterface;
    UserSettings settings;
    int maxLogs;
    int packetSize;

    bool testFlashErase();
    bool testLogConfig(int unixStamp, int offset, int interval, int numLogs);
    void eraseFlash();
    bool validateConnection();
    bool validateErase(QWidget *parent);
    bool validateTotalLogs(int totalLogs);
    bool validateLogTime(QString days, QString hours, QString minutes, QString seconds);
    bool validateInterval(QString interval, int totalSecs);
    bool validateEndTime( QDateTime startTime, QDateTime endTime);
    bool validateStartTime(QDateTime startTime);
    bool validateDateAndTime(QString date, QString time);
    bool validateLoggingStatus();
};

#endif // LOGSETUP_H
