#include "interfaceupdater.h"
#include "databasehelper.h"
#include "usersettings.h"

#include <QDebug>
#include <QGraphicsWebView>
#include <QWebFrame>
#include <QFile>
#include <QTextStream>
#include <ctime>
#include <QFileDialog>
#include <QtSerialPort/QSerialPortInfo>
#include <QProgressDialog>
#include <QProgressBar>
#include <QMessageBox>
#include <QApplication>
#include <QtWebKitWidgets>

// This is also defined in ClimateLogger class
//  What happens if one is defined as true and
//  the other false?  Not sure this should be here,
//  maybe should be pulling the one from ClimateLogger?
//#define PRODUCTION_VERSION  true


InterfaceUpdater::InterfaceUpdater(QWidget *parent) : Html5ApplicationViewer(parent) {
    QObject::connect(webView()->page()->mainFrame(),
            SIGNAL(javaScriptWindowObjectCleared()), SLOT(addToJavaScript()));

    // Force cursor to be arrow
    QApplication::setOverrideCursor(Qt::ArrowCursor);

    climateLogger = new ClimateLogger(this);
}

void InterfaceUpdater::addToJavaScript() {
    webView()->page()->mainFrame()->addToJavaScriptWindowObject("InterfaceUpdater", this);
}

bool InterfaceUpdater::connectUSB() {
    return climateLogger->connect();
}

bool InterfaceUpdater::isConnected() {
    return climateLogger->isConnected();
}

void InterfaceUpdater::storeTemperatureUnit(int unit) {
    settings.saveTemperatureUnit(unit);
}

void InterfaceUpdater::storePressureUnit(int unit) {
    settings.savePressureUnit(unit);
}

int InterfaceUpdater::getTemperatureUnit() {
    return settings.getTemperatureUnit();
}

int InterfaceUpdater::getPressureUnit() {
    return settings.getPressureUnit();
}

int InterfaceUpdater::getLastLogInterval() {
    return settings.getLogInterval();
}

int InterfaceUpdater::getLastLogDays() {
    return settings.getLogDays();
}

int InterfaceUpdater::getLastLogHours() {
    return settings.getLogHours();
}

int InterfaceUpdater::getLastLogMinutes() {
    return settings.getLogMinutes();
}

int InterfaceUpdater::getLastLogSeconds() {
    return settings.getLogSeconds();
}

//bool InterfaceUpdater::getProductionVersion() {
//    return (bool)PRODUCTION_VERSION;
//}

QString InterfaceUpdater::getProductIdString() {
    return climateLogger->getProductIdString();
}

int InterfaceUpdater::getProductVersion() {
    return climateLogger->getProductVersion();
}

int InterfaceUpdater::getProductRevision() {
    return climateLogger->getProductRevision();
}

int InterfaceUpdater::getProductBuild() {
    return climateLogger->getProductBuild();
}

bool InterfaceUpdater::getTemperatureSensor() {
    return climateLogger->temperatureSensorAvailable();
}

bool InterfaceUpdater::getHumiditySensor() {
    return climateLogger->humiditySensorAvailable();
}

bool InterfaceUpdater::getPressureSensor() {
    return climateLogger->pressureSensorAvailable();
}

bool InterfaceUpdater::getLightSensor() {
    return climateLogger->lightSensorAvailable();
}

bool InterfaceUpdater::getCOSensor() {
    return climateLogger->coSensorAvailable();
}

bool InterfaceUpdater::getADC4() {
    return climateLogger->adc4Available();
}

bool InterfaceUpdater::getADC5() {
    return climateLogger->adc5Available();
}

bool InterfaceUpdater::getADC9() {
    return climateLogger->adc9Available();
}

bool InterfaceUpdater::getUVSensor() {
    return climateLogger->uvDebugAvailable();
}

bool InterfaceUpdater::getIRSensor() {
    return climateLogger->irDebugAvailable();
}

bool InterfaceUpdater::getBatterySensor() {
    return climateLogger->batteryDebugAvailable();
}

//void InterfaceUpdater::config(QString unixStamp, QString offset, QString interval, QString numLogs) {
    //climateLogger->setUpLog(unixStamp, offset, interval, numLogs);
//}

void InterfaceUpdater::measureSensors() {
    //climateLogger->measureSensors();
}

void InterfaceUpdater::coZero() {
    //climateLogger->coZero();
}


void InterfaceUpdater::coCalibrate() {
    climateLogger->coCalibrate();
}

void InterfaceUpdater::updateSensorReadings() {
    //climateLogger->streamSensors();
}

QString InterfaceUpdater::readTemperature() {
    //return QString::number(climateLogger->readTemperature(),'f',1);
    return "";
}

QString InterfaceUpdater::readHumidity() {
    //return QString::number(climateLogger->readHumidity(),'f',1);
    return "";
}

QString InterfaceUpdater::readPressure() {
//    if((settings.getPressureUnit() == 1) || (settings.getPressureUnit() == 2)) {
//        return QString::number(climateLogger->readPressure(),'f',2);
//    } else {
//        return QString::number(climateLogger->readPressure());
//    }
    return "";
}

QString InterfaceUpdater::readLight() {
    //return QString::number(climateLogger->readLight(),'f',1);
    return "";
}

QString InterfaceUpdater::readCO() {
//    return QString::number(climateLogger->readCO());
    return "";
}

QString InterfaceUpdater::readUV() {
    return QString::number(climateLogger->readUV());
}

QString InterfaceUpdater::readIR() {
    return QString::number(climateLogger->readIR());
}

QString InterfaceUpdater::readBattery() {
    return QString::number(climateLogger->readBattery(),'f',2);
}

QString InterfaceUpdater::readADC4() {
//    return QString::number(climateLogger->readADC4());
    return "";
}

QString InterfaceUpdater::readADC5() {
    return QString::number(climateLogger->readADC5());
}

QString InterfaceUpdater::readADC9() {
    return QString::number(climateLogger->readADC9());
}

QString InterfaceUpdater::readDeviceName() {
    return climateLogger->readName();
}

void InterfaceUpdater::openDeviceNameDialog() {
    climateLogger->changeName();
}

//bool InterfaceUpdater::checkForNewLogs(bool askForDownload) {
    //return climateLogger->checkForNewLogs(askForDownload);
//    return true;
//}

//int InterfaceUpdater::downloadFromDevice(void) {
//    return climateLogger->downloadFromDevice();
//}

void InterfaceUpdater::generateCSV(int captureID) {
    climateLogger->generateCSV(captureID);
}

void InterfaceUpdater::eraseFlash() {
    //climateLogger->eraseFlash();
}

void InterfaceUpdater::eraseDB() {
    climateLogger->eraseDB();
}

int InterfaceUpdater::checkLoggingStatus() {
    return climateLogger->checkLoggingStatus();
}

int InterfaceUpdater::checkCoZeroStatus() {
    return climateLogger->checkCoZeroStatus();
}

int InterfaceUpdater::checkCoCalStatus() {
    return climateLogger->checkCoCalStatus();
}

void InterfaceUpdater::clearCoZeroStatus() {
//    climateLogger->clearCoZeroStatus();
}

void InterfaceUpdater::clearCoCalStatus() {
//    climateLogger->clearCoCalStatus();
}

bool InterfaceUpdater::checkLogResetError() {
    //return climateLogger->checkLogResetError();
    return false;
}

QString InterfaceUpdater::getCoBaseline() {
    return QString::number(climateLogger->getCoBaseline());
}

QString InterfaceUpdater::getCoBaselineTemp() {
    return "";
}

QString InterfaceUpdater::getCoSensitivity() {
    return "";
}

QString InterfaceUpdater::getCoSensitivityTemp() {
    return "";
}

QString InterfaceUpdater::getCoBaselineADC() {
    return "";
}

QString InterfaceUpdater::getCoSensitivityADC() {
    return "";
}

int InterfaceUpdater::getDaysUntilCalibration() {
return 0;
}

QString InterfaceUpdater::readADC() {
    return QString::number(climateLogger->readBatteryVoltage(),'f',2);
}

void InterfaceUpdater::resetLog() {
    climateLogger->resetLog();
}

void InterfaceUpdater::setUpLogLater(QString startDate, QString startTime, QString endDate, QString endTime, int startAmPm, int endAmPm, QString interval) {   
    climateLogger->setUpLogLater(startDate, startTime, endDate, endTime, startAmPm, endAmPm, interval);
}

void InterfaceUpdater::setUpLogLater(QString startDate, QString startTime, int startAmPm, QString days, QString hours, QString minutes, QString seconds, QString interval) {
    climateLogger->setUpLogLater(startDate, startTime, startAmPm, days, hours, minutes, seconds, interval);
}

void InterfaceUpdater::setUpLogNow(QString endDate, QString endTime, int endAmPm, QString interval) {
    climateLogger->setUpLogNow(endDate, endTime, endAmPm, interval);
}

void InterfaceUpdater::setUpLogNow(QString days, QString hours, QString minutes, QString seconds, QString interval) {
    climateLogger->setUpLogNow(days, hours, minutes, seconds, interval);
}

void InterfaceUpdater::showSensorconSite() {
     QDesktopServices::openUrl(QUrl("http://sensorcon.com/store/solutions-support/product-support/user-manuals-data-sheets", QUrl::TolerantMode));
}
// duplicate of function in climatelogger.cpp
/*
bool InterfaceUpdater::advancedDialog() {
    bool response = false;

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Advanced Settings", "This option is only for advanced users. Are you sure you want to continue?",
                                   QMessageBox::Yes|QMessageBox::Cancel);
     if (reply == QMessageBox::Yes) {
        response = true;
     }

     return response;
}
*/