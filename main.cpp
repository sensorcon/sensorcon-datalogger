#include <QApplication>
#include "html5applicationviewer.h"
#include "climatelogger.h"
#include "constants.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    ClimateLogger viewer;
    viewer.setOrientation(Html5ApplicationViewer::ScreenOrientationAuto);
    viewer.showExpanded();
    viewer.loadFile(QLatin1String("interface/index.html"));

    viewer.resize(900, 680);
    viewer.setMinimumWidth(900);
    viewer.setMinimumHeight(680);
    viewer.setMaximumWidth(900);
    viewer.setMaximumHeight(680);

    viewer.setWindowTitle(Constants::PROGRAM_NAME);

    return app.exec();
}
