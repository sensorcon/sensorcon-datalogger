#include "constants.h"


const int Constants::SENSOR_STREAM_PACKET_ID = 0;
const int Constants::AVAILABLE_SENSOR_PACKET_ID = 1;
const int Constants::LOG_SETUP_PACKET_ID = 2;
const int Constants::FLASH_ERASE_PACKET_ID = 3;
const int Constants::FLASH_TEST_PACKET_ID = 4;
const int Constants::FLASH_READ_PACKET_ID = 5;
const int Constants::SEND_CONFIG_PACKET_ID = 6;
const int Constants::READ_LOG_PACKET_ID = 7;
const int Constants::FW_VERSION_PACKET_ID = 8;
const int Constants::CO_ZERO_PACKET_ID = 9;
const int Constants::CO_CALIBRATE_PACKET_ID = 10;
const int Constants::READ_CO_CAL_PARAMS_PACKET_ID = 11;
const int Constants::READ_STATUS_PACKET_ID = 12;
const int Constants::WRITE_CAL_DATE_PACKET_ID = 13;
const int Constants::READ_CAL_DATE_PACKET_ID = 14;
const int Constants::WRITE_NAME_PACKET_ID = 15;
const int Constants::READ_NAME_PACKET_ID = 16;
const int Constants::RESET_LOG_PACKET_ID = 17;
const int Constants::CLEAR_ZERO_ERROR_PACKET_ID = 18;
const int Constants::READ_BATTERY_PACKET_ID = 19;
const int Constants::BOOT_PACKET_ID = 20;
const int Constants::GET_INST_BASELINE_ID = 21;
const int Constants::GET_INST_SENSITIVITY_ID = 22;
const int Constants::WRITE_CO_BL_ID = 23;
const int Constants::WRITE_CO_SENS_ID = 24;
const int Constants::WRITE_FLASH_KEY_ID = 25;

const int Constants::TEMPERATURE_INDEX = 0;
const int Constants::HUMIDITY_INDEX = 1;
const int Constants::PRESSURE_INDEX = 2;
const int Constants::LIGHT_INDEX = 3;
const int Constants::CO_INDEX = 4;
const int Constants::ADC4_INDEX = 12;

const int Constants::TOTAL_SENSORS = 8;
const int Constants::TOTAL_ADC = 10;
const int Constants::TOTAL_DEBUG = 8;

const QString Constants::PROGRAM_NAME = QString("Sensorcon Data Logger");

Constants::Constants()
{
}
