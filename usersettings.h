#ifndef USERSETTINGS_H
#define USERSETTINGS_H

#include "QtSql/QtSql"
#include <vector>

class UserSettings
{
public:
    UserSettings();
    void saveTemperatureUnit(int unit);
    void savePressureUnit(int unit);
    void saveFileDirectory(QString directory);
    void saveEnabledSensors(bool *enabledSensors);
    void saveEnabledADCs(bool *enabledADC);
    void saveEnabledDebug(bool *enabledDebug);
    void saveTimeLength(int days, int hours, int minutes, int seconds);
    void saveInterval(int interval);
    QString getFileDirectory();
    int getTemperatureUnit();
    int getPressureUnit();
    void getEnabledSensors(bool *enabledSensors);
    void getEnabledADCs(bool *enabledADC);
    void getEnabledDebug(bool *enabledDebug);
    int getLogDays();
    int getLogHours();
    int getLogMinutes();
    int getLogSeconds();
    int getLogInterval();
};

#endif // USERSETTINGS_H
