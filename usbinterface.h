#ifndef USBINTERFACE_H
#define USBINTERFACE_H

#include <QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

class USBInterface
{
public:
    USBInterface();
    bool isConnected();
    bool connect();
    QList<int> readPacket(QString command, int packetId);
    QByteArray readPacket(int packetId);
    bool writeCommand(QString command);
    QByteArray readLog(int segment, int packetSize, int &stat);
    void writeCommand(int packetId, int size, QByteArray data);

private:
    QSerialPort *serial;
    bool connected;

    // Debug
    int numErrors;
};

#endif // USBINTERFACE_H
