#include "usersettings.h"

UserSettings::UserSettings()
{
}

void UserSettings::saveTemperatureUnit(int unit) {
    QSettings settings("Sensorcon", "ClimateLogger");
    settings.setValue("temperatureUnit", unit);
}
void UserSettings::savePressureUnit(int unit) {
    QSettings settings("Sensorcon", "ClimateLogger");
    settings.setValue("pressureUnit", unit);
}
void UserSettings::saveFileDirectory(QString directory) {
    QSettings settings("Sensorcon", "ClimateLogger");
    settings.setValue("fileDirectory", directory);
}
void UserSettings::saveEnabledSensors(bool *enabledSensors) {
    QSettings settings("Sensorcon", "ClimateLogger");
    settings.setValue("sensorTemperature", (bool)enabledSensors[0]);
    settings.setValue("sensorHumidity", (bool)enabledSensors[1]);
    settings.setValue("sensorPressure", (bool)enabledSensors[2]);
    settings.setValue("sensorLight", (bool)enabledSensors[3]);
    settings.setValue("sensorCO", (bool)enabledSensors[4]);
    settings.setValue("sensorH2S", (bool)enabledSensors[5]);
    settings.setValue("sensorIRT", (bool)enabledSensors[6]);
}
void UserSettings::saveEnabledADCs(bool *enabledADC) {
    QSettings settings("Sensorcon", "ClimateLogger");
    settings.setValue("adc0", (bool)enabledADC[0]);
    settings.setValue("adc1", (bool)enabledADC[1]);
    settings.setValue("adc2", (bool)enabledADC[2]);
    settings.setValue("adc3", (bool)enabledADC[3]);
    settings.setValue("adc4", (bool)enabledADC[4]);
    settings.setValue("adc5", (bool)enabledADC[5]);
    settings.setValue("adc6", (bool)enabledADC[6]);
    settings.setValue("adc7", (bool)enabledADC[7]);
    settings.setValue("adc8", (bool)enabledADC[8]);
    settings.setValue("adc9", (bool)enabledADC[9]);
}
void UserSettings::saveEnabledDebug(bool *enabledDebug) {
    QSettings settings("Sensorcon", "ClimateLogger");
    settings.setValue("debug0", (bool)enabledDebug[0]);
    settings.setValue("debug1", (bool)enabledDebug[1]);
    settings.setValue("debug2", (bool)enabledDebug[2]);
    settings.setValue("debug3", (bool)enabledDebug[3]);
    settings.setValue("debug4", (bool)enabledDebug[4]);
    settings.setValue("debug5", (bool)enabledDebug[5]);
    settings.setValue("debug6", (bool)enabledDebug[6]);
    settings.setValue("debug7", (bool)enabledDebug[7]);;
}
void UserSettings::saveTimeLength(int days, int hours, int minutes, int seconds) {
    QSettings settings("Sensorcon", "ClimateLogger");
    settings.setValue("logDays", days);
    settings.setValue("logHours", hours);
    settings.setValue("logMinutes", minutes);
    settings.setValue("logSeconds", seconds);
}
void UserSettings::saveInterval(int interval) {
    QSettings settings("Sensorcon", "ClimateLogger");
    settings.setValue("logInterval", interval);
}


QString UserSettings::getFileDirectory() {
    QSettings settings("Sensorcon", "ClimateLogger");
    return settings.value("fileDirectory").toString();
}
int UserSettings::getTemperatureUnit() {
    QSettings settings("Sensorcon", "ClimateLogger");
    return settings.value("temperatureUnit").toInt();
}
int UserSettings::getPressureUnit() {
    QSettings settings("Sensorcon", "ClimateLogger");
    return settings.value("pressureUnit").toInt();
}
void UserSettings::getEnabledSensors(bool *enabledSensors) {
    QSettings settings("Sensorcon", "ClimateLogger");

    enabledSensors[0] = settings.value("sensorTemperature").toBool();
    enabledSensors[1] = (bool)settings.value("sensorHumidity").toBool();
    enabledSensors[2] = (bool)settings.value("sensorPressure").toBool();
    enabledSensors[3] = (bool)settings.value("sensorLight").toBool();
    enabledSensors[4] = (bool)settings.value("sensorCO").toBool();
    enabledSensors[5] = (bool)settings.value("sensorH2S").toBool();
    enabledSensors[6] = (bool)settings.value("sensorIRT").toBool();
}
void UserSettings::getEnabledADCs(bool *enabledADC) {
    QSettings settings("Sensorcon", "ClimateLogger");

    enabledADC[0] = settings.value("adc0").toBool();
    enabledADC[1] = (bool)settings.value("adc1").toBool();
    enabledADC[2] = (bool)settings.value("adc2").toBool();
    enabledADC[3] = (bool)settings.value("adc3").toBool();
    enabledADC[4] = (bool)settings.value("adc4").toBool();
    enabledADC[5] = (bool)settings.value("adc5").toBool();
    enabledADC[6] = (bool)settings.value("adc6").toBool();
    enabledADC[7] = (bool)settings.value("adc7").toBool();
    enabledADC[8] = (bool)settings.value("adc8").toBool();
    enabledADC[9] = (bool)settings.value("adc9").toBool();
}
void UserSettings::getEnabledDebug(bool *enabledDebug) {
    QSettings settings("Sensorcon", "ClimateLogger");

    enabledDebug[0] = settings.value("debug1").toBool();
    enabledDebug[1] = (bool)settings.value("debug2").toBool();
    enabledDebug[2] = (bool)settings.value("debug3").toBool();
    enabledDebug[3] = (bool)settings.value("debug4").toBool();
    enabledDebug[4] = (bool)settings.value("debug5").toBool();
    enabledDebug[5] = (bool)settings.value("debug6").toBool();
    enabledDebug[6] = (bool)settings.value("debug7").toBool();
    enabledDebug[7] = (bool)settings.value("debug8").toBool();
}
int UserSettings::getLogDays() {
    QSettings settings("Sensorcon", "ClimateLogger");
    return settings.value("logDays").toInt();
}
int UserSettings::getLogHours() {
    QSettings settings("Sensorcon", "ClimateLogger");
    return settings.value("logHours").toInt();
}
int UserSettings::getLogMinutes() {
    QSettings settings("Sensorcon", "ClimateLogger");
    return settings.value("logMinutes").toInt();
}
int UserSettings::getLogSeconds() {
    QSettings settings("Sensorcon", "ClimateLogger");
    return settings.value("logSeconds").toInt();
}
int UserSettings::getLogInterval() {
    QSettings settings("Sensorcon", "ClimateLogger");
    return settings.value("logInterval").toInt();
}
