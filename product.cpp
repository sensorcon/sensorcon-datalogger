#include "product.h"
#include "constants.h"
#include <QtCore/QDebug>
#include <QInputDialog>
#include <QMessageBox>


Product::Product()
{

}

Product::Product(USBInterface &interface, DatabaseHelper &helper)
{
    usbInterface = &interface;
    dbHelper = &helper;

    for(int i = 0; i < Constants::TOTAL_SENSORS; i++) {
        availableSensors[i] = false;
    }
    for(int i = 0; i < Constants::TOTAL_ADC; i++) {
        availableADC[i] = false;
    }
    for(int i = 0; i < Constants::TOTAL_DEBUG; i++) {
        availableDebug[i] = false;
    }

    sensorNames[0] = "Temperature";
    sensorNames[1] = "Humidity";
    sensorNames[2] = "Pressure";
    sensorNames[3] = "Light";
    sensorNames[4] = "CO";
    sensorNames[5] = "H2S";
    sensorNames[6] = "IR Temperature";

    productVersion = 0;
    productRevision = 0;
    productBuild = 0;
    logPacketSize = 0;
    numSensors = 0;
    maxLogs = 0;
}

//Gets FW version
void Product::getInformation() {

    // Send command
    QByteArray data = usbInterface->readPacket(Constants::FW_VERSION_PACKET_ID);

    if(data.size() > 0) {
        productVersion = (int)data[0];
        productRevision = (int)data[1];
        productBuild = (int)data[2];
        qDebug() << "HARDWARE VERSION: " << (int)data[3];
    }

    data = usbInterface->readPacket(Constants::AVAILABLE_SENSOR_PACKET_ID);

    // Clear out variables
    for(int i = 0; i < Constants::TOTAL_SENSORS; i++) {
        availableSensors[i] = false;
    }
    for(int i = 0; i < Constants::TOTAL_ADC; i++) {
        availableADC[i] = false;
    }
    for(int i = 0; i < Constants::TOTAL_DEBUG; i++) {
        availableDebug[i] = false;
    }

    logPacketSize = 0;
    numSensors = 0;

    if(data.size() > 0) {

        // Get available sensors on board
        if((int)data[Constants::TEMPERATURE_INDEX] == 1) {
            availableSensors[Constants::TEMPERATURE_INDEX] = true;
            logPacketSize += 2;
            numSensors++;
        }
        if((int)data[Constants::HUMIDITY_INDEX] == 1) {
            availableSensors[Constants::HUMIDITY_INDEX] = true;
            logPacketSize += 2;
            numSensors++;
        }
        if((int)data[Constants::PRESSURE_INDEX] == 1) {
            availableSensors[Constants::PRESSURE_INDEX] = true;
            logPacketSize += 3;
            numSensors++;
        }
        if((int)data[Constants::LIGHT_INDEX] == 1) {
            availableSensors[Constants::LIGHT_INDEX] = true;
            logPacketSize += 4;
            numSensors++;
        }
        // only get the CO sensor if this isn't the production version
        if((int)data[Constants::CO_INDEX] == 1) {
            availableSensors[Constants::CO_INDEX] = true;
            logPacketSize += 2;
            numSensors++;
        }
        if((int)data[Constants::ADC4_INDEX] == 1) {
            availableADC[4] = true;
            logPacketSize += 2;
            numSensors++;
        }

        maxLogs = 98304 / logPacketSize;

    }

    qDebug() << "Sensors: " << availableSensors[0] << " " << availableSensors[1] << " " << availableSensors[2] << " " << availableSensors[3] << " " << availableSensors[4] << " " << availableSensors[5] << " " << availableSensors[6] <<" " << availableSensors[7];
    qDebug() << "Total segments: " << maxLogs;
    qDebug() << "Packet size: " << logPacketSize;

    settings.saveEnabledSensors(availableSensors);
    settings.saveEnabledADCs(availableADC);
    settings.saveEnabledDebug(availableDebug);
}


QString Product::getIdString() {
    QString idString;
    int sensorCt = 0;

    idString = "(";

    for(int i = 0; i < Constants::TOTAL_SENSORS; i++) {
        if(availableSensors[i] == true) {
            sensorCt++;
            if((sensorCt >= numSensors) && (sensorCt > 2)) {
                idString.append(", and ");
            }
            else if((sensorCt >= numSensors) && (sensorCt == 2)) {
                idString.append(" and ");
            }
            else if((sensorCt != 1) && (sensorCt > 1)) {
                idString.append(", ");
            }

          idString.append(sensorNames[i]);
        }
    }

    idString.append(")");

    return idString;
}


void Product::writeName(QString name) {

    QByteArray command = name.toLatin1();

    usbInterface->writeCommand(Constants::WRITE_NAME_PACKET_ID, command.size(), command);
}


QString Product::readName() {

    // Read sensor data
    QByteArray data = usbInterface->readPacket(Constants::READ_NAME_PACKET_ID);
    QString name(data);
    QString serial("00001"); // dummy data for now
    QString location(""); // dummy data for now

    if(data.size() == 0) {
        int index = 0;
        bool success = false;
        name = "DataLogger";

        do {
            name = "DataLogger";
            name.append(QString::number(index));
            success = dbHelper->insertDevice(name,serial,location);
            index++;
        } while(!success);

        writeName(name);
    }

    qDebug() << "DEVICE NAME: " << name;

    return name;
}


void Product::changeName(QWidget *parent) {

    bool ok;
    bool success = false;
    QString name = QInputDialog::getText(parent, parent->tr("Change Device Name"),
                                      parent->tr("New device name:                                           "), QLineEdit::Normal, "", &ok);
    QString mac("00:00:00:00:00:00"); // dummy data for now
    QString location(""); // dummy data for now

    if(ok) {

        // Only accept numbers and letters
        QRegExp re("[A-Za-z0-9]+");
        if(!re.exactMatch(name)) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Change Name Error");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setText("The device name must only consist of numbers and letters (a-z, A-Z, and 0-9).");
            msgBox.exec();
        }
        else if(name.size() == 0) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Change Name Error");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setText("The device name must be at least one character.");
            msgBox.exec();
        }
        else if(name.size() > 16) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Change Name Error");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setText("The device name cannot be more than 16 characters.");
            msgBox.exec();

        } else {

            success = dbHelper->insertDevice(name, mac, location);

            if(!success) {
                QMessageBox msgBox;
                msgBox.setWindowTitle("Change Name Error");
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setText("This name already exists in the database.");
                msgBox.exec();
            } else {
                writeName(name);
            }
        }
    }
}


int Product::getVersion() {
    return productVersion;
}


int Product::getRevision() {
    return productRevision;
}


int Product::getBuild() {
    return productBuild;
}


int Product::getNumSensors() {
    return numSensors;
}

int Product::getLogPacketSize() {
    return logPacketSize;
}


int Product::getMaxLogs() {
    return maxLogs;
}


void Product::getAvailableSensors(bool *sensors) {
    for(int i = 0; i < Constants::TOTAL_SENSORS; i++) {
        sensors[i] = availableSensors[i];
    }
}


void Product::getAvailableADC(bool *adc) {
    for(int i = 0; i < Constants::TOTAL_ADC; i++) {
        adc[i] = availableADC[i];
    }
}


void Product::getAvailableDebug(bool *debug) {
    for(int i = 0; i < Constants::TOTAL_DEBUG; i++) {
        debug[i] = availableDebug[i];
    }
}
