#ifndef PRODUCT_H
#define PRODUCT_H

#include "usbinterface.h"
#include "usersettings.h"
#include "databasehelper.h"

class Product
{
public:
    Product();
    Product(USBInterface &interface, DatabaseHelper &helper);
    void getInformation();
    QString getIdString();
    void changeName(QWidget *parent);
    QString readName();
    int getVersion();
    int getRevision();
    int getBuild();
    int getNumSensors();
    int getLogPacketSize();
    int getMaxLogs();
    void getAvailableSensors(bool *sensors);
    void getAvailableADC(bool *adc);
    void getAvailableDebug(bool *debug);
private:
    USBInterface *usbInterface;
    DatabaseHelper *dbHelper;
    UserSettings settings;

    QString sensorNames[8];
    bool availableSensors[8];
    bool availableADC[10];
    bool availableDebug[8];
    int productVersion;
    int productRevision;
    int productBuild;
    int logPacketSize;
    int numSensors;
    int maxLogs;

    void writeName(QString name);
};

#endif // PRODUCT_H
