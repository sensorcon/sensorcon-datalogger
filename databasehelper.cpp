#include "databasehelper.h"
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QProgressDialog>
#include <QProgressBar>
#include <QJsonObject>
#include <QJsonDocument>
#include <QMessageBox>
#include <QDateTime>

#define TIMESTAMP_INDEX     1
#define NAME_INDEX          2
#define TEMP_INDEX          3
#define HUMIDITY_INDEX      4
#define PRESSURE_INDEX      5
#define LIGHT_INDEX         6
#define CO_INDEX            7
#define H2S_INDEX           8
#define IR_TEMP_INDEX       9
#define ADC0_INDEX          10
#define ADC1_INDEX          11
#define ADC2_INDEX          12
#define ADC4_INDEX          13
#define ADC5_INDEX          14
#define ADC9_INDEX          15
#define UV_INDEX            16
#define IR_INDEX            17
#define BATT_INDEX          18
#define TASK_INDEX          19

/**
 * Constructor
 */
DatabaseHelper::DatabaseHelper() {
  db = QSqlDatabase::addDatabase("QSQLITE");

  QString directory = QStandardPaths::standardLocations(QStandardPaths::HomeLocation).at(0);
  directory.append("/logdb20.db");
  // make sure private variables are initialized
  deviceTotalCount = 0;
  logTotalCount = 0;
  logUpdateCount = 0;
  
  db.setDatabaseName(directory);

  bool success = db.open();

  insertQuery = QSqlQuery(db);

  if (!success) {
    QMessageBox::critical(0, QObject::tr("Database Error"), db.lastError().text());
  } else {        
    qDebug() << "DB Opened";
  }
}

/*
 * Destructor
 */
DatabaseHelper::~DatabaseHelper(){
  // Close the database when class object goes out of scope.
  //  Should only happen when the application is closed
  
  closeDB();
  
}


bool DatabaseHelper::isOpen() {
  return db.open();
}

/**
 * @brief closeDB
 *
 * Closes database
 */
void DatabaseHelper::closeDB() {
  // make sure it's open before we try to close it
  if(isOpen()){
    db.close();
  }
}

/**
 * @brief DatabaseHelper
 * @return true if successful
 *
 * Creates the captures, devices and logs tables
 */
bool DatabaseHelper::createTable() {

  bool ret = true;
  QString sqlString;
  if (db.isOpen()) {
    QSqlQuery query;
    sqlString = "CREATE TABLE "
            "IF NOT EXISTS  captures"
            "(capture_id integer primary key autoincrement, "
            "device_id integer, "
            "capture_ts integer, "
            "capture_note varchar, "
            "capture_count integer);";
    if(!query.exec(sqlString)){
      // query failed
     // not sure we want to leave this here in production
      QString sqlError;
      QMessageBox msgBox;
      msgBox.setWindowTitle("Error");
      sqlError = query.lastError().text();
      msgBox.setText("Error creating captures table:\n" + sqlError);
      msgBox.exec();
      ret = false;
    }
    query.clear();
    sqlString = "";
    sqlString = "CREATE TABLE "
            "IF NOT EXISTS logs "
            "(log_id integer primary key autoincrement, "
            "capture_id integer, "
            "log_ts integer, "
            "temperature real, "
            "humidity real, "
            "pressure real, "
            "light real, "
            "co real, "
            "h2s real, "
            "irTemperature real,"
            "adc0 real, "
            "adc1 real, "
            "adc2 real, "
            "adc4 real, "
            "adc5 real, "
            "adc9 real, "
            "uv real, "
            "ir real, "
            "battery real, "
            "tasks real, "
            "log_remote_id varchar);";
    if(!query.exec(sqlString)){
      // query failed
      // not sure we want to leave this here in production
      QString sqlError;
      QMessageBox msgBox;
      msgBox.setWindowTitle("Error");
      sqlError = query.lastError().text();
      msgBox.setText("Error creating logs table:\n" + sqlError);
      msgBox.exec();
      ret = false;
    }
    query.clear();
    sqlString = "";
    sqlString = "CREATE TABLE "
            "IF NOT EXISTS devices "
            "(device_id integer primary key autoincrement, "
            "device_name varchar unique, "
            "device_serial varchar, "
            "device_location varchar, "
            "device_remote_id varchar);";
    if(!query.exec(sqlString)){
      // query failed
      // not sure we want to leave this here in production
      QString sqlError;
      QMessageBox msgBox;
      msgBox.setWindowTitle("Error");
      sqlError = query.lastError().text();
      msgBox.setText("Error creating devices table:\n" + sqlError);
      msgBox.exec();
      ret = false;
    }
  }  
  return ret;
}


/**
 * @brief Allows us to add columns to existing tables
 *        Anything more involved will require a different
 *        function that re-creates the table.  Called when
 *        the application loads by ClimateLogger constructor
 *        Enter the name of the table, the column you want
 *        to add and the datatype
 * @return true if worked
 */
bool DatabaseHelper::checkTables(){
  // enter the names of the tables to be updated here
  bool ret = true;
  // Updates required on version 2.0 of the database
  ret = updateTable("devices", "device_remote_id", "varchar");
  ret = updateTable("logs", "log_remote_id", "varchar");
  return ret;
  
}

/**
 * @brief Function that will add a column to an
 *        existing table
 * @param tableName - name of the table
 * @param fieldName - name of the field to add
 * @param dataType - datatype of the field
 * @return  true if worked
 */
bool DatabaseHelper::updateTable(QString tableName, QString fieldName,
        QString dataType){
  
  bool ret = true;
  if (db.isOpen()){
    bool foundField = false;
    QString sqlString;
    QSqlQuery query;
    sqlString = "PRAGMA "
            "table_info(" + tableName + ");";
    if(!query.exec(sqlString)){
      // query failed
     // not sure we want to leave this here in production
      QString sqlError;
      QMessageBox msgBox;
      msgBox.setWindowTitle("Error");
      sqlError = query.lastError().text();
      msgBox.setText("Error getting device table pragma:\n" + sqlError);
      msgBox.exec();
      ret = false;
    }else{
      while(query.next()){
        if (query.value(1) == fieldName){
          foundField = true;
        }
      }
    }
    if (!foundField){
      qDebug() << fieldName << " not found";
      sqlString = "ALTER TABLE " + tableName;
      sqlString += " ADD COLUMN " + fieldName;
      sqlString += " " + dataType + ";";
      if(!query.exec(sqlString)){
        // query failed
        // not sure we want to leave this here in production
        qDebug() << "sql: " + sqlString;
        QString sqlError;
        QString errorText;
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        sqlError = query.lastError().text();
        errorText = "Error updating table " + tableName + ":\n" + sqlError;
        msgBox.setText( errorText);
        msgBox.exec();
        ret = false;
      }
    }    
  }
  return ret;
}

/**
 * @brief startTransaction
 *
 * Used for increased speed in writing to database. Primarily used when downloading
 * logs from the device
 */
void DatabaseHelper::startTransaction() {
  insertQuery.exec("begin exclusive transaction;");
  insertQuery.prepare("INSERT INTO logs "
          "(capture_id, "
          "log_ts, "
          "temperature, "
          "humidity, "
          "pressure, "
          "light, "
          "co, "
          "h2s, "
          "irTemperature, "
          "adc0, "
          "adc1, "
          "adc2, "
          "adc4, "
          "adc5, "
          "adc9, "
          "uv, "
          "ir, "
          "battery, "
          "tasks) "
          "VALUES "
          "(:captureID, "
          ":logTS, "
          ":temperature, "
          ":humidity, "
          ":pressure, "
          ":light, "
          ":co, "
          ":h2s, "
          ":irTemperature, "
          ":adc0, "
          ":adc1, "
          ":adc2, "
          ":adc4, "
          ":adc5, "
          ":adc9, "
          ":uv, "
          ":ir, "
          ":battery, "
          ":tasks)");
}

/**
 * @brief endTransaction
 *
 * Signals to database that large writing job is complete
 */
void DatabaseHelper::endTransaction() {
  insertQuery.exec("commit;");
}

/**
 * @brief insertLog.  Checks to see if the field is included using one of
 *        of the arrays.  If it is it binds to that value.  If not it
 *        binds to NULL.  This allows us to test for null values when
 *        we're reading the table later on, and only include those fields
 *        that are NOT null
 * @param fields - all currently available for insert
 * @param arrays - tells us which fields are being included
 * @return True if successful
 */
bool DatabaseHelper::insertLog(int captureID, int logTS, float temperature, 
        float humidity, float pressure, float light, float co, float adc4, 
        float adc5, float adc9, float uv, float ir, float battery, 
        bool availableSensors[], bool availableADC[],
        bool availableDebug[], bool coVersion) {

  bool ret = false;
  
  insertQuery.bindValue(":captureID", captureID);
  insertQuery.bindValue(":logTS", logTS);
  if(availableSensors[0]){
    insertQuery.bindValue(":temperature", temperature);
  }else{
    insertQuery.bindValue(":temperature", QVariant::Double);
  }
  if(availableSensors[1]){ 
    insertQuery.bindValue(":humidity", humidity);
  }else{
    insertQuery.bindValue(":humidity", QVariant::Double);
  }
  if(availableSensors[2]){
    insertQuery.bindValue(":pressure", (qint32) pressure);
  }else{
    insertQuery.bindValue(":pressure", QVariant::Double);
  }
  if(availableSensors[3]){
    insertQuery.bindValue(":light", (qint32) light);
  }else{
    insertQuery.bindValue(":light", QVariant::Double);
  }
  // Only get the CO sensor if CO_VERSION is true
  if(availableSensors[4] && coVersion){
    insertQuery.bindValue(":co", co);
  }else{
    insertQuery.bindValue(":co", QVariant::Double);
  }
  // these fields don't exist in the parameters
  insertQuery.bindValue(":h2s", QVariant::Double);  
  insertQuery.bindValue(":irTemperature", QVariant::Double);
  insertQuery.bindValue(":adc0", QVariant::Double);
  insertQuery.bindValue(":adc1", QVariant::Double);
  insertQuery.bindValue(":adc2", QVariant::Double);
  if(availableADC[4]){
    insertQuery.bindValue(":adc4", adc4);
  }else{
    insertQuery.bindValue(":adc4", QVariant::Double);
  }
  if(availableADC[5]){
    insertQuery.bindValue(":adc5", adc5);
  }else{
    insertQuery.bindValue(":adc5", QVariant::Double);
  }  
  if(availableADC[9]){
    insertQuery.bindValue(":adc9", adc9);
  }else{
    insertQuery.bindValue(":adc9", QVariant::Double);
  }
  if(availableDebug[0]){
    insertQuery.bindValue(":uv", uv);
  }else{
    insertQuery.bindValue(":uv", QVariant::Double);
  }  
  if(availableDebug[1]){
    insertQuery.bindValue(":ir", ir);
  }else{
    insertQuery.bindValue(":ir", QVariant::Double);
  }
  if(availableDebug[2]){
    insertQuery.bindValue(":battery", battery);
  }else{
    insertQuery.bindValue(":battery", QVariant::Double);
  }
  // tasks not included in parameter list
  //if(availableDebug[3]){
  //  insertQuery.bindValue(":tasks", tasks);
  //}else{
    insertQuery.bindValue(":tasks", QVariant::Double);
  //}

  ret = insertQuery.exec();

  return ret;
}

/*
 * @brief insertDevice
 * @param deviceName - Name from device
 * @param deviceSerial - Serial from device**
 * @param deviceLocation - Location from device**
 * 
 * **Serial and Location are not currently implemented, will
 *    be available for possible future capabilities
 */
bool DatabaseHelper::insertDevice(QString deviceName, QString deviceSerial, QString deviceLocation) {

  bool ret = false;

  insertQuery.prepare("INSERT INTO devices (device_name, device_serial, device_location) "
          "VALUES (:deviceName, :deviceSerial, :deviceLocation)");
  insertQuery.bindValue(":deviceName", deviceName);
  insertQuery.bindValue(":deviceSerial", deviceSerial);
  insertQuery.bindValue(":deviceLocation", deviceLocation);
  ret = insertQuery.exec();

  return ret;
}

/**
 * @brief insertCapture - inserts a new record into the capture table
 * @param deviceID
 * @param captureTS
 * @param captureNote 
 * @return ID of the new record, or 0 if fails
 */
int DatabaseHelper::insertCapture(int deviceID, int captureTS, QString captureNote, int captureCount) {
  int captureID = 0;
  QSqlQuery sql;
  QString sqlString;
  
  sqlString = "INSERT INTO captures ( "
          "device_id, "
          "capture_ts, "
          "capture_note, "
          "capture_count ) "
          "VALUES ("
          ":device_id, "
          ":capture_ts, "
          ":capture_note, "
          ":capture_count);";

  sql.prepare(sqlString);
  sql.bindValue(":device_id", deviceID);
  sql.bindValue(":capture_ts", captureTS);
  sql.bindValue(":capture_note", captureNote);
  sql.bindValue(":capture_count", captureCount);

  if(sql.exec()){
    // worked, now get the ID that was created    
    captureID = sql.lastInsertId().toInt();    
  }else{
    // not sure we want to leave this here in production
    QString sqlError;    
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    sqlError = sql.lastError().text();
    msgBox.setText("Error entering new capture record:\n" + sqlError);
    msgBox.exec();
  }
  return captureID;
}


/**
 * @brief Updates the capture record with the actual
 *        record count of records entered into the
 *        log table
 * @param recordCount
 */
void DatabaseHelper::updateCaptureCount(int recordCount, int captureID) {
  QSqlQuery sql;
  QString sqlString;
  if (recordCount > 0) {
    sqlString = "UPDATE captures "
            "SET capture_count = :capture_count "
            "WHERE capture_id = :capture_id;";

    sql.prepare(sqlString);
    sql.bindValue(":capture_count", recordCount);
    sql.bindValue(":capture_id", captureID);

    if (!sql.exec()) {
      // not sure we want to leave this here in production
      QString sqlError;
      QMessageBox msgBox;
      msgBox.setWindowTitle("Error");
      sqlError = sql.lastError().text();
      msgBox.setText("Error entering capture count:\n" + sqlError);
      msgBox.exec();
    }
  }
}


/**
 * @brief Queries the database for capture data to
 *        show on logs.html page.  Returns a JSON
 *        string for easier parsing on the front end
 * @return 
 */
QString DatabaseHelper::getCaptureJSON() {
  QString sqlString;
  QSqlQuery sql;
  QJsonDocument jDoc;
    
  // had to alias the fields because the sql exec returns the
  //  fields in alpha order, and I need them in order as listed
  //  so that the table will display correctly on the front end
  sqlString = "SELECT "          
          "c.capture_ts AS capture_0, "
          "d.device_name AS capture_1, "
          "c.capture_note AS capture_2, "
          "c.capture_count AS capture_3, "
          "c.capture_id AS capture_4 "
          "FROM "
          "devices d "
          "INNER JOIN captures c "
          "ON d.device_id = c.device_id "
          "ORDER BY "
          "c.capture_ts DESC";
  if (sql.exec(sqlString)) {
    // query worked

    QJsonArray jArray;
    while (sql.next()) {
      // retrieve all capture records
      QJsonObject jObject;
      for (int i = 0; i < sql.record().count(); i++) {
        // retrieve all record fields
        jObject.insert(sql.record().fieldName(i), QJsonValue::fromVariant(sql.value(i)));
      }
      // push the record into the array
      jArray.push_back(jObject);
    }
    // push array into document
    jDoc.setArray(jArray);

  } else {
    // not sure we want to leave this here in production
    QString sqlError;
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    sqlError = sql.lastError().text();
    msgBox.setText("Error retrieving capture records:\n" + sqlError);
    msgBox.exec();
  }

  // convert the json doc into a string and return it
  return jDoc.toJson();
}

/**
 * @brief Queries the database for log data for graph
 *        show on logs.html page.  Returns a JSON
 *        string for the graphing javascript
 * @note  The database records are checked for overload values
 *        here as was easier to do it here.  However, if overload
 *        values are changed they also need to be changed in
 *        dl_dashboard.js global variables, so that the dashboard
 *        chart has the same overload values
 * @return JSON string object
 */
QString DatabaseHelper::getChartDataJSON(int tempUnit, int pressUnit, int captureID) {
  QSqlQuery sql;
  QJsonDocument jDoc;

  if (!captureID || captureID == 0) {
    // should never be able to get here, but just in case
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    msgBox.setText("Graphing function must include a capture ID");
    msgBox.exec();
  } else {
    sql.prepare("SELECT "
            "l.log_ts AS Date, "
            "l.temperature AS Temp, "
            "l.humidity AS Humidity, "
            "l.pressure AS Pressure, "
            "l.light AS Light, "
            "l.co AS CO "
            "FROM "
            "logs l "
            "WHERE l.capture_id = (:captureID);");
    //  see what our sql looks like
    sql.bindValue(":captureID", captureID);
    if (sql.exec()) {
      // query worked
      QJsonArray jArray;
      while (sql.next()) {
        // retrieve all capture records
        QJsonObject jObject;
        for (int i = 0; i < sql.record().count(); i++) {
          // retrieve record fields only if not null
          if (!sql.value(i).isNull()) {
            // adjust the temperature to F if necessary
            if (sql.record().fieldName(i) == "Temp") {
              float tmp = sql.value(i).toFloat();
              // test for overloads first
              if (tmp == -9999.0){
                // negative overload is same for both C and F
                tmp = -40;
              }else if (tmp == -9998.0){
                // positive overload
                if (tempUnit == 1) {
                  tmp = 257;
                }else{
                  tmp = 125;
                }
              }else{
                if(tempUnit == 1){
                  tmp = (float) ((9 * tmp / 5) + 32);
                } 
              }
              jObject.insert(sql.record().fieldName(i), QJsonValue::fromVariant(tmp));
            }else if (sql.record().fieldName(i) == "Pressure") {
              // adjust the pressure values according to what's selected
              //  on the front end
              float pressure = sql.value(i).toFloat();
              switch (pressUnit){
                case 0:
                  // pressure in mBar
                  if (pressure == 0xFFFFFF) {
                    // overload value
                    pressure = 1100;
                  } else if (pressure == 0xFFFFFE){
                    // negative overload
                    pressure = 200;
                  }else{
                    pressure = (float) (pressure * 0.01);
                  }
                  break;
                case 1:
                  // pressure in inHg
                  if (pressure == 0xFFFFFF) {
                    // overload value
                    pressure = 32;
                  } else if (pressure == 0xFFFFFE){
                    // negative overload
                    pressure = 6;
                  }else{
                    pressure = (float) (pressure * 0.0002953);
                  }
                  break;
                case 2:
                  // pressure in Pa
                  if (pressure == 0xFFFFFF) {
                    // overload value
                    pressure = 110000;
                  } else if (pressure == 0xFFFFFE){
                    // negative overload
                    pressure = 20000;
                  }
                  // pressure is stored in db as Pa, so no conversion
                  break;      
              }
              jObject.insert(sql.record().fieldName(i), QJsonValue::fromVariant(pressure));
            }else if(sql.record().fieldName(i) == "Humidity"){
              float humidity = sql.value(i).toFloat();
              if (humidity == -9999.0 || humidity == -9998.0){
                // humidity is OL for both of these conditions
                humidity = 100;
              }
              jObject.insert(sql.record().fieldName(i), QJsonValue::fromVariant(humidity));              
            } else if(sql.record().fieldName(i) == "Light"){
             float light = sql.value(i).toFloat();
             if (light == -1.0){
               light = 20000;
             }
             jObject.insert(sql.record().fieldName(i), QJsonValue::fromVariant(light));
            }else if(sql.record().fieldName(i) == "CO"){
              float co = sql.value(i).toFloat();
              if (co == -9998.0){
                co = 500;
              }
              jObject.insert(sql.record().fieldName(i), QJsonValue::fromVariant(co));            
            }else{
              // shouldn't get here, but leave it just in case
              jObject.insert(sql.record().fieldName(i), QJsonValue::fromVariant(sql.value(i)));
            }            
          }
        }
        // push the record into the array
        jArray.push_back(jObject);
      }
      // push array into document
      jDoc.setArray(jArray);

    } else {
      // not sure we want to leave this here in production
      QString sqlError;
      QMessageBox msgBox;
      msgBox.setWindowTitle("Error");
      sqlError = sql.lastError().text();
      msgBox.setText("Error retrieving logs for graph:\n" + sqlError);
      msgBox.exec();
    }
  }
      // convert the json doc into a string and return it
    return jDoc.toJson();
  }




/**
 * @brief Deletes a capture record by id
 *        and any related records in logs
 * @param captureID
 * @return true if capture record was deleted
 */
bool DatabaseHelper::removeCapture(QWidget *parent,int captureID, bool showMessage) {
  bool captureDeleted = false;
  bool deleteRecords = false;
  QSqlQuery sqlLogs;
  QSqlQuery sqlCapture;
  // If showMessage == true we'll give the user a chance to back out
  //  Needed to add this because this message is very confusing if the 
  //    user cancels out of a download function
  if(showMessage){
    // We better give the user a chance to back out  
    QMessageBox::StandardButton mReply;
    QString msgStr = "This will permanently delete this capture record from the database.\n";
    msgStr += "Are you sure you want to continue?";
    mReply = QMessageBox::question(parent, "Delete record",msgStr,QMessageBox::Yes | QMessageBox::No);
    if(mReply == QMessageBox::Yes){
      deleteRecords = true;
    }
  }else{
    // we're not showing the message, just deleting the records
    deleteRecords = true;
   }
  if (deleteRecords) {
    // when we delete a capture we also need to delete
    //  any related records in the log file
    // delete the log files first
    sqlLogs.prepare("DELETE FROM logs WHERE capture_id = (:captureID);");
    sqlLogs.bindValue(":captureID", captureID);
    if(sqlLogs.exec()){
      // query worked, not sure if any logs were deleted, but
      // don't really care if it deleted anything in logs, so just move on
      // now get the capture record
      sqlCapture.prepare("DELETE FROM captures WHERE capture_id = (:captureID);");
      sqlCapture.bindValue(":captureID", captureID);
      if (sqlCapture.exec()) {
        // we will return ability to delete the captures file
        captureDeleted = true;
      } else {
        // not sure we want to leave this here in production
        QString sqlError;
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        sqlError = sqlCapture.lastError().text();
        msgBox.setText("Error trying to erase from capture table with capture id " + QString::number(captureID) + ":\n" + sqlError);
        msgBox.exec();
      }    
    }else{
      // not sure we want to leave this here in production
      QString sqlError;
      QMessageBox msgBox;
      msgBox.setWindowTitle("Error");
      sqlError = sqlLogs.lastError().text();
      msgBox.setText("Error trying to erase from log table with capture id " + QString::number(captureID) + ":\n" + sqlError);
      msgBox.exec();
    }
  }
  return captureDeleted;
}


/**
 * @brief alreadyCaptured
 * @param deviceID - id of the connected device
 * @param captureTS - timestamp of log in device
 * @return True if record already exists in database
 *
 */
bool DatabaseHelper::alreadyCaptured(int deviceID, int captureTS){
  bool foundRec = false;
  QSqlQuery sql;
  sql.prepare("SELECT capture_id "
          "FROM captures "
          "WHERE device_id = (:deviceID) "
           "AND capture_ts = (:captureTS);");
  sql.bindValue(":deviceID", deviceID);
  sql.bindValue(":captureTS", captureTS);
  if(sql.exec()){
    // query executed ok
    if(sql.next()){
      // found matching record
      foundRec = true;
      }
  }else{
    // not sure we want to leave this here in production
    QString sqlError;
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    sqlError = sql.lastError().text();
    msgBox.setText("Error testing for an existing capture record:\n" + sqlError);
    msgBox.exec();
  }
  return foundRec;
}

/**
 * @brief getDeviceIDFromName
 * @param deviceName - name of the connected device
 * @return device_id if found, 0 if not
 *
 */
int DatabaseHelper::getDeviceIDFromName(QString deviceName){
  int deviceID = 0;
  QSqlQuery sql;
  sql.prepare("SELECT device_id "
          "FROM devices "
          "WHERE device_name = (:deviceName);");
  sql.bindValue(":deviceName", deviceName);
  if(sql.exec()){
    // query executed ok
    if(sql.next()){
      // found matching record
      deviceID = sql.value(0).toInt();
    }
  }else{
        // not sure we want to leave this here in production
    QString sqlError;
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    sqlError = sql.lastError().text();
    msgBox.setText("Error getting device id:\n" + sqlError);
    msgBox.exec();
  }
  return deviceID;
}


/**
 * @brief removeLog
 * @param logId = id of the log to be deleted
 * @return True if successful
 *
 * Deletes a log entry in the database using timestamp
 */

/*********OBSOLETE***********************************/
bool DatabaseHelper::removeLog(int logId) {

  bool ret;
  QSqlQuery deleteQuery;

  QString command = "DELETE FROM logs WHERE log_id = ";
  command.append(logId);
  command.append(";");

  ret = deleteQuery.exec(command);

  return ret;
}

/**
 * @brief eraseAll
 * @return true if successful
 *
 * Erases all entries in database
 */
bool DatabaseHelper::eraseAll() {

  bool ret = false;

  QSqlQuery query;
  query.prepare("DELETE FROM logs;");

  ret = query.exec();

  return ret;
}
/**
 * @brief getLogs
 * @param tempUnit, pressUnit
 * @return List of all logs ONLY IF DEFAULT PARAMETER captureID == 0
 *          otherwise returns list of records for a single captureID
 *
 * Returns a list of all logs in database. The unit determines what
 * temperature unit will be used
 */

QStringList DatabaseHelper::getLogs(QWidget *parent, int tempUnit, int pressUnit, int captureID) {
  
  QStringList qList;
  // If captureID == 0 it means the user has requested all logs, which can
  //  take some time to run. Give them a chance to back out
  if(captureID == 0){
    QMessageBox::StandardButton mReply;
    QString msgStr = "This will transfer the entire database into a CSV file, which may take some time.\n";
    msgStr += "Are you sure you want to continue?";

    mReply = QMessageBox::question(parent, "Database transfer",msgStr,QMessageBox::Yes | QMessageBox::No);
    if (mReply == QMessageBox::No){
      return qList;
    }
  }else{
    // Give a message even if only transferring one file, as is easy to click
    //  on the wrong icon so give user some feedback
    QMessageBox::StandardButton mReply;
    QString msgStr = "Transfer this log to a CSV file?";
    mReply = QMessageBox::question(parent, "Log transfer",msgStr,QMessageBox::Yes | QMessageBox::No);
    if (mReply == QMessageBox::No){
      return qList;
    }
  }

  QString qRecord;
  QString headerString;
  QString sensorString = "0";
  int tmpCaptureID = 0;
  float tmp;
  bool qFirst = true;
  
  QSqlQuery sql;
  QString sqlString = "SELECT "
          "c.capture_id, "
          "d.device_name AS 'Device name', "
          //"d.device_serial AS 'Device serial', "
          //"d.device_location AS 'Device location', "
          "c.capture_ts AS 'Log timestamp', "
          "c.capture_note AS 'Note', "
          "c.capture_count AS 'Total count', "
          "l.log_ts as 'Record timestamp', "
          "round(l.temperature,2) AS Temperature, "
          "round(l.humidity,2) AS Humidity, "
          "round(l.pressure,2) AS Pressure, "
          "l.light AS Light, "
          "l.co AS CO, "
          "l.h2s AS H2S, "
          "l.irTemperature AS irTemp, "
          "l.adc0 AS Future00, "
          "l.adc1 AS Future01, "
          "l.adc2 AS Future02, "
          "l.adc4 AS Future04, "
          "l.adc5 AS Future05, "
          "l.adc9 AS Future09, "
          "l.uv AS UV, "
          "l.ir AS IR, "
          "l.battery AS Battery, "
          "l.tasks "
          "FROM "
          "devices as d "
          "INNER JOIN captures as c "
          "ON d.device_id = c.device_id "
          "INNER JOIN logs as l "
          "ON c.capture_id = l.capture_id ";

  if (captureID > 0) {
    sqlString += " WHERE c.capture_id = ";
    sqlString += QString::number(captureID);
  }else{  
     sqlString += " ORDER BY c.capture_ts DESC;";
  }
  
  // debug only, remove for production
  //  see what our sql looks like
  //QMessageBox msgBox;
  //msgBox.setText(sqlString);
  //msgBox.exec();

  if (!sql.exec(sqlString)) {
    // something went wrong, not sure we
    //  want to leave this in for production
    QString sqlError;
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    sqlError = sql.lastError().text();
    msgBox.setText("Error trying to retrieve logs with capture id " + QString::number(captureID) + ":\n" + sqlError);
    msgBox.exec();
  } else {
    // find out how many rows were returned
    // sqlite makes us go to the end of the record set
    //  to get the count, then come back to the start
    int recordCount = 0;
    int currentCount = 0;
    sql.last();
    recordCount = sql.at() + 1;
    sql.first();
    sql.previous();

    QProgressDialog progress(parent);
    progress.setWindowModality(Qt::WindowModal);
    progress.setWindowTitle("Notice");
    progress.setLabelText("Getting " + QString::number(recordCount) + " database records...");
    progress.setCancelButton(0);
    progress.setRange(0, recordCount);
    progress.setMinimumDuration(0);
    progress.show();

    while (sql.next()) {
      // Check to see if this is a new capture.  If so we need
      //  to create a new header
      if (sql.value(0) != tmpCaptureID) {
        // remember the capture id for the next set
        tmpCaptureID = sql.value(0).toInt();
        // see if this is the first capture log
        if(!qFirst){
          // If not, reset the first time through flag
          qFirst = true;
          // And add in a couple of blank lines between capture log groups
          qRecord = "\n";
          qList << qRecord;
        }
        // we need new header information, but only
        //  for first 3 fields:
        //    starts at 1, so skips capture_id
        //    d.device_name AS 'Device name'
        //    c.capture_ts AS 'Log timestamp'
        //    c.capture_note AS 'Note'
        //    c.capture_count AS 'Capture count'
        for (int i = 1; i < 5; i++) {
          // no comma at beginning of record
          if (i > 1) {
            qRecord.append(", ");
          }
          qRecord.append(sql.record().fieldName(i));
        }
        // add capture header names to list
        qList << qRecord;
        qRecord = "";

        // now get the header values
        for (int i = 1; i < 5; i++) {
          if (i > 1) {
            // no comma at beginning of record
            qRecord.append(", ");
          }
          // if it's the date format it
          if (sql.record().fieldName(i) == "Log timestamp") {
            int rTime = sql.value(i).toInt();
            QDateTime qTime;
            qTime.setTime_t(rTime);
            qRecord.append(qTime.toString("MMM-dd-yyyy hh:mm:ss A"));
          } else {
            qRecord.append(sql.value(i).toString());
          }
        }
        // add capture header values to list
        qList << qRecord;
        qRecord = "";
        // put in a blank row / line
        qList << qRecord;
        qRecord = "";
      }

      // get the field names first, but only the first time through
      if (qFirst) {
        for (int i = 5; i < sql.record().count(); i++) {
          // no comma at beginning of record
          if (i > 5) {
            qRecord.append(", ");
          }
          if (sql.record().fieldName(i) == "Record timestamp") {
            // split into day and time
            qRecord.append("Log day, Log time");
          }else if (sql.record().fieldName(i) == "Temperature") {
            QString strTempUnit = (tempUnit == 0) ? "C" : "F";
            qRecord.append("Temperature (" +  strTempUnit + ")");
          }else if (sql.record().fieldName(i) == "Pressure" && !sql.value(i).isNull()) {
            QString strPressUnit;
            switch(pressUnit){
              case 0:
                strPressUnit = "mBar";
                break;
              case 1:
                strPressUnit = "inHg";
                break;                
              case 2:
                strPressUnit = "Pa";
                break;
              default:
                strPressUnit = QString::number(pressUnit);
            }
            qRecord.append("Pressure (" +  strPressUnit + ")");
          } else if (!sql.value(i).isNull()) {
            qRecord.append(sql.record().fieldName(i));
          }
        }

        qList << qRecord;
        qRecord = "";
        qFirst = false;
      }

      // now get all of the data
      for (int i = 5; i < sql.record().count(); i++) {
        // no comma at beginning of record
        if (i > 5) {
          qRecord.append(", ");
        }

        // if it's the timestamp convert from unix to something readable
        if (sql.record().fieldName(i) == "Record timestamp") {
          int rTime = sql.value(i).toInt();
          QDateTime qTime;
          qTime.setTime_t(rTime);
          qRecord.append(qTime.toString("MMM-dd-yyyy"));
          qRecord.append(",");
          qRecord.append(qTime.toString("hh:mm:ss A"));
          // if it's the temperature may need to convert it
        } else if (sql.record().fieldName(i) == "Temperature") {
          if (sql.value(i).toInt() == -9999) {
            qRecord.append("Negative OL");
          } else if (sql.value(i).toInt() == -9998) {
            qRecord.append("OL");
          } else {
            // Convert to fahrenheit if unit is 1
            if (tempUnit == 1) {
              tmp = (float) ((9 * sql.value(i).toFloat()) / 5) + 32;
            } else {
              tmp = sql.value(i).toFloat();
            }
            qRecord.append(QString::number(tmp));
          }
          tmp = 0;
          // if it's the humidity check the values
        } else if (sql.record().fieldName(i) == "Humidity") {
          if ((sql.value(i).toInt() == -9999) || (sql.value(i).toInt() == -9998)) {
            qRecord.append("OL");
          } else {
            qRecord.append(sql.value(i).toString());
          }
          // if it's the pressure may need to convert it
        } else if (sql.record().fieldName(i) == "Pressure") {
          if (!sql.value(i).isNull()) {
            float pressure = (float) (sql.value(i).toFloat());
            if (pressure == 0xFFFFFE) {
              sensorString = "Negative OL";
            } else if (pressure == 0xFFFFFF) {
              sensorString = "OL";
            } else {
              if (pressUnit == 1) {
                // inHg
                pressure = (float) (sql.value(i).toFloat() * 0.0002953);
                //sensorString = QString::number(pressure, 'f', 2);
                sensorString = QString::number(pressure);
              } else if (pressUnit == 0) {
                // mBar
                pressure = (float) (sql.value(i).toFloat() * 0.01);
                //sensorString = QString::number(pressure, 'f', 1);
                sensorString = QString::number(pressure);
              } else {
                // Pa
                pressure = sql.value(i).toFloat();
                sensorString = QString::number(pressure);
              }
            }
            qRecord.append(sensorString);
            sensorString = "0";
          }
          // if it's the light may need to convert it  
        } else if (sql.record().fieldName(i) == "Light") {
          if (!sql.value(i).isNull()) {
            float light = sql.value(i).toFloat();
            if (light == -1) {
              sensorString = "OL";
            } else {
              sensorString = QString::number(light, 'f', 1);
            }
            qRecord.append(sensorString);
            sensorString = "0";
          }
          // if it's the CO may need to convert it  
        } else if (sql.record().fieldName(i) == "CO") {
          if (!sql.value(i).isNull()) {
            // only add something if the field is not null
            if (!sql.value(i).isNull()) {
              int co = sql.value(i).toInt();
              if (co == -9999) {
                sensorString = "Negative OL";
              } else if (co == -9998) {
                sensorString = "OL";
              } else {
                sensorString = QString::number(co);
              }
              qRecord.append(sensorString);
              sensorString = "0";
            }
          }
          // if it's the battery may need to convert it  
        } else if (sql.record().fieldName(i) == "Battery") {
          // only add something if the field is not null
          if (!sql.value(i).isNull()) {
            float battery = sql.value(i).toFloat() / 100;
            sensorString = QString::number(battery, 'f', 2);
            qRecord.append(sensorString);
            sensorString = "0";
          }
        } else {
          // just enter the value
          qRecord.append(sql.value(i).toString());
        }
      }
      //qRecord.append("\n");
      progress.setValue(currentCount);
      qList << qRecord;
      qRecord = "";
      currentCount++;      
    }
    progress.setValue(recordCount);
  }
  return qList;
}

/**
 * Returns the note inserted by the user when the
 *  log is created.  Used to set the file name
 *  for saving to CSV file
 * @param captureID
 * @return capture_note as file name
 */
QString DatabaseHelper::getLogName(int captureID){
  QString logName = "";
  QString sqlString;
  QSqlQuery sql;
  sqlString = "SELECT "
          "c.capture_note "
          "FROM "
          "captures c "
          "WHERE c.capture_id = ";
   sqlString += QString::number(captureID);
   if (!sql.exec(sqlString)) {
    // something went wrong, not sure we
    //  want to leave this in for production
    QString sqlError;
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    sqlError = sql.lastError().text();
    msgBox.setText("Error trying to retrieve logs with capture id " + QString::number(captureID) + ":\n" + sqlError);
    msgBox.exec();
  } else {
    // get to the first record
    if(sql.next()){
      logName = sql.value(0).toString();
    }
  }
  return logName;     
}

/**
 * @brief Queries the database for device data to
 *        post on remote Mongo database. 
 * @return QJsonArray
 */
QJsonArray DatabaseHelper::getLogsForRemoteDB(QWidget *parent) {
  QString sqlString;
  QSqlQuery sql;
  QJsonArray jArray;  
  int logCount = 0;
  int totalCount = 0;  
  sqlString = "SELECT "
          "l.log_id, "
          "c.capture_id, "
          "d.device_id, "
          "d.device_serial, "
          "c.capture_ts, "
          "c.capture_note, "
          "c.capture_count, "
          "l.log_ts, "
          "l.temperature, "
          "l.humidity, "
          "pressure, "
          "l.light, "
          "l.co, "
          "l.h2s, "
          "l.irTemperature, "
          "l.adc0, "
          "l.adc1, "
          "l.adc2, "
          "l.adc4, "
          "l.adc5, "
          "l.adc9, "
          "l.uv, "
          "l.ir, "
          "l.battery, "
          "l.tasks "
          "FROM "
          "devices as d "
          "INNER JOIN captures as c "
          "ON d.device_id = c.device_id "
          "INNER JOIN logs as l "
          "ON c.capture_id = l.capture_id "
          "WHERE l.log_remote_id IS NULL ";
  if (!sql.exec(sqlString)) {
    // something went wrong, not sure we
    //  want to leave this in for production
    QString sqlError;
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    sqlError = sql.lastError().text();
    msgBox.setText("Error trying to retrieve devices:\n" + sqlError);
    msgBox.exec();
  }else{
    // query worked
    sql.last();
    totalCount = sql.at() + 1;
    sql.first();
    sql.previous();
    if(totalCount > 1000){
      totalCount = 1000;
    }
    QProgressDialog progress(parent);
    progress.setWindowModality(Qt::WindowModal);
    progress.setWindowTitle("Notice");
    progress.setLabelText("Getting " + QString::number(totalCount) + " log records...");
    progress.setCancelButton(0);
    progress.setRange(0, totalCount);
    progress.setMinimumDuration(0);
    progress.show();
    while (sql.next()&& logCount < totalCount) {
      // retrieve all capture records
      QJsonObject jObject;
      for (int i = 0; i < sql.record().count(); i++) {
        // retrieve all record fields
        jObject.insert(sql.record().fieldName(i), QJsonValue::fromVariant(sql.value(i)));
      }
      // push the record into the array
      jArray.push_back(jObject);
      logCount++;
      //qDebug() << "Log count: " << logCount;
      progress.setValue(logCount);
    }
    progress.setValue(totalCount);
  }
  
  // set the local variable
  logTotalCount = logCount;
  // return the array
  return jArray;
}


/**
 * @brief Queries the database for device data to
 *        post on remote Mongo database. 
 * @return QJsonArray
 */
QJsonArray DatabaseHelper::getDevicesForRemoteDB(QWidget *parent) {
  QString sqlString;
  QSqlQuery sql;
  QJsonArray jArray;
  
  int deviceCount = 0;  
  // had to alias the fields because the sql exec returns the
  //  fields in alpha order, and I need them in order as listed
  //  so that the table will display correctly on the front end
  sqlString = "SELECT "          
          "d.device_id, "
          "d.device_name, "
          "d.device_serial, "
          "d.device_location "
          "FROM "
          "devices d "
          "WHERE "
          "d.device_remote_id IS NULL;";
  // This should be removed for production
  if (!sql.exec(sqlString)) {
    // something went wrong, not sure we
    //  want to leave this in for production
    QString sqlError;
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    sqlError = sql.lastError().text();
    msgBox.setText("Error trying to retrieve devices:\n" + sqlError);
    msgBox.exec();
  }else{
    // query worked
    sql.last();
    int totalCount = sql.at() + 1;
    sql.first();
    sql.previous();
    if(totalCount > 1000){
      totalCount = 1000;
    }
    QProgressDialog progress(parent);
    progress.setWindowModality(Qt::WindowModal);
    progress.setWindowTitle("Notice");
    progress.setLabelText("Getting " + QString::number(totalCount) + " device records...");
    progress.setCancelButton(0);
    progress.setRange(0, totalCount);
    progress.setMinimumDuration(0);
    progress.show();    
    
    while (sql.next() && deviceCount < totalCount) {
      // retrieve all capture records
      QJsonObject jObject;
      for (int i = 0; i < sql.record().count(); i++) {
        // retrieve all record fields
        jObject.insert(sql.record().fieldName(i), QJsonValue::fromVariant(sql.value(i)));
      }
      // push the record into the array
      jArray.push_back(jObject);
      deviceCount++;
      progress.setValue(deviceCount);
    }
    progress.setValue(totalCount);
  }
  
  // set the device total count variable
  deviceTotalCount = deviceCount;
  return jArray;
}

void DatabaseHelper::updateDeviceWithRemoteID(int deviceID, QString mongoID) {
  QSqlQuery sql;
  QString sqlString;
  sqlString = "UPDATE devices "
          "SET device_remote_id = :mongo_id "
          "WHERE device_id = :device_id;";

  sql.prepare(sqlString);
  sql.bindValue(":mongo_id", mongoID);
  sql.bindValue(":device_id", deviceID);

  if (!sql.exec()) {
    // not sure we want to leave this here in production
    QString sqlError;
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    sqlError = sql.lastError().text();
    msgBox.setText("Could not update device with mongo id:\n" + sqlError);
    msgBox.exec();
  }
}

void DatabaseHelper::updateLogWithRemoteID(int logID, QString mongoID) {
  QSqlQuery sql;
  QString sqlString;
  sqlString = "UPDATE logs "
          "SET log_remote_id = :mongo_id "
          "WHERE log_id = :log_id;";
  
  //sql.exec("begin exclusive transaction;");
  sql.prepare(sqlString);
  sql.bindValue(":mongo_id", mongoID);
  sql.bindValue(":log_id", logID);

  if (!sql.exec()) {
    // not sure we want to leave this here in production
    QString sqlError;
    QMessageBox msgBox;
    msgBox.setWindowTitle("Error");
    sqlError = sql.lastError().text();
    msgBox.setText("Could not update device with mongo id:\n" + sqlError);
    msgBox.exec();    
  }  
  //sql.exec("commit;");

}

int DatabaseHelper::getDeviceTotalCount(){
  return deviceTotalCount;
}

int DatabaseHelper::getLogTotalCount(){
  return logTotalCount;
}
