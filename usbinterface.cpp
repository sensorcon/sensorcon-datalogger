#include "usbinterface.h"

#include <QMessageBox>
#include <QtCore/QDebug>
#include <QtCore/QThread>

//#define PACKET_DEBUG
#define COMMAND_DEBUG

#define PACKET_ERROR_WAIT_TIME  20

USBInterface::USBInterface()
{
    connected = false;
    serial = new QSerialPort();
    numErrors = 0;
}


bool USBInterface::isConnected() {

    bool present = false;
    QString description;

    // Check to see if it is present
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        description = info.description();

        if(description == "Sensorcon Datalogger") {
            present = true;
        }
    }

    // If not, then it is not connected. Otherwise, return previous connection status
    if(present == false) {
        connected = false;
        serial->close();
        serial->reset();
    }

    return connected;
}


bool USBInterface::connect() {

    bool success = false;
    bool present = false;
    QString description;
    QString portName = "";

    // First check to see if a ClimateLogger is connected
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        description = info.description();

        //qDebug() << description;

        if(description == "Sensorcon Datalogger") {
            present = true;
            portName = info.portName();
        }
    }

    // If not, let the user know
    if(present == false) {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Device Not Connected");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("Please connect the device over USB and try again.");
        msgBox.exec();
    } else {

        serial->setPortName(portName);

        // Open the connection and set it up
        if (serial->open(QIODevice::ReadWrite)) {
            connected = true;
            success = true;

            serial->setBaudRate(QSerialPort::Baud9600);
            serial->setDataBits(QSerialPort::Data8);
            serial->setParity(QSerialPort::NoParity);
            serial->setStopBits(QSerialPort::OneStop);

            // Get versioning and sensors from product

        }
        else {
            // If it fails, let the user know and close the serial port
            qDebug() << "Serial error " << serial->error();
            connected = false;

            serial->close();

            QMessageBox msgBox;
            msgBox.setWindowTitle("Connection Error");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setText("The USB connection has failed. Please unplug and try again.");
            msgBox.exec();
        }
    }

    return success;
}

//not used
QList<int> USBInterface::readPacket(QString command, int packetId) {

    QList<int> list;
    QByteArray metadata;
    QByteArray data;
    quint16 size = 0;
    quint16 id;
    int tries = 0;
    bool fail;

    if(connected == true) {

        // Loop for attempting read
        do {
            fail = false;

#ifdef PACKET_DEBUG
            //qDebug() << "-------------------------------------------------------------------------------";
            //qDebug() << "CURRENTLY READING ID: " << packetId;
#endif

            // Send command
            serial->write(command.toUtf8().constData());
            serial->waitForBytesWritten(20);
            serial->waitForReadyRead(200);

            // Read acknowledgement
            data = serial->read(3);
#ifdef PACKET_DEBUG
            //qDebug() << "ACK: " << data.toHex();
#endif

            if(data.size() < 3) {
                fail = true;
                QThread::msleep(32);
                serial->readAll();
                serial->clear(QSerialPort::AllDirections);
                numErrors++;
                //qDebug() << "------------------------------------";
                //qDebug() << "ACK SIZE FAIL!!! ";
                //qDebug() << "Attempts: " << tries;
                //qDebug() << "Total fails: " << numErrors;
                //qDebug() << "------------------------------------";
            } else {
                if(!((data.at(0) == 'A') && (data.at(1) == 'C') && (data.at(2) == 'K'))) {
                    fail = true;
                    QThread::msleep(32);
                    serial->readAll();
                    serial->clear(QSerialPort::AllDirections);
                    numErrors++;
                    //qDebug() << "------------------------------------";
                    //qDebug() << "ACK FAIL!!!";
                    //qDebug() << "Attempts: " << tries;
                    //qDebug() << "Total fails: " << numErrors;
                    //qDebug() << "------------------------------------";
                } else {

                    // Read in first two bytes
                    serial->waitForReadyRead(200);
                    metadata = serial->read(2);
    #ifdef PACKET_DEBUG
                    //qDebug() << "PACKET METADATA: " << metadata.toHex();
    #endif

                    if(metadata.size() < 2) {
                        fail = true;
                        QThread::msleep(32);
                        serial->readAll();
                        serial->clear(QSerialPort::AllDirections);
                        numErrors++;
                        //qDebug() << "------------------------------------";
                        //qDebug() << "SIZE FAIL!!! ";
                        //qDebug() << "Attempts: " << tries;
                        //qDebug() << "Total fails: " << numErrors;
                        //qDebug() << "------------------------------------";
                    } else {

                        if(((quint16)(metadata.at(0) & 0x00FF)) != 82) {
                            fail = true;
                            QThread::msleep(32);
                            serial->readAll();
                            serial->clear(QSerialPort::AllDirections);
                            numErrors++;
                            //qDebug() << "------------------------------------";
                            //qDebug() << "ALIGNMENT FAIL!!! ";
                            //qDebug() << "Attempts: " << tries;
                            //qDebug() << "Total fails: " << numErrors;
                            //qDebug() << "------------------------------------";
                        } else {

                            // Get size
                            size = (quint16)(metadata.at(1) & 0x00FF);
                            data = serial->read(size);
    #ifdef PACKET_DEBUG
                            //qDebug() << "PACKET DATA: " << data.toHex();
    #endif

                            id = (quint16)(data.at(0) & 0x00FF);
                            if(packetId != id) {
                                fail = true;
                                QThread::msleep(32);
                                serial->readAll();
                                serial->clear(QSerialPort::AllDirections);
                                numErrors++;
                                //qDebug() << "------------------------------------";
                                //qDebug() << "ID FAIL!!! ";
                                //qDebug() << "Attempts: " << tries;
                                //qDebug() << "Total fails: " << numErrors;
                                //qDebug() << "------------------------------------";
                            } else {

                                // Calculate checksum
                                int checksum = (((quint16)data[size-2] << 8) & 0x0000FF00) + ((quint16)data[size-1] & 0x000000FF);
                                int calculatedChecksum = 0;
                                for(int i = 0; i < size-2; i++) {
                                    calculatedChecksum += ((quint16)data[i] & 0x00FF);
                                }
                                calculatedChecksum += (((quint16)metadata[0] & 0x00FF) + ((quint16)metadata[1] & 0x00FF));

                                if(checksum != calculatedChecksum) {
                                    numErrors++;
                                    //qDebug() << "------------------------------------";
                                    //qDebug() << "CHECKSUM FAIL!!! ";
                                    //qDebug() << "Calculated: " << calculatedChecksum << " Sent: " << checksum;
                                    //qDebug() << "Attempts: " << tries;
                                    //qDebug() << "Total fails: " << numErrors;
                                    //qDebug() << "------------------------------------";
                                    fail = true;
                                    QThread::msleep(32);
                                    serial->readAll();
                                    serial->clear(QSerialPort::AllDirections);
                                }
                            }
                        }
                    }
                }
            }

            tries++;
#ifdef PACKET_DEBUG
            //qDebug() << "-------------------------------------------------------------------------------";
#endif

        } while((fail == true) && (tries < 5));

        // Close connection if fail is true
        if(fail == true) {
            connected = false;
            serial->close();
            serial->reset();
        }
    }

    // Get payload
    for(int i = 0; i < size-3; i++) {
        list << ((quint16)data[i+1] & 0x00FF);
    }

    serial->clear(QSerialPort::AllDirections);

    return list;
}

//not used
bool USBInterface::writeCommand(QString command) {

    QByteArray data;
    int tries = 0;
    bool fail = true;

    if(connected == true) {

        // Loop for attempting read
        do {
            fail = false;

#ifdef PACKET_DEBUG
            //qDebug() << "-------------------------------------------------------------------------------";
            //qDebug() << "WRITING COMMAND: " << command.left(command.size() - 1);
#endif

            // Send command
            serial->write(command.toUtf8().constData());
            serial->waitForBytesWritten(20);
            serial->waitForReadyRead(200);

            // Read in all data
            data = serial->read(3);
#ifdef PACKET_DEBUG
            //qDebug() << "ACK: " << data.toHex();
#endif

            if(data.size() != 3) {
                fail = true;
                QThread::msleep(32);
                serial->readAll();
                serial->clear(QSerialPort::AllDirections);
                numErrors++;
                //qDebug() << "------------------------------------";
                //qDebug() << "SIZE FAIL!!! ";
                //qDebug() << "Attempts: " << tries;
                //qDebug() << "Total fails: " << numErrors;
                //qDebug() << "------------------------------------";
            } else {
                if(!((data.at(0) == 'A') && (data.at(1) == 'C') && (data.at(2) == 'K'))) {
                    fail = true;
                    QThread::msleep(32);
                    serial->readAll();
                    serial->clear(QSerialPort::AllDirections);
                    numErrors++;
                    //qDebug() << "------------------------------------";
                    //qDebug() << "ACK FAIL!!! ";
                    //qDebug() << "Attempts: " << tries;
                    //qDebug() << "Total fails: " << numErrors;
                    //qDebug() << "------------------------------------";
                }
            }

#ifdef PACKET_DEBUG
            //qDebug() << "-------------------------------------------------------------------------------";
#endif
            tries++;

        } while((fail == true) && (tries < 5));

        // Close connection if fail is true
        if(fail == true) {
            connected = false;
            serial->close();
            serial->reset();
        }
    }

    serial->clear(QSerialPort::AllDirections);

    return !fail;
}


QByteArray USBInterface::readLog(int segment, int packetSize, int &stat) {

    QByteArray data;
    QByteArray sensorData;
    QByteArray command;
    int fail = 0;

    int attempts = 0;

    do {
        attempts++;

        command[0] = 0x52;
        command[1] = 0x05;
        command[2] = 0x07;

        command[3] = (segment & 0x000000FF);
        command[4] = ((segment >> 8) & 0x000000FF);
        command[5] = ((segment >> 16) & 0x000000FF);
        command[6] = ((segment >> 24) & 0x000000FF);

        // Send command
        serial->write(command);
        serial->waitForBytesWritten(10);
        serial->waitForReadyRead(20);
        data = serial->readAll();
//        data = serial->read(packetSize + 5);

        //qDebug() << "LOG PACKET [" << QString::number(segment) << "]" << data.toHex();

        if(data.size() != packetSize + 5)
        {
            fail = 1;
            //qDebug() << "Data size: " << data.size();
            //qDebug() << "Packet size: " << packetSize;
            QThread::msleep(100);
        } else {

            // Calculate checksum
            unsigned int checksum = 0;
            unsigned int sentChecksum = 0;

            sentChecksum = ((int)data[packetSize + 3] & 0x000000FF) + (((int)data[packetSize + 4] << 8) & 0x0000FF00);

            int csByte = 0;
            for(int i = 0; i < packetSize + 3; i++) {
                csByte = (int)data[i];

                if(csByte < 0) {
                    csByte += 256;
                }

                checksum += csByte;
            }

            //qDebug() << "Received checksum: " << sentChecksum;
            //qDebug() << "Calculated checksum: " << checksum;

            // Compare sent and calculated checksums
            if((checksum != sentChecksum)/* || (sentChecksum == 0)*/) {
                if(sentChecksum == 65535) {
                    fail = 2;
                } else {
                    fail = 1;
                }
            } else {
                fail = 0;
            }
        }

    } while((fail > 0) && (attempts < 5));

    stat = fail;

    bool endOfMemory = true;
    for(int i = 0; i < packetSize; i++) {
        sensorData[i] = data[i+3];

        // Use this variable to check if all values are 0xFF. This indicates that
        //  it has reached the edge of memory.
        if((quint8)sensorData[i] != 0xFF) {
            endOfMemory = false;
        }
    }

    if(endOfMemory == true) {
        if(segment == 0) {
            stat = 3;
        } else {
            stat = 2;
        }
    }

    return sensorData;
}


QByteArray USBInterface::readPacket(int packetId) {

    QByteArray metaData;
    QByteArray data;
    QByteArray returnData;
    int checksum = 0;
    bool fail = false;
    int attempts = 0;
    int size = 0;

    QByteArray command;
    command.resize(3);
    command[0] = 0x52;
    command[1] = 0x01;
    command[2] = packetId;

#ifdef PACKET_DEBUG
    //qDebug() << "------------------------- READING PACKET ID: " << packetId << " -------------------------";
#endif

    do {
        fail = false;
        checksum = 0;

        // Send command
        serial->write(command);
        serial->waitForBytesWritten(20);
        serial->waitForReadyRead(20);

        // Read header
        metaData = serial->read(2);
#ifdef PACKET_DEBUG
        //qDebug() << "META DATA: " << metaData.toHex();
#endif

        if(metaData.size() < 2) {
#ifdef PACKET_DEBUG
            //qDebug() << "PACKET SIZE FAIL";
#endif
            fail = true;
            attempts++;
            QThread::msleep(PACKET_ERROR_WAIT_TIME);
            serial->readAll();
        } else {

            //Reads 0x51 instead 0x52
            if((int)metaData[0] != 0x51) {
#ifdef PACKET_DEBUG
                //qDebug() << "PACKET ALIGN FAIL";
#endif
                fail = true;
                attempts++;
                QThread::msleep(PACKET_ERROR_WAIT_TIME);
                serial->readAll();
            } else {
                size = (int)metaData[1];

                //Reads only the size that was sent
                data = serial->read(size);
#ifdef PACKET_DEBUG
                //qDebug() << "DATA: " << data.toHex();
#endif

                if(size != data.size()) {
#ifdef PACKET_DEBUG
                    //qDebug() << "DATA SIZE FAIL";
#endif
                    fail = true;
                    attempts++;
                    QThread::msleep(PACKET_ERROR_WAIT_TIME);
                    serial->readAll();
                } else {

                    //Streaming vs. logging for packedID
                    if((quint8)data[0] != packetId) {
#ifdef PACKET_DEBUG
                        //qDebug() << "ID FAIL";
#endif
                        fail = true;
                        attempts++;
                        QThread::msleep(PACKET_ERROR_WAIT_TIME);
                        serial->readAll();
                    } else {
                        int sentChecksum = ((int)data[size - 2] & 0x000000FF) + (((int)data[size - 1] << 8) & 0x0000FF00);

                        checksum = (quint8)metaData[0] + (quint8)metaData[1];

                        quint8 csByte = 0;
                        for(int i = 0; i < size - 2; i++) {
                            csByte = (quint8)data[i];

                            checksum += csByte;
                        }

                        if(sentChecksum != checksum) {
#ifdef PACKET_DEBUG
                            //qDebug() << "CHECKSUM FAIL";
                            //qDebug() << "Sent checksum: " << sentChecksum << ", Calculated: " << checksum;
#endif
                            fail = true;
                            attempts++;
                            QThread::msleep(PACKET_ERROR_WAIT_TIME);
                            serial->readAll();
                        }
                    }
                }
            }
        }

    } while((fail == true) && (attempts < 5));

    // Close connection if fail is true
    if(fail == true) {
        connected = false;
        serial->close();
        serial->reset();
    }
    else
    {
        returnData.resize(size - 3);
        for(int i = 0; i < size - 3; i++) {
            returnData[i] = data[i+1];
        }
    }

    return returnData;
}


void USBInterface::writeCommand(int packetId, int size, QByteArray data) {

    QByteArray command;
    QByteArray response;
    bool fail = false;
    int attempts = 0;

    command.resize(size + 3);
    command[0] = 0x52;
    command[1] = size + 1;
    command[2] = packetId;

    for(int i = 0; i < size; i++) {
        command[i+3] = data[i];
    }

#ifdef COMMAND_DEBUG
    //qDebug() << "------------------------- WRITING COMMAND: " << packetId << " -------------------------";
#endif

    do {
        fail = false;

        // Send command
        serial->write(command);
        serial->waitForBytesWritten(10);
        serial->waitForReadyRead(200);

        // Read ack
        response = serial->read(3);

#ifdef COMMAND_DEBUG
        //qDebug() << "RESPONSE: " << response.toHex();
#endif

        if(response.size() < 3) {
    #ifdef COMMAND_DEBUG
            //qDebug() << "ACK SIZE FAIL";
    #endif
            fail = true;
            attempts++;
            QThread::msleep(PACKET_ERROR_WAIT_TIME);
            serial->readAll();
        } else {

            //Reads 0x51 instead 0x52
            if(((int)response[0] != 0x41) && ((int)response[0] != 0x43) && ((int)response[0] != 0x4B)) {
    #ifdef COMMAND_DEBUG
                //qDebug() << "ACK ALIGN FAIL";
    #endif
                fail = true;
                attempts++;
                QThread::msleep(PACKET_ERROR_WAIT_TIME);
                serial->readAll();
            }
        }
    } while((fail == true) && (attempts < 5));

    // Close connection if fail is true
    if(fail == true) {
        connected = false;
        serial->close();
        serial->reset();
    }
}
