#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "QString"

class Constants
{
public:
    Constants();

    static const int SENSOR_STREAM_PACKET_ID;
    static const int AVAILABLE_SENSOR_PACKET_ID;
    static const int LOG_SETUP_PACKET_ID;
    static const int FLASH_ERASE_PACKET_ID;
    static const int FLASH_TEST_PACKET_ID;
    static const int FLASH_READ_PACKET_ID;
    static const int SEND_CONFIG_PACKET_ID;
    static const int READ_LOG_PACKET_ID;
    static const int FW_VERSION_PACKET_ID;
    static const int CO_ZERO_PACKET_ID;
    static const int CO_CALIBRATE_PACKET_ID;
    static const int READ_CO_CAL_PARAMS_PACKET_ID;
    static const int READ_STATUS_PACKET_ID;
    static const int WRITE_CAL_DATE_PACKET_ID;
    static const int READ_CAL_DATE_PACKET_ID;
    static const int WRITE_NAME_PACKET_ID;
    static const int READ_NAME_PACKET_ID;
    static const int RESET_LOG_PACKET_ID;
    static const int CLEAR_ZERO_ERROR_PACKET_ID;
    static const int READ_BATTERY_PACKET_ID;
    static const int BOOT_PACKET_ID;
    static const int GET_INST_BASELINE_ID;
    static const int GET_INST_SENSITIVITY_ID;
    static const int WRITE_CO_BL_ID;
    static const int WRITE_CO_SENS_ID;
    static const int WRITE_FLASH_KEY_ID;

    static const int TEMPERATURE_INDEX;
    static const int HUMIDITY_INDEX;
    static const int PRESSURE_INDEX;
    static const int LIGHT_INDEX;
    static const int CO_INDEX;
    static const int ADC4_INDEX;

    static const int TOTAL_SENSORS;
    static const int TOTAL_ADC;
    static const int TOTAL_DEBUG;

    static const QString PROGRAM_NAME;
};

#endif // CONSTANTS_H
