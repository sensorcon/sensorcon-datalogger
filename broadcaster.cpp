#include "broadcaster.h"

BroadCaster::BroadCaster(DatabaseHelper &helper) {

  // establishes connection to web service
  //  when class is instantiated
  bDeviceManager = new QNetworkAccessManager(this);
  bDeviceRequest = QNetworkRequest();

  bLogManager = new QNetworkAccessManager(this);
  bLogRequest = QNetworkRequest();

  // This request will always be a JSON request, set it here
  bDeviceRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
  bLogRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
  
  dbHelper = &helper;
 
  // set our variables;
  deviceUpdateCount = 0;
  logUpdateCount = 0;
}

void BroadCaster::postDevice(QWidget *parent) {
  
  // reset our variables;
  deviceUpdateCount = 0;
  logUpdateCount = 0;
  
  // set up the connection, signal and slot
  connect(bDeviceManager, SIGNAL(finished(QNetworkReply*)), this,
          SLOT(onDevicePostCompleted(QNetworkReply*)));

  // build our json string
  QJsonArray jArray = dbHelper->getDevicesForRemoteDB(parent);


  qDebug() << "working postDevice";
  
  // set the url for this action
  QUrl bUrl("http://dev01:3002/devices/");
  bDeviceRequest.setUrl(bUrl);

  for (int i = 0; i < jArray.count(); i++) {

    // convert the record to a JSON object
    QJsonObject jObj = jArray.at(i).toObject();

    // now post the object as a JSON string
    bDeviceManager->post(bDeviceRequest, QJsonDocument(jObj).toJson());
  }

  qDebug() << "postDevice completed";
}

void BroadCaster::onDevicePostCompleted(QNetworkReply* bDeviceReply) {

  QString bError;
  //bError = bReply->errorString();
  if (bDeviceReply->error()) {
    bError = bDeviceReply->errorString();
    qDebug() << "Error string: " << bError;

    // should force process to terminate here
  }

  // think this should move inside error function above,
  //  should otherwise always be 200?
  int bStatus = bDeviceReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
  qDebug() << "Device reply status: " << bStatus;
  // trap responses here, this should be separate function?:
  //  200 - worked
  //  201 - created?
  //  204 - no content?
  //  400 - bad request
  //  401 - not authorized
  //  403 - forbidden
  //  404 - bad url
  //  500 - internal server error
  //  503 - service unavailable

  // this block needs to get moved into an if(bStatus == 200)
  QByteArray response_data = bDeviceReply->readAll();
  QJsonDocument jsonResponse = QJsonDocument::fromJson(response_data);
  QJsonObject jsonObject = jsonResponse.object();
  QString mongoID = jsonObject["_id"].toString();
  int deviceID = jsonObject["device_id"].toInt();
  qDebug() << "MongoID: " << mongoID;
  qDebug() << "DeviceID: " << deviceID;
  dbHelper->updateDeviceWithRemoteID(deviceID, mongoID);
  // 

  bDeviceReply->deleteLater();

}

void BroadCaster::postLog(QWidget *parent) {
  
  // reset the update counter
  logUpdateCount = 0;
  deviceUpdateCount = 0;
  // set up the connection, signal and slot
  connect(bLogManager, SIGNAL(finished(QNetworkReply*)), this,
          SLOT(onLogPostCompleted(QNetworkReply*)));

  // build our json string
  QJsonArray jArray = dbHelper->getLogsForRemoteDB(parent);

  // set the url for this action
  QUrl bUrl("http://dev01:3002/logs/");
  bLogRequest.setUrl(bUrl);

  for (int i = 0; i < jArray.count(); i++) {

    // convert the record to a JSON object
    QJsonObject jObj = jArray.at(i).toObject();

    // now post the object as a JSON string
    bLogManager->post(bLogRequest, QJsonDocument(jObj).toJson());
  }
}

void BroadCaster::onLogPostCompleted(QNetworkReply* bLogReply) {

  QString bError;
  //bError = bReply->errorString();
  if (bLogReply->error()) {
    bError = bLogReply->errorString();
    qDebug() << "Log Post Completed Error string: \n" << bError;

    // should force process to terminate here
  }
 
  // think this should move inside error function above,
  //  should otherwise always be 200?
  int bStatus = bLogReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
  qDebug() << "LogReply status: " << bStatus;
  // trap responses here, this should be separate function?:
  //  200 - worked
  //  201 - created?
  //  204 - no content?
  //  400 - bad request
  //  401 - not authorized
  //  403 - forbidden
  //  404 - bad url
  //  500 - internal server error
  //  503 - service unavailable

  //qDebug() << "Connection response: " << bStatus;

  // this block needs to get moved into an if(bStatus == 200)
  QByteArray response_data = bLogReply->readAll();
  QJsonDocument jsonResponse = QJsonDocument::fromJson(response_data);
  QJsonObject jsonObject = jsonResponse.object();
  QString mongoID = jsonObject["_id"].toString();
  int logID = jsonObject["log_id"].toInt();
  qDebug() << "MongoID: " << mongoID;
  qDebug() << "LogID: " << logID;
  dbHelper->updateLogWithRemoteID(logID, mongoID);
  logUpdateCount++;
  bLogReply->deleteLater();
}

int BroadCaster::getLogUpdateCount() {
  return logUpdateCount;
}










































