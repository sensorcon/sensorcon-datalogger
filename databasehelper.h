#ifndef DATABASEHELPER_H
#define DATABASEHELPER_H

#include "QtSql/QtSql"

class DatabaseHelper {
private:
  int deviceTotalCount;
  int logTotalCount;
  int logUpdateCount;
  
public:
  DatabaseHelper();
  ~DatabaseHelper();
  QSqlDatabase db;
  bool checkTables();
  bool updateTable(QString tableName, QString fieldName, QString dataType);
  QSqlQuery insertQuery;
  bool alreadyCaptured(int deviceID, int captureTS);
  int getDeviceIDFromName(QString deviceName);
  int insertCapture(int deviceID, int captureTS, QString captureNote, int captureCount = 0);
  bool removeCapture(QWidget *parent, int captureID, bool showMessage = true);
  QString getCaptureJSON();
  QString getChartDataJSON(int tempUnit, int pressUnit, int captureID);
  void updateCaptureCount(int recordCount, int captureID);
  QJsonArray getDevicesForRemoteDB(QWidget *parent);
  QJsonArray getLogsForRemoteDB(QWidget *parent);
  void updateDeviceWithRemoteID(int deviceID, QString mongoID);
  void updateLogWithRemoteID(int logID, QString mongoID);
  int getDeviceTotalCount();
  int getLogTotalCount();
  QString getLogName(int captureID);

  public slots:
  bool isOpen();
  bool createTable();
  void startTransaction();
  void endTransaction();
  bool insertLog(int captureID, int logTS, float temperature, float humidity, float pressure,
          float light, float co, float adc4, float adc5, float adc9, float uv, float ir,
          float battery, bool availableSensors[], bool availableADC[],
          bool availableDebug[], bool coVersion);
  bool insertDevice(QString, QString, QString);
  bool removeLog(int logId);
  QStringList getLogs(QWidget *parent, int tempUnit, int pressUnit, int captureID);
  bool eraseAll();
  void closeDB();
};

#endif // DATABASEHELPER_H
