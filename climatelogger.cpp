#include "climatelogger.h"
#include "constants.h"
#include <ctime>
#include <QGraphicsWebView>
#include <QWebFrame>
#include <QtWebKitWidgets>
#include <QApplication>

#include <QMessageBox>

/**
 * This is the single point of reference for
 * UI items that should not be visible or 
 * running if this is a production release
 * @param parent
 */
#define DEVELOPMENT_VERSION  false;
#define CLOUD_VERSION false;
#define CO_VERSION true;


ClimateLogger::ClimateLogger(QWidget *parent) : Html5ApplicationViewer(parent) {

  QObject::connect(webView()->page()->mainFrame(), SIGNAL(javaScriptWindowObjectCleared()), SLOT(addToJavaScript()));

  for (int i = 0; i < Constants::TOTAL_SENSORS; i++) {
    availableSensors[i] = false;
    sensorStreamValues[i] = 0;
  }
  for (int i = 0; i < Constants::TOTAL_ADC; i++) {
    availableADC[i] = false;
    adcStreamValues[i] = 0;
  }
  for (int i = 0; i < Constants::TOTAL_DEBUG; i++) {
    availableDebug[i] = false;
    debugStreamValues[i] = 0;
  }

  streamSensors = false;
  batteryADC = 0;

  coBaseline = 0;
  coBaselineTemp = 0;
  coSensitivity = 10000;

  loggingStatus = 0;
  coZeroStatus = 0;
  coCalibrateStatus = 0;
  batteryError = 0;
  coSensorError = 0;
  coZeroError = 0;

  productName = "ClimateLogger0";

  settings.getEnabledSensors(availableSensors);
  settings.getEnabledADCs(availableADC);
  settings.getEnabledDebug(availableDebug);

  // Set start value so program knows it is first time
  previousSensitivity = -99999;

  // DEBUG!!!
  availableSensors[0] = true;
  availableSensors[1] = true;
  availableSensors[2] = false;
  availableSensors[3] = false;
  availableSensors[4] = false;
  availableSensors[5] = false;
  availableSensors[6] = false;
  availableSensors[7] = false;

  qDebug() << "Enabled sensors: " << availableSensors[0] << " " << availableSensors[1] << " " << availableSensors[2] << " " << availableSensors[3] << " " << availableSensors[4] << " " << availableSensors[5] << " " << availableSensors[6] << " " << availableSensors[7];
  qDebug() << "Enabled adcs: " << availableADC[0] << " " << availableADC[1] << " " << availableADC[2] << " " << availableADC[3] << " " << availableADC[4] << " " << availableADC[5] << " " << availableADC[6] << " " << availableADC[7] << " " << availableADC[8] << " " << availableADC[9];
  qDebug() << "Enabled debug: " << availableDebug[0] << " " << availableDebug[1] << " " << availableDebug[2] << " " << availableDebug[3] << " " << availableDebug[4] << " " << availableDebug[5] << " " << availableDebug[6] << " " << availableDebug[7];

  // if first time running will create the database tables
  bool tablesCreated = helper.createTable();
  qDebug() << "Tables created: " << tablesCreated;

  // check for any updates to the existing tables
  bool tablesUpdated = helper.checkTables();
  qDebug() << "Tables updated: " << tablesUpdated;

  logSetup = new LogSetup(usbInterface);
  logDownload = new LogDownload(usbInterface, helper);
  product = new Product(usbInterface, helper);
  broadCaster = new BroadCaster(helper);

  timer = new QTimer(this);
  QObject::connect(timer, SIGNAL(timeout()), this, SLOT(periodicTasks()));

  // Force cursor to be arrow
  //QApplication::setOverrideCursor(Qt::ArrowCursor);
  webView()->setAcceptHoverEvents(true);
}

void ClimateLogger::addToJavaScript() {
  webView()->page()->mainFrame()->addToJavaScriptWindowObject("ClimateLogger", this);
}

//Immediately on connection, this function is called to populate fields in the program
//Previously has read a different packet to see what fields to populate

bool ClimateLogger::connect() {
  bool success = false;
  success = usbInterface.connect();
  if (success == true) {
    timer->start(1000);
    product->getInformation();
    product->getAvailableSensors(availableSensors);
    product->getAvailableADC(availableADC);
    //        product->getAvailableDebug(availableDebug);

    //SW calculates Max Logs
    logSetup->setMaxLogs(product->getMaxLogs());
    logDownload->setAvailableSensors(availableSensors);
    logDownload->setAvailableADC(availableADC);
    //        logDownload->setAvailableDebug(availableDebug);
    logDownload->setLogPacketSize(product->getLogPacketSize());
    logSetup->setLogPacketSize(product->getLogPacketSize());

    readUSBValues();
  }

  return success;
}

bool ClimateLogger::isConnected() {
  return usbInterface.isConnected();
}

QString ClimateLogger::getProductIdString() {
  return product->getIdString();
}

int ClimateLogger::getProductVersion() {
  return product->getVersion();
}

int ClimateLogger::getProductRevision() {
  return product->getRevision();
}

int ClimateLogger::getProductBuild() {
  return product->getBuild();
}

bool ClimateLogger::getDevelopmentVersion() {
  return DEVELOPMENT_VERSION;
}

bool ClimateLogger::getCloudVersion() {
  return CLOUD_VERSION;
}

bool ClimateLogger::getCOVersion(){
  return CO_VERSION;
}

int ClimateLogger::getLastLogInterval() {
  return settings.getLogInterval();
}

int ClimateLogger::getLastLogDays() {
  return settings.getLogDays();
}

int ClimateLogger::getLastLogHours() {
  return settings.getLogHours();
}

int ClimateLogger::getLastLogMinutes() {
  return settings.getLogMinutes();
}

int ClimateLogger::getLastLogSeconds() {
  return settings.getLogSeconds();
}

void ClimateLogger::setSensorStreamStatus(bool status) {
  streamSensors = status;
}

bool ClimateLogger::temperatureSensorAvailable() {
  return availableSensors[Constants::TEMPERATURE_INDEX];
}

bool ClimateLogger::humiditySensorAvailable() {
  return availableSensors[Constants::HUMIDITY_INDEX];
}

bool ClimateLogger::pressureSensorAvailable() {
  return availableSensors[Constants::PRESSURE_INDEX];
}

bool ClimateLogger::lightSensorAvailable() {
  return availableSensors[3];
}

bool ClimateLogger::coSensorAvailable() {

  if(getCOVersion()){
    // if co version is true, it's ok
    //  to show the CO sensor if it's available
    return availableSensors[4]; 
    
  }else{
    // if co version is false, we don't
    //  want to show any co stuff
    return false; 
  }  
}

bool ClimateLogger::h2sSensorAvailable() {
  return availableSensors[5];
}

bool ClimateLogger::irTemperatureSensorAvailable() {
  return availableSensors[6];
}

bool ClimateLogger::adc0Available() {
  return availableADC[0];
}

bool ClimateLogger::adc1Available() {
  return availableADC[1];
}

bool ClimateLogger::adc2Available() {
  return availableADC[2];
}

bool ClimateLogger::adc4Available() {
  return availableADC[4];
}

bool ClimateLogger::adc5Available() {
  return availableADC[5];
}

bool ClimateLogger::adc9Available() {
  return availableADC[9];
}

bool ClimateLogger::uvDebugAvailable() {
  return availableDebug[0];
}

bool ClimateLogger::irDebugAvailable() {
  return availableDebug[1];
}

bool ClimateLogger::batteryDebugAvailable() {
  return availableDebug[2];
}

bool ClimateLogger::tasksDebugAvailable() {
  return availableDebug[3];
}

int ClimateLogger::getTemperatureUnit() {
  return settings.getTemperatureUnit();
}

int ClimateLogger::getPressureUnit() {
  return settings.getPressureUnit();
}

void ClimateLogger::storeTemperatureUnit(int unit) {
  settings.saveTemperatureUnit(unit);
}

void ClimateLogger::storePressureUnit(int unit) {
  settings.savePressureUnit(unit);
}

void ClimateLogger::coZero() {
  QByteArray command;
  usbInterface.writeCommand(Constants::CO_ZERO_PACKET_ID, 0, command);
}

void ClimateLogger::coCalibrate() {
  QByteArray command;
  usbInterface.writeCommand(Constants::CO_CALIBRATE_PACKET_ID, 0, command);
}

void ClimateLogger::coSendCalTimestamp() {
  QByteArray command;

  int timestamp = time(0);

  command.resize(4);

  command[0] = (timestamp & 0x000000FF);
  command[1] = ((timestamp >> 8) & 0x000000FF);
  command[2] = ((timestamp >> 16) & 0x000000FF);
  command[3] = ((timestamp >> 24) & 0x000000FF);

  usbInterface.writeCommand(Constants::WRITE_CAL_DATE_PACKET_ID, 4, command);
}

QString ClimateLogger::coGetCalTimestamp() {

  QString dateString = "";
  QByteArray data = usbInterface.readPacket(Constants::READ_CAL_DATE_PACKET_ID);

  if (data.size() == 4) {
    quint32 timestamp = ((quint32) (data[0]) & 0x000000FF) + (((quint32) (data[1]) << 8) & 0x0000FF00) + (((quint32) (data[2]) << 16) & 0x00FF0000) + (((quint32) (data[3]) << 24) & 0xFF000000);

    if (timestamp == 0) {
      dateString = "Not Calibrated";
    } else {
      QDateTime time;
      time.setTime_t(timestamp);
      dateString = time.toString("M/d/yyyy");
    }
  }

  return dateString;
}

int ClimateLogger::getDaysFromLastCalibration() {

  int daysFromCal = 0;
  quint32 currentTimestamp = time(0);
  quint32 timestamp = 0;

  QByteArray data = usbInterface.readPacket(Constants::READ_CAL_DATE_PACKET_ID);

  if (data.size() == 4) {
    // Get timestamp from last calibration
    timestamp = ((quint32) (data[0]) & 0x000000FF) + (((quint32) (data[1]) << 8) & 0x0000FF00) + (((quint32) (data[2]) << 16) & 0x00FF0000) + (((quint32) (data[3]) << 24) & 0xFF000000);

    // DEBUG!!!
    //        timestamp = 1414249200; // 10/25/2014
    //        timestamp = 1414162800; // 10/24/2014
    //        timestamp = 1398092400; // 4/21/2014

    if (timestamp != 0) {
      quint32 secondsDiff = currentTimestamp - timestamp;

      // Divide by number of seconds in a day to get days until cal
      daysFromCal = secondsDiff / 86400;
      qDebug() << "Seconds difference: " << secondsDiff << ", Days from cal: " << daysFromCal;
    }
  }

  return daysFromCal;
}

QString ClimateLogger::readTemperature() {
  if ((qint16) sensorStreamValues[Constants::TEMPERATURE_INDEX] == -9999) {
    return "-OL";
  } else if ((qint16) sensorStreamValues[Constants::TEMPERATURE_INDEX] == -9998) {
    return "OL";
  } else {
    return QString::number(sensorStreamValues[Constants::TEMPERATURE_INDEX], 'f', 1);
  }
}


// Humidity can easily jump over -OL and wrap around to positive OL,
//  so this function will ONLY return positive OL

QString ClimateLogger::readHumidity() {
  if ((qint16) sensorStreamValues[Constants::HUMIDITY_INDEX] == -9999) {
    return "OL";
  } else if ((qint16) sensorStreamValues[Constants::HUMIDITY_INDEX] == -9998) {
    return "OL";
  } else {
    return QString::number(sensorStreamValues[Constants::HUMIDITY_INDEX], 'f', 1);
  }
}

QString ClimateLogger::readPressure() {
  int pressureUnits = settings.getPressureUnit();
  if (sensorStreamValues[Constants::PRESSURE_INDEX] == 0xFFFFFE) {
    return "-OL";
  } else if (sensorStreamValues[Constants::PRESSURE_INDEX] == 0xFFFFFF) {
    return "OL";
  } else {
    if (pressureUnits == 2) {
      // return Pa in whole numbers
      return QString::number(sensorStreamValues[Constants::PRESSURE_INDEX]);
    } else if (pressureUnits == 1){
      // return inHg with 2 decimals
      return QString::number(sensorStreamValues[Constants::PRESSURE_INDEX], 'f', 2);
    } else {
      // return Bar with 1 decimal
      return QString::number(sensorStreamValues[Constants::PRESSURE_INDEX], 'f', 1);
    }
  }
}

QString ClimateLogger::readLight() {
  if (sensorStreamValues[3] == 0xFFFFFFFF) {
    return "OL";
  } else {
    return QString::number((qint32) sensorStreamValues[3]);
  }
}

QString ClimateLogger::readCO() {
  if (coSensorError == 1) {
    return "Error";
  } else if ((qint16) sensorStreamValues[4] == -9999) {
    return "-OL";
  } else if ((qint16) sensorStreamValues[4] == -9998) {
    return "OL";
  } else {
    return QString::number(sensorStreamValues[4]);
  }
}

QString ClimateLogger::readADC4() {
  return QString::number(adcStreamValues[4]);
}

float ClimateLogger::readUV() {
  return debugStreamValues[0];
}

float ClimateLogger::readIR() {
  return debugStreamValues[1];
}

float ClimateLogger::readBattery() {
  return debugStreamValues[2];
}

int ClimateLogger::readADC5() {
  return adcStreamValues[5];
}

int ClimateLogger::readADC9() {
  return adcStreamValues[9];
}

QString ClimateLogger::readName() {
  productName = product->readName();
  return productName;
}

void ClimateLogger::changeName() {
  product->changeName(this);
}

int ClimateLogger::checkCoZeroStatus() {
  return (int) coZeroStatus;
}

int ClimateLogger::checkCoZeroError() {
  return (int) coZeroError;
}

int ClimateLogger::checkCoCalStatus() {
  return (int) coCalibrateStatus;
}

void ClimateLogger::clearCoZeroError() {
  QByteArray command;
  usbInterface.writeCommand(Constants::CLEAR_ZERO_ERROR_PACKET_ID, 0, command);
}

bool ClimateLogger::checkForNewLogs() {
  return logDownload->checkForNewLogs(productName);
}

int ClimateLogger::downloadFromDevice() {
  bool coVersion = getCOVersion();
  return logDownload->downloadFromDevice(this, productName, coVersion);
}

void ClimateLogger::openCSV() {
  logDownload->openCSV();
}

void ClimateLogger::generateCSV(int captureID) {
  logDownload->generateCSV(this, captureID);
}

QString ClimateLogger::getCaptureRecordsJSON() {
  return helper.getCaptureJSON();
}

QString ClimateLogger::getChartDataJSON(int captureID) {
  return helper.getChartDataJSON(settings.getTemperatureUnit(), settings.getPressureUnit(), captureID);
}

bool ClimateLogger::deleteCapture(int captureID) {
  return helper.removeCapture(this, captureID);
}

void ClimateLogger::eraseDB() {

  QMessageBox::StandardButton reply;
  reply = QMessageBox::question(this, tr("Erase Database"),
          "WARNING: This will erase all stored logs in your history. Continue?",
          QMessageBox::Yes | QMessageBox::No);

  if (reply == QMessageBox::Yes) {
    helper.eraseAll();
  }
}

int ClimateLogger::checkLoggingStatus() {
  return (int) loggingStatus;
}

int ClimateLogger::getCoBaseline() {
  return coBaseline;
}

int ClimateLogger::getCoBaselineTemp() {
  return coBaselineTemp;
}

QString ClimateLogger::getCoSensitivity() {
  previousSensitivity = coSensitivity;
  float sens = (float) coSensitivity / 1000;

  return QString::number(sens, 'f', 2);
}

int ClimateLogger::getCoBaselineInst() {
  return coBaselineInst;
}

QString ClimateLogger::getCoSensitivityInst() {
  float sens = (float) coSensitivityInst / 1000;

  return QString::number(sens, 'f', 2);
}

void ClimateLogger::getInstBaseline() {
  QByteArray command;
  usbInterface.writeCommand(Constants::GET_INST_BASELINE_ID, 0, command);
}

void ClimateLogger::getInstSensitivity() {
  QByteArray command;
  usbInterface.writeCommand(Constants::GET_INST_SENSITIVITY_ID, 0, command);
}

void ClimateLogger::writeCOBaseline() {
  QByteArray command;
  usbInterface.writeCommand(Constants::WRITE_CO_BL_ID, 0, command);
}

void ClimateLogger::writeCOSensitivity() {
  QByteArray command;
  usbInterface.writeCommand(Constants::WRITE_CO_SENS_ID, 0, command);
}

float ClimateLogger::readBatteryVoltage() {

  float battery = -1;

  if (batteryADC != 65535) {
    // Calculate voltage
    battery = ((float) batteryADC * 2 * 3) / 4095;
  }

  return battery;
}

int ClimateLogger::getMaxEndTime(int startTimestamp, int interval) {
  return logSetup->getMaxEndTime(startTimestamp, interval);
}

int ClimateLogger::getMaxLogs() {
  return logSetup->getMaxLogs();
}

void ClimateLogger::setUpLog(int duration, int interval) {
  logSetup->setUpLog(this, duration, interval);
}

void ClimateLogger::setUpLog(int startTimestamp, int duration, int interval) {
  logSetup->setUpLog(this, startTimestamp, duration, interval);
}

void ClimateLogger::setUpLogLater(QString startDate, QString startTime, QString endDate, QString endTime, int startAmPm, int endAmPm, QString interval) {
  logSetup->setUpLogLater(this, startDate, startTime, endDate, endTime, startAmPm, endAmPm, interval);
}

void ClimateLogger::setUpLogLater(QString startDate, QString startTime, int startAmPm, QString days, QString hours, QString minutes, QString seconds, QString interval) {
  logSetup->setUpLogLater(this, startDate, startTime, startAmPm, days, hours, minutes, seconds, interval);
}

void ClimateLogger::setUpLogNow(QString endDate, QString endTime, int endAmPm, QString interval) {
  logSetup->setUpLogNow(this, endDate, endTime, endAmPm, interval);
}

void ClimateLogger::setUpLogNow(QString days, QString hours, QString minutes, QString seconds, QString interval) {
  logSetup->setUpLogNow(this, days, hours, minutes, seconds, interval);
}

void ClimateLogger::resetLog() {

  QMessageBox::StandardButton reply;
  reply = QMessageBox::information(this,
          tr("Cancel Log"),
          "Are you sure you would like to cancel the current log?",
          QMessageBox::Yes | QMessageBox::No);
  if (reply == QMessageBox::Yes) {
    QByteArray command;
    usbInterface.writeCommand(Constants::RESET_LOG_PACKET_ID, 0, command);
  }
}

QString ClimateLogger::getLogTimestamp() {
  return logDownload->getLogTimestamp();
}

void ClimateLogger::readConfig() {
  logDownload->readConfig();
}

int ClimateLogger::getUnixTimestampValue() {
  return logDownload->getUnixTimestampValue();
}

int ClimateLogger::getOffsetValue() {
  return logDownload->getOffsetValue();
}

int ClimateLogger::getTotalLogValue() {
  return logDownload->getTotalLogValue();
}

int ClimateLogger::getIntervalValue() {
  return logDownload->getIntervalValue();
}

bool ClimateLogger::advancedDialog() {
  bool response = false;

  QMessageBox::StandardButton reply;
  reply = QMessageBox::question(this, "Advanced Settings", "This option is only for advanced users. Are you sure you want to continue?",
          QMessageBox::Yes | QMessageBox::No);
  if (reply == QMessageBox::Yes) {
    response = true;
  }

  return response;
}

void ClimateLogger::showSensorconSite() {
  QString sUrl = "https://sensorcon.com/pages/software";
  QDesktopServices::openUrl(QUrl(sUrl, QUrl::TolerantMode));
}

/**
 * @brief Returns the estimated time in seconds the device will last assuming full battery
 *
 * @param interval  Interval of log
 * @return          Number of seconds it will last
 */
int ClimateLogger::getEstimatedBattery(int interval) {

  float baseCurrent = 65; // Base current of device (about 65 uA)
  float ledCurrent = 35; // Average current per second of led blink
  float sensorCurrent = 0; // Total additional current based on sensor sampling

  // If CO sensor is present, add 15 uA to base current
  if (availableSensors[4] == true) {
    baseCurrent += 15;
  }

  // Add remaining sensor average currents
  if (availableSensors[0] == true) {
    sensorCurrent += 62;
  }
  if (availableSensors[1] == true) {
    sensorCurrent += 40;
  }
  if (availableSensors[2] == true) {
    sensorCurrent += 230;
  }
  if (availableSensors[3] == true) {
    sensorCurrent += 100;
  }

  // Get average sensor and LED current based on interval
  float averageLEDCurrent = ledCurrent / interval;
  float averageSensorCurrent = sensorCurrent / interval;

  // Add to base current
  float totalCurrent = baseCurrent + averageLEDCurrent + averageSensorCurrent;

  // Calculate total life based on battery mAH
  int totalSeconds = int((255 / (totalCurrent / 1000)) * 3600);

  return totalSeconds;
}


//Periodic Tasks runs once per second

void ClimateLogger::periodicTasks() {

  // Stop timer if disconnected
  if (usbInterface.isConnected() == false) {
    timer->stop();
  }

  readUSBValues();
}

//New function - makes sure the red reset button shows up immediately instead of after a delay
//Makes sure that all important flags read on connection

void ClimateLogger::readUSBValues() {

  // Read sensor data
  QByteArray data;
  int index = 0;

  // Only stream sensors when on dashboard
  if (streamSensors == true) {

    // Read sensor data
    data = usbInterface.readPacket(Constants::SENSOR_STREAM_PACKET_ID);
    index = 0;

    //Checks to make sure a packet is actually there, if data.size == 0, there is no packet
    //Streams all sensor values to interface
    if (data.size() > 0) {
      if (availableSensors[Constants::TEMPERATURE_INDEX] == true) {
        qint16 tempVal = (((int) (data[index++]) & 0x000000FF) + (((int) (data[index++]) << 8) & 0x0000FF00));

        // Check for overload
        if (tempVal > -9998) {
          float temperature = (float) tempVal / 10;

          if (settings.getTemperatureUnit() == 1) {
            temperature = (float) ((9 * temperature / 5) + 32);
          }
          sensorStreamValues[Constants::TEMPERATURE_INDEX] = temperature;
        } else {
          sensorStreamValues[Constants::TEMPERATURE_INDEX] = (float) tempVal;
        }
      }
      if (availableSensors[Constants::HUMIDITY_INDEX] == true) {
        qint16 humVal = (((int) (data[index++]) & 0x000000FF) + (((int) (data[index++]) << 8) & 0x0000FF00));

        // Check for overload
        if (humVal > -9998) {
          sensorStreamValues[Constants::HUMIDITY_INDEX] = (float) humVal / 10;
        } else {
          sensorStreamValues[Constants::HUMIDITY_INDEX] = (float) humVal;
        }
      }
      if (availableSensors[Constants::PRESSURE_INDEX] == true) {
        float pressure = (float) (((int) (data[index++]) & 0x000000FF) + (((int) (data[index++]) << 8) & 0x0000FF00) + (((int) (data[index++]) << 16) & 0x00FF0000));

        // Check for overload
        if (pressure < 0xFFFFFE) {
          pressure = pressure / 10;
          // defaults to Pa
          if (settings.getPressureUnit() == 1) {
            // inHg
            pressure = pressure * 0.0002953;
          } else if (settings.getPressureUnit() == 0) {
            // mBar
            pressure = pressure * 0.01;
          }

          sensorStreamValues[Constants::PRESSURE_INDEX] = pressure;
        } else {
          sensorStreamValues[Constants::PRESSURE_INDEX] = pressure;
        }
      }
      if (availableSensors[Constants::LIGHT_INDEX] == true) {
        float light = (float) (((int) (data[index++]) & 0x000000FF) + (((int) (data[index++]) << 8) & 0x0000FF00) + (((int) (data[index++]) << 16) & 0x00FF0000) + (((int) (data[index++]) << 24) & 0xFF000000));

        // Check for overload
        if (light < 0xFFFFFFFF) {
          sensorStreamValues[Constants::LIGHT_INDEX] = light / 10;
        } else {
          sensorStreamValues[Constants::LIGHT_INDEX] = light;
        }
      }
      if (availableSensors[Constants::CO_INDEX] == true) {
        qint16 coVal = (((int) (data[index++]) & 0x000000FF) + (((int) (data[index++]) << 8) & 0x0000FF00));

        // Check for overload
        if (coVal > -9998) {
          sensorStreamValues[Constants::CO_INDEX] = (float) (coVal) / 10;

        } else {
          sensorStreamValues[Constants::CO_INDEX] = (float) (coVal);
        }
      }
      if (availableADC[4] == true) {
        adcStreamValues[4] = (float) (((int) (data[index++]) & 0x000000FF) + (((int) (data[index++]) << 8) & 0x0000FF00));
      }
    }
  }

  // Get ADC 5 for battery voltage
  data = usbInterface.readPacket(Constants::READ_BATTERY_PACKET_ID);
  index = 0;

  batteryADC = (float) (((int) (data[index++]) & 0x00FF) + (((int) (data[index++]) << 8) & 0xFF00));

  // If CO sensor present, read CO cal data
  if (availableSensors[Constants::CO_INDEX] == true) {
    data = usbInterface.readPacket(Constants::READ_CO_CAL_PARAMS_PACKET_ID);
    index = 0;

    quint16 coBaselineADC = ((int) (data[index++]) & 0x00FF) + (((int) (data[index++]) << 8) & 0xFF00);
    coBaseline = ((((qint32) coBaselineADC * 10) - 7475) * 732) / 3000;

    coBaselineADC = ((int) (data[index++]) & 0x00FF) + (((int) (data[index++]) << 8) & 0xFF00);
    coBaselineTemp = ((((qint32) coBaselineADC * 10) - 7475) * 732) / 3000;

    coSensitivity = ((int) (data[index++]) & 0x00FF) + (((int) (data[index++]) << 8) & 0xFF00);

    coBaselineADC = ((int) (data[index++]) & 0x00FF) + (((int) (data[index++]) << 8) & 0xFF00);
    coBaselineInst = ((((qint32) coBaselineADC * 10) - 7475) * 732) / 3000;

    coSensitivityInst = ((int) (data[index++]) & 0x00FF) + (((int) (data[index++]) << 8) & 0xFF00);
  }

  // Read status data
  //Status packet contains logging status
  data = usbInterface.readPacket(Constants::READ_STATUS_PACKET_ID);
  index = 0;

  loggingStatus = (quint8) (data[index++]);
  coZeroStatus = (quint8) (data[index++]);
  coCalibrateStatus = (quint8) (data[index++]);
  batteryError = (quint8) (data[index++]);
  coSensorError = (quint8) (data[index++]);
  coZeroError = (quint8) (data[index++]);

  //        qDebug() << loggingStatus << " " << coZeroStatus << " " << coCalibrateStatus << " " << batteryError << " " << coSensorError << " " << coZeroError;
}

void ClimateLogger::postLocalDB() {
  qDebug() << "working here?";
  broadCaster->postDevice(this);
  broadCaster->postLog(this);
}

int ClimateLogger::getLogUpdateCount() {
  return broadCaster->getLogUpdateCount();
  return 0;
}

int ClimateLogger::getLogTotalCount() {
  int logCount = helper.getLogTotalCount();
  //qDebug() << "climateLogger total update log count: " << logCount;
  return logCount;
  // return 0;
}

/**
 * Displays the QT message on the front end
 *  title has the default value of 'Data Logger',
 *  if you want something different just include
 *  the title argument
 * @param message
 * @param title - default if not included
 */
void ClimateLogger::displayMessage(QString message, QString title) {
  QMessageBox msgBox;
  msgBox.setWindowTitle(title);
  msgBox.setText(message);
  msgBox.exec();
}
