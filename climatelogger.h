#ifndef CLIMATELOGGER_H
#define CLIMATELOGGER_H

#include "databasehelper.h"
#include "usbinterface.h"
#include "product.h"
#include "logsetup.h"
#include "logdownload.h"
#include "usersettings.h"
#include "broadcaster.h"

#include "html5applicationviewer/html5applicationviewer.h"

class ClimateLogger : public Html5ApplicationViewer
{
    Q_OBJECT

public:
    explicit ClimateLogger(QWidget *parent=0);

    bool h2sSensorAvailable();
    bool irTemperatureSensorAvailable();
    bool adc0Available();
    bool adc1Available();
    bool adc2Available();
    bool adc5Available();
    bool adc9Available();
    bool uvDebugAvailable();
    bool irDebugAvailable();
    bool batteryDebugAvailable();
    bool tasksDebugAvailable();
    float readUV();
    float readIR();
    float readBattery();
    int readADC5();
    int readADC9();

public slots:
    bool connect();
    bool isConnected();

    int getProductVersion();
    int getProductRevision();
    int getProductBuild();
    bool getDevelopmentVersion();
    bool getCloudVersion();
    bool getCOVersion();
    QString getProductIdString();
    int getLastLogInterval();
    int getLastLogDays();
    int getLastLogHours();
    int getLastLogMinutes();
    int getLastLogSeconds();
    void setSensorStreamStatus(bool status);

    bool temperatureSensorAvailable();
    bool humiditySensorAvailable();
    bool pressureSensorAvailable();
    bool lightSensorAvailable();
    bool coSensorAvailable();
    bool adc4Available();

    int getTemperatureUnit();
    int getPressureUnit();
    void storeTemperatureUnit(int unit);
    void storePressureUnit(int unit);

    QString readTemperature();
    QString readHumidity();
    QString readPressure();
    QString readLight();
    QString readCO();
    QString readADC4();
    float readBatteryVoltage();

    void changeName();
    QString readName();

    void coCalibrate();
    void coZero();
    int getCoBaseline();
    int getCoBaselineTemp();
    QString getCoSensitivity();
    int getCoBaselineInst();
    QString getCoSensitivityInst();
    void coSendCalTimestamp();
    QString coGetCalTimestamp();
    int getDaysFromLastCalibration();

    int checkLoggingStatus();
    int checkCoZeroStatus();
    int checkCoZeroError();
    int checkCoCalStatus();
    void clearCoZeroError();

    void getInstBaseline();
    void getInstSensitivity();
    void writeCOBaseline();
    void writeCOSensitivity();

    int getMaxEndTime(int startTimestamp, int interval);
    void setUpLog(int duration, int interval);
    void setUpLog(int startTimestamp, int duration, int interval);
    int getMaxLogs();
    int getEstimatedBattery(int interval);

    void setUpLogLater(QString startDate, QString startTime, QString endDate, QString endTime, int startAmPm, int endAmPm, QString interval);
    void setUpLogLater(QString startDate, QString startTime, int startAmPm, QString days, QString hours, QString minutes, QString seconds, QString interval);
    void setUpLogNow(QString endDate, QString endTime, int endAmPm, QString interval);
    void setUpLogNow(QString days, QString hours, QString minutes, QString seconds, QString interval);
    void resetLog();
    QString getLogTimestamp();
    int getUnixTimestampValue();
    int getOffsetValue();
    int getTotalLogValue();
    int getIntervalValue();
    void readConfig();

    int downloadFromDevice();
    void openCSV();
    bool checkForNewLogs();
    void generateCSV(int captureID = 0);
    void eraseDB();
    bool deleteCapture(int captureID);
    QString getCaptureRecordsJSON();
    QString getChartDataJSON(int captureID);

    bool advancedDialog();
    void showSensorconSite();
    
    void postLocalDB();
    int getLogUpdateCount();
    int getLogTotalCount();
    
    void displayMessage(QString message, QString title="Data Logger");

private:
    Product *product;
    LogSetup *logSetup;
    LogDownload *logDownload;
    DatabaseHelper helper;
    USBInterface usbInterface;
    UserSettings settings;
    BroadCaster *broadCaster;

    QTimer *timer;

    bool availableSensors[8];
    bool availableADC[10];
    bool availableDebug[8];
    float sensorStreamValues[8];
    float adcStreamValues[10];
    float debugStreamValues[8];
    bool streamSensors;

    quint16 batteryADC;

    qint16 coBaseline;
    qint16 coBaselineTemp;
    quint16 coSensitivity;
    qint16 coBaselineInst;
    quint16 coSensitivityInst;

    quint8 loggingStatus;
    quint8 coZeroStatus;
    quint8 coCalibrateStatus;
    quint8 batteryError;
    quint8 coSensorError;
    quint8 coZeroError;

    QString productName;

    int previousSensitivity;
    
private slots:
    void addToJavaScript();
    void periodicTasks();
    void readUSBValues();
};

#endif // CLIMATELOGGER_H
