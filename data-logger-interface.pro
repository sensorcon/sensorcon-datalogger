greaterThan(QT_MAJOR_VERSION, 4):QT += widgets webkitwidgets

# Add more folders to ship with the application, here
folder_01.source = interface
folder_01.target = .
DEPLOYMENTFOLDERS = folder_01

# Define TOUCH_OPTIMIZED_NAVIGATION for touch optimization and flicking
#DEFINES += TOUCH_OPTIMIZED_NAVIGATION

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    interfaceupdater.cpp \
    databasehelper.cpp \
    usersettings.cpp \
    usbinterface.cpp \
    product.cpp \
    constants.cpp \
    logsetup.cpp \
    logdownload.cpp \
    climatelogger.cpp \
    broadcaster.cpp

QT += serialport \
      sql \
      network

# Please do not modify the following two lines. Required for deployment.
include(html5applicationviewer/html5applicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    interfaceupdater.h \
    databasehelper.h \
    usersettings.h \
    usbinterface.h \
    product.h \
    constants.h \
    logsetup.h \
    logdownload.h \
    climatelogger.h \
    broadcaster.h

win32:RC_ICONS += icon64.ico

DISTFILES += \
    interface/templates/admin/charts-flotcharts_dl.js
