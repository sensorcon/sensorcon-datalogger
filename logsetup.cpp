#include "logsetup.h"
#include "constants.h"
#include <QMessageBox>
#include <ctime>

LogSetup::LogSetup()
{
}


LogSetup::LogSetup(USBInterface &interface)
{
    usbInterface = &interface;
    maxLogs = 2000;
}


void LogSetup::setMaxLogs(int mLogs) {
    maxLogs = mLogs;
}


void LogSetup::setLogPacketSize(int pSize) {
    packetSize = pSize;
}


bool LogSetup::testFlashErase() {
    int stat = false;
    int fail = 0;

    QByteArray data = usbInterface->readLog(0, packetSize, fail);

    if(fail == 3) {
        stat = true;
    }

    return stat;
}


bool LogSetup::testLogConfig(int unixStamp, int offset, int interval, int numLogs) {

    int stat = true;
    int unixStampTemp = 0;
    int offsetTemp = 0;
    int intervalTemp = 0;
    int numLogsTemp = 0;

    QByteArray data = usbInterface->readPacket(Constants::SEND_CONFIG_PACKET_ID);


    unixStampTemp = ((int)data[0] & 0x000000FF) + (((int)data[1] << 8) & 0x0000FF00) + (((int)data[2] << 16) & 0x00FF0000) + (((int)data[3] << 24) & 0xFF000000);
    offsetTemp = ((int)data[4] & 0x000000FF) + (((int)data[5] << 8) & 0x0000FF00) + (((int)data[6] << 16) & 0x00FF0000) + (((int)data[7] << 24) & 0xFF000000);
    intervalTemp = ((int)data[8] & 0x000000FF) + (((int)data[9] << 8) & 0x0000FF00) + (((int)data[10] << 16) & 0x00FF0000) + (((int)data[11] << 24) & 0xFF000000);
    numLogsTemp = ((int)data[12] & 0x000000FF) + (((int)data[13] << 8) & 0x0000FF00);

    if(unixStampTemp != unixStamp) {
        stat = false;
    }
    if(offsetTemp != offset) {
        stat = false;
    }
    if(intervalTemp != interval) {
        stat = false;
    }
    if(numLogsTemp != numLogs) {
        stat = false;
    }

    return stat;
}


/**
 * @brief Returns the maximum possible end time of a log as a unix timestamp
 *
 * @param startTimestamp    Start time of log
 * @return                  Max end time of log
 */
int LogSetup::getMaxEndTime(int startTimestamp, int interval) {
    return startTimestamp + (interval * maxLogs);
}

/**
 * @brief Returns the private constant maxLogs
 *
 * @param 
 * @return                  maxLogs
 * @comment                 to calculate possible unix date overflow in dl_capture.js
 */
int LogSetup::getMaxLogs(){
  return maxLogs;
}

/**
 * @brief Sets up a log that starts now
 *
 * @param duration          Total log time in seconds
 * @param interval          Interval between logs in seconds
 */
void LogSetup::setUpLog(QWidget *parent, int duration, int interval) {

    if(validateErase(parent)) {

        if(validateConnection() && validateLoggingStatus()) {

            settings.saveInterval(interval);

            int offsetSecs = 0;     // Offset is 0 when starting now

            if(validateInterval(QString::number(interval), duration)) {

                int totalLogs = duration / interval;

                if(validateTotalLogs(totalLogs)) {

                    int timestamp = time(0); // Use current timestamp for start time

                    bool success = sendConfiguration(timestamp, offsetSecs, interval, totalLogs);

                    if(success == true) {
                        QDateTime startDateTime = QDateTime::currentDateTime();
                        QDateTime endDateTime = startDateTime.addSecs(duration);
                        showLogSetup(startDateTime, endDateTime, QString::number(interval));
                    }
                }
            }
        }
    }
}


/**
 * @brief Sets up a log that starts at a later time
 *
 * @param startTimestamp    Start time of the log
 * @param duration          Total log time in seconds
 * @param interval          Interval between logs in seconds
 */
void LogSetup::setUpLog(QWidget *parent, int startTimestamp, int duration, int interval) {

    if(validateErase(parent)) {

        if(validateConnection() && validateLoggingStatus()) {

            settings.saveInterval(interval);

            int offsetSecs = startTimestamp - time(0);  // Subtract current timestamp from start timestamp for offset

            if(validateInterval(QString::number(interval), duration)) {

                int totalLogs = duration / interval;

                if(validateTotalLogs(totalLogs)) {

                    bool success = sendConfiguration(startTimestamp, offsetSecs, interval, totalLogs);

                    if(success == true) {
                        QDateTime startDateTime;
                        startDateTime.setTime_t(startTimestamp);
                        QDateTime endDateTime = startDateTime.addSecs(duration);

                        showLogSetup(startDateTime, endDateTime, QString::number(interval));
                    }
                }
            }
        }
    }
}


bool LogSetup::sendConfiguration(int unixStamp, int offset, int interval, int numLogs) {

    qDebug() << "LOGGING PARAMETERS";
    qDebug() << "------------------";
    qDebug() << "Timestamp: " << unixStamp;
    qDebug() << "Offset: " << offset;
    qDebug() << "Interval: " << interval;
    qDebug() << "numLogs: " << numLogs;

    bool success = true;
    QByteArray command;

    int tries = 0;
    bool stat = false;
    int flashKey = 32;
    command.resize(1);
    command[0] = (flashKey & 0x000000FF);

    do {
        usbInterface->writeCommand(Constants::WRITE_FLASH_KEY_ID, 1, command);
        QThread::msleep(20);
        usbInterface->writeCommand(Constants::FLASH_ERASE_PACKET_ID, 0, command);
        QThread::msleep(200);
        stat = testFlashErase();

        tries++;
    } while((stat == false) && (tries < 5));

    if(stat == false) {
        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The log was not configured correctly due to a communication error. Please try disconnecting and reconnecting over USB.");
        msgBox.exec();
    } else {
        command.resize(14);

        command[0] = (unixStamp & 0x000000FF);
        command[1] = ((unixStamp >> 8) & 0x000000FF);
        command[2] = ((unixStamp >> 16) & 0x000000FF);
        command[3] = ((unixStamp >> 24) & 0x000000FF);

        command[4] = (offset & 0x000000FF);
        command[5] = ((offset >> 8) & 0x000000FF);
        command[6] = ((offset >> 16) & 0x000000FF);
        command[7] = ((offset >> 24) & 0x000000FF);

        command[8] = (interval & 0x000000FF);
        command[9] = ((interval >> 8) & 0x000000FF);
        command[10] = ((interval >> 16) & 0x000000FF);
        command[11] = ((interval >> 24) & 0x000000FF);

        command[12] = (numLogs & 0x000000FF);
        command[13] = ((numLogs >> 8) & 0x000000FF);

        stat = false;
        tries = 0;

        do {
            usbInterface->writeCommand(Constants::LOG_SETUP_PACKET_ID, 14, command);
            QThread::msleep(200);
            stat = testLogConfig(unixStamp, offset, interval, numLogs);

            tries++;
        } while((stat == false) && (tries < 5));

        if(stat == false) {
            success = false;

            QMessageBox msgBox;
            msgBox.setWindowTitle("Log Setup Error");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setText("The log was not configured correctly due to a communication error. Please try disconnecting and reconnecting over USB.");
            msgBox.exec();

            QThread::msleep(200);
            QByteArray command;
            usbInterface->writeCommand(Constants::RESET_LOG_PACKET_ID, 0, command);
        }
    }

    return success;
}


void LogSetup::eraseFlash() {
    const char *command = "erase\r";
    usbInterface->writeCommand(command);
}



void LogSetup::setUpLogLater(QWidget *parent, QString startDate, QString startTime, QString endDate, QString endTime, int startAmPm, int endAmPm, QString interval) {

    QDateTime startDateTime;
    QDateTime endDateTime;

    if(validateErase(parent)) {

        if(validateConnection() && validateLoggingStatus() && validateDateAndTime(startDate, startTime) && validateDateAndTime(endDate, endTime)) {

            QStringList startDateList = startDate.split("/");
            QDate date1(startDateList[2].toInt(), startDateList[0].toInt(), startDateList[1].toInt());
            startDateTime.setDate(date1);

            QStringList startTimeList = startTime.split(":");
            int startHour = startTimeList[0].toInt();

            if(startAmPm == 1) {
                if(startHour == 12) {
                    startHour = 0;
                }
                startHour += 12;
            }

            QTime time1(startHour, startTimeList[1].toInt(), startTimeList[2].toInt(), 0);
            startDateTime.setTime(time1);

            QStringList endDateList = endDate.split("/");
            QDate date2(endDateList[2].toInt(), endDateList[0].toInt(), endDateList[1].toInt());
            endDateTime.setDate(date2);

            QStringList endTimeList = endTime.split(":");
            int endHour = endTimeList[0].toInt();
            if(endAmPm == 1) {
                if(endHour == 12) {
                    endHour = 0;
                }
                endHour += 12;
            }
            QTime time2(endHour, endTimeList[1].toInt(), endTimeList[2].toInt(), 0);
            endDateTime.setTime(time2);

            qDebug() << "START TIME: " << startDateTime;
            qDebug() << "END TIME: " << endDateTime;

            if(validateStartTime(startDateTime) && validateEndTime(startDateTime, endDateTime)) {

                settings.saveInterval(interval.toInt());

                int offsetSecs = QDateTime::currentDateTime().secsTo(startDateTime);
                int totalSecs = startDateTime.secsTo(endDateTime);

                if(validateInterval(interval, totalSecs)) {

                    int totalLogs = totalSecs / interval.toInt();

                    if(validateTotalLogs(totalLogs)) {
                        bool success = sendConfiguration(startDateTime.toTime_t(), offsetSecs, interval.toInt(), totalLogs);

                        if(success == true) {
                            showLogSetup(startDateTime, endDateTime, interval);
                        }
                    }
                }
            }
        }
    }
}


void LogSetup::setUpLogLater(QWidget *parent, QString startDate, QString startTime, int startAmPm, QString days, QString hours, QString minutes, QString seconds, QString interval) {

    QDateTime startDateTime;

    if(validateErase(parent)) {

        if(validateConnection() && validateLoggingStatus() && validateDateAndTime(startDate, startTime)) {

            QStringList startDateList = startDate.split("/");
            QDate date1(startDateList[2].toInt(), startDateList[0].toInt(), startDateList[1].toInt());
            startDateTime.setDate(date1);

            QStringList startTimeList = startTime.split(":");
            int startHour = startTimeList[0].toInt();
            if(startAmPm == 1) {
                if(startHour == 12) {
                    startHour = 0;
                }
                startHour += 12;
            }
            QTime time1(startHour, startTimeList[1].toInt(), startTimeList[2].toInt(), 0);
            startDateTime.setTime(time1);

            qDebug() << "START TIME: " << startDateTime;

            if(validateLogTime(days, hours, minutes, seconds) && validateStartTime(startDateTime)) {

                int numDays = days.toInt();
                int numHours = hours.toInt();
                int numMinutes = minutes.toInt();
                int numSeconds = seconds.toInt();

                int offsetSecs = QDateTime::currentDateTime().secsTo(startDateTime);
                int totalSecs = numSeconds + (numMinutes * 60) + (numHours * 60 * 60) + (numDays * 60 * 60 * 24);

                if(validateInterval(interval, totalSecs)) {

                    settings.saveTimeLength(numDays, numHours, numMinutes, numSeconds);
                    settings.saveInterval(interval.toInt());

                    int totalLogs = totalSecs / interval.toInt();

                    if(validateTotalLogs(totalLogs)) {
                        bool success = sendConfiguration(startDateTime.toTime_t(), offsetSecs, interval.toInt(), totalLogs);

                        QDateTime endDateTime = startDateTime.addSecs(totalSecs);

                        if(success == true) {
                            showLogSetup(startDateTime, endDateTime, interval);
                        }
                    }
                }
            }
        }
    }
}


void LogSetup::setUpLogNow(QWidget *parent, QString endDate, QString endTime, int endAmPm, QString interval) {

    QDateTime endDateTime;

    if(validateErase(parent)) {

        if(validateConnection() && validateLoggingStatus() && validateDateAndTime(endDate, endTime)) {

            QStringList endDateList = endDate.split("/");
            QDate date2(endDateList[2].toInt(), endDateList[0].toInt(), endDateList[1].toInt());
            endDateTime.setDate(date2);

            QStringList endTimeList = endTime.split(":");
            int endHour = endTimeList[0].toInt();
            if(endAmPm == 1) {
                if(endHour == 12) {
                    endHour = 0;
                }
                endHour += 12;
            }
            QTime time2(endHour, endTimeList[1].toInt(), endTimeList[2].toInt(), 0);
            endDateTime.setTime(time2);

            qDebug() << "END TIME: " << endDateTime;

            if(validateEndTime(QDateTime::currentDateTime(), endDateTime)) {

                int offsetSecs = 0;
                int totalSecs = QDateTime::currentDateTime().secsTo(endDateTime);

                if(validateInterval(interval, totalSecs)) {

                    int totalLogs = totalSecs / interval.toInt();

                    if(validateTotalLogs(totalLogs)) {

                        settings.saveInterval(interval.toInt());

                        int timestamp = time(0);
                        bool success = sendConfiguration(timestamp, offsetSecs, interval.toInt(), totalLogs);

                        QDateTime startDateTime = QDateTime::currentDateTime();

                        if(success == true) {
                            showLogSetup(startDateTime, endDateTime, interval);
                        }
                    }
                }
            }
        }
    }
}


void LogSetup::setUpLogNow(QWidget *parent, QString days, QString hours, QString minutes, QString seconds, QString interval) {

    if(validateErase(parent)) {

        if(validateConnection() && /* validateLoggingStatus() && */ validateLogTime(days, hours, minutes, seconds)) {

            int numDays = days.toInt();
            int numHours = hours.toInt();
            int numMinutes = minutes.toInt();
            int numSeconds = seconds.toInt();

            int offsetSecs = 0;
            int totalSecs = numSeconds + (numMinutes * 60) + (numHours * 60 * 60) + (numDays * 60 * 60 * 24);

            if(validateInterval(interval, totalSecs)) {

                int totalLogs = totalSecs / interval.toInt();

                if(validateTotalLogs(totalLogs)) {

                    settings.saveTimeLength(numDays, numHours, numMinutes, numSeconds);
                    settings.saveInterval(interval.toInt());

                    int timestamp = time(0);
                    bool success = sendConfiguration(timestamp, offsetSecs, interval.toInt(), totalLogs);

                    QDateTime startDateTime = QDateTime::currentDateTime();
                    QDateTime endDateTime = startDateTime.addSecs(totalSecs);

                    if(success == true) {
                        showLogSetup(startDateTime, endDateTime, interval);
                    }
                }
            }
        }
    }
}


void LogSetup::showLogSetup(QDateTime startDateTime, QDateTime endDateTime, QString interval) {

    int intervalVal = interval.toInt();
    int messageInterval = 0;
    QString intervalType = " second(s)";

    // Display success message
    QString message = "The device has been successfully set up for this log.";
    message.append("\n\nStarting time: ");
    message.append(startDateTime.toString("M/d/yyyy h:mm:ss AP"));
    message.append("\nEnding time: ");
    message.append(endDateTime.toString("M/d/yyyy h:mm:ss AP"));
    message.append("\nTime between logs: ");
    if((intervalVal > 59) && (intervalVal < 3600)) {
        messageInterval = intervalVal / 60;
        intervalType = " minute(s)";
    }
    else if(intervalVal >= 3600) {
        messageInterval = intervalVal / 3600;
        intervalType = " hour(s)";
    }
    else {
        messageInterval = intervalVal;
    }

    message.append(QString::number(messageInterval));
    message.append(intervalType);

    QMessageBox msgBox;
    msgBox.setWindowTitle("Success");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setText(message);
    msgBox.exec();
}


bool LogSetup::validateConnection() {

    if(usbInterface->isConnected() == false) {

        QMessageBox msgBox;
        msgBox.setWindowTitle("Connnection Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The device is not currently connected over USB.");
        msgBox.exec();
    }

    return usbInterface->isConnected();
}


bool LogSetup::validateErase(QWidget *parent) {

    bool success = false;

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(parent, parent->tr("Confirm"),
                                 "Warning: Starting a new log will erase all currently stored data on your device. Continue?",
                                 QMessageBox::Yes | QMessageBox::No);

    if(reply == QMessageBox::Yes) {
        success = true;
    }

    return success;
}


bool LogSetup::validateTotalLogs(int totalLogs) {

    bool success = true;

    if(totalLogs > maxLogs) {

        success = false;

        QString message = "You have surpassed the maximum number of logs that this device can hold. The maximum is ";
        message.append(QString::number(maxLogs));
        message.append(". You are over by ");
        message.append(QString::number(totalLogs - maxLogs));
        message.append(".");

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText(message);
        msgBox.exec();
    }

    return success;
}


bool LogSetup::validateInterval(QString interval, int totalSecs) {

    bool success = true;

    bool valid;
    int numInterval = interval.toInt(&valid, 10);

    if(valid == false) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The interval must be a number that includes no other characters.");
        msgBox.exec();
    }
    else if(interval == "") {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("You need to enter a logging interval.");
        msgBox.exec();
    }
    else if((numInterval == 0) || (numInterval < 0)) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The interval must be a positive number.");
        msgBox.exec();
    }
    else if(numInterval > totalSecs) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The interval must be less than the total logging time.");
        msgBox.exec();
    }

    return success;
}


bool LogSetup::validateLogTime(QString days, QString hours, QString minutes, QString seconds) {

    bool valid1;
    bool valid2;
    bool valid3;
    bool valid4;
    int numDays = days.toInt(&valid1, 10);
    int numHours = hours.toInt(&valid2, 10);
    int numMinutes = minutes.toInt(&valid3, 10);
    int numSeconds = seconds.toInt(&valid4, 10);

    bool success = true;

    if((days == "0") && (hours == "0") && (minutes == "0") && (seconds == "0")) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("You need to enter a length of time to log.");
        msgBox.exec();
    }
    else if((valid1 == false) || (valid2 == false) || (valid3 == false) || (valid4 == false)) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The log length must be a positive number and include no other characters.");
        msgBox.exec();
    }
    else if((numDays < 0) || (numHours < 0) || (numMinutes < 0) || (numSeconds < 0)) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The log length must be a positive number and include no other characters.");
        msgBox.exec();
    }
    else if((days == 0) && (hours == 0) && (minutes == 0) && (seconds == 0)) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("You must have a log length longer than 0 seconds.");
        msgBox.exec();
    }

    return success;
}


bool LogSetup::validateDateAndTime(QString date, QString time) {

    bool success = true;

    QStringList dateList = date.split("/");
    QStringList timeList = time.split(":");
    int dateCount = dateList.size();
    int timeCount = timeList.size();

    if((date == "") || (time == "")) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("You need to enter both a date and a time.");
        msgBox.exec();
    }
    else if(dateCount != 3) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("Date format is incorrect.");
        msgBox.exec();
    }
    else if(timeCount != 3) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("Time format is incorrect.");
        msgBox.exec();
    }
    else if( ((timeList.at(0).toInt() > 12) || (timeList.at(0).toInt() < 1))
              || ((timeList.at(1).toInt() > 59) || (timeList.at(1).toInt() < 0))
                || ((timeList.at(2).toInt() > 59) || (timeList.at(2).toInt() < 0)) ){
        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("Time format is incorrect. Example: 05:45:00");
        msgBox.exec();
    }

    return success;
}


bool LogSetup::validateEndTime(QDateTime startTime, QDateTime endTime) {

    bool success = true;

    if(endTime < QDateTime::currentDateTime()) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The end time must not be in the past.");
        msgBox.exec();

    }
    else if(endTime < startTime) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The end time must not be before the start time.");
        msgBox.exec();
    }
    else if(endTime == startTime) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The end time must not be the same as the start time.");
        msgBox.exec();
    }

    return success;
}


bool LogSetup::validateStartTime(QDateTime startTime) {

    bool success = true;

    if(startTime < QDateTime::currentDateTime()) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The start time must not be in the past.");
        msgBox.exec();
    }

    return success;
}


bool LogSetup::validateLoggingStatus() {

    bool success = true;
    /*

    const char *command = "logstat\r";

    QList<int> list = usbInterface->readPacket(command, Constants::LOG_STAT_ID);

    if(list.at(0) == 1) {

        success = false;

        QMessageBox msgBox;
        msgBox.setWindowTitle("Log Setup Error");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("The device is currently logging. You can reset to force the logging to stop.");
        msgBox.exec();
    }
*/
    return success;
}
