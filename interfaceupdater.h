#ifndef INTERFACEUPDATER_H
#define INTERFACEUPDATER_H

#include <QtCore/QDebug>
#include <QtCore/QThread>
#include <QDateTime>
#include "usersettings.h"
#include "climatelogger.h"

#include "html5applicationviewer/html5applicationviewer.h"

class InterfaceUpdater : public Html5ApplicationViewer
{
    Q_OBJECT

public:
    explicit InterfaceUpdater(QWidget *parent=0);
    char stampBytes[4];

    int stampVal;
    QString timestampString;
    int offsetVal;
    int intervalVal;
    int logVal;
    bool connectionStatus;
    bool usbErrorMessage;
    UserSettings settings;

    ClimateLogger *climateLogger;
private:
    int MAX_LOGS;
    int LOG_PACKET_SIZE;
    int NUM_SENSORS;
    int NUM_ADCS;

    static const int TEMP_ID;
    static const int HUMIDITY_ID;
    static const int ADC_ID;
    static const int LOG_STAT_ID;
    static const int RESET_ERROR_ID;
    static const int BATT_ERROR_ID;
    static const int FW_VER_ID;
    static const int LOG_CON_ID;
    static const int PRESSURE_ID;
    static const int LIGHT_ID;
    static const int HW_VER_ID;
    static const int AVAIL_SENSORS_ID;
    static const int UV_ID;
    static const int IR_ID;
    static const int ALL_SENSORS_ID;
    static const int SENSOR_TIME_ID;
    static const int NAME_ID;
    static const int BATTERY_ID;
    static const int AVAIL_ADC_ID;
    static const int CO_ZERO_STAT_ID;
    static const int CO_CAL_STAT_ID;
    static const int CO_BASELINE_ID;
    static const int CO_SENS_ID;
    static const int CO_BASELINE_ADC_ID;
    static const int CO_SENS_ADC_ID;
    static const int ALL_ADC_ID;
    static const int ALL_DEBUG_ID;
    static const int AVAIL_DEBUG_ID;


    QString sensorNames[8];
    QString debugNames[8];

private slots:
    void addToJavaScript();
public slots:
    void storeTemperatureUnit(int unit);
    void storePressureUnit(int unit);
    int getTemperatureUnit();
    int getPressureUnit();
    int getLastLogInterval();
    int getLastLogDays();
    int getLastLogHours();
    int getLastLogMinutes();
    int getLastLogSeconds();
    //bool getProductionVersion();
    QString getProductIdString();
    int getProductVersion();
    int getProductRevision();
    int getProductBuild();
    QString getCoBaseline();
    QString getCoSensitivity();
    QString getCoBaselineTemp();
    QString getCoSensitivityTemp();
    QString getCoBaselineADC();
    QString getCoSensitivityADC();
    int getDaysUntilCalibration();
    bool getTemperatureSensor();
    bool getHumiditySensor();
    bool getPressureSensor();
    bool getLightSensor();
    bool getCOSensor();
    bool getUVSensor();
    bool getIRSensor();
    bool getBatterySensor();
    bool getADC4();
    bool getADC5();
    bool getADC9();
    bool connectUSB();
    bool isConnected();
    void setUpLogLater(QString startDate, QString startTime, QString endDate, QString endTime, int startAmPm, int endAmPm, QString interval);
    void setUpLogLater(QString startDate, QString startTime, int startAmPm, QString days, QString hours, QString minutes, QString seconds, QString interval);
    void setUpLogNow(QString endDate, QString endTime, int endAmPm, QString interval);
    void setUpLogNow(QString days, QString hours, QString minutes, QString seconds, QString interval);
    //void config(QString unixStamp, QString offset, QString interval, QString numLogs);
    void measureSensors();
    void coCalibrate();
    void coZero();
    void updateSensorReadings();
    QString readTemperature();
    QString readHumidity();
    QString readPressure();
    QString readLight();
    QString readCO();
    QString readUV();
    QString readIR();
    QString readBattery();
    QString readADC();
    QString readADC4();
    QString readADC5();
    QString readADC9();
    QString readDeviceName();
    void openDeviceNameDialog();
    //bool checkForNewLogs(bool askForDownload);
    //int downloadFromDevice(void);
    void generateCSV(int captureID = 0);
    void eraseFlash();
    void eraseDB();
    int checkLoggingStatus();
    int checkCoZeroStatus();
    int checkCoCalStatus();
    void clearCoZeroStatus();
    void clearCoCalStatus();
    bool checkLogResetError();
    void resetLog();
    void showSensorconSite();
    // duplicate of function in climatelogger.cpp
    //bool advancedDialog();
};

#endif // INTERFACEUPDATER_H
